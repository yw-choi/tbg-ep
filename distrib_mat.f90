! This module is a modified version of ParaBands/distribution.f90 of BGW package 
module distrib_mat
  use mpi
  use mpi_pool
  use kinds, only: dp
  implicit none

  type, public :: distrib_mat_t

    class(mpi_pool_t), pointer :: pool

    integer :: cntxt ! blacs context
    integer :: desc(9)
    !> Number of rows in distributed matrix
    integer :: M
    !> Number of cols in distributed matrix
    integer :: N
    !> Number of rows in local matrix buffer
    integer :: Ml
    !> Number of cols in local matrix buffer
    integer :: Nl
    !> Number of rows of the distributed matrix that I own (<=Ml)
    integer :: Mown
    !> Number of cols of the distributed matrix that I own (<=Nl)
    integer :: Nown
    !> Block size
    integer :: Mb, Nb
    !> Number of processors in each row of the *processor grid*
    integer :: nprow
    !> Number of processors in each col of the *processor grid*
    integer :: npcol
    !> My row number in the blacs *processor grid*
    integer :: myprow
    !> My col number in the blacs *processor grid*
    integer :: mypcol

  contains

    procedure, private :: setup_serial_mat => distrib_setup_serial_mat
    procedure :: setup_mat_1d => distrib_setup_mat_1d
    procedure :: setup_mat_2d => distrib_setup_mat_2d

  endtype distrib_mat_t

  public :: read_matrix, write_matrix_all, write_matrix
  
  private 

  integer, parameter :: BLOCK_SIZE = 32
  integer, external :: numroc

contains

subroutine distrib_setup_serial_mat(dm, M, N)
  class(distrib_mat_t), intent(inout) :: dm
  integer, intent(in) :: M
  integer, intent(in) :: N

  dm%nprow = 1
  dm%npcol = 1
  dm%myprow = 0
  dm%mypcol = 0
  dm%M = M
  dm%N = N
  dm%Mb = M
  dm%Nb = N
  dm%Mown = M
  dm%Nown = N
  dm%Ml = M
  dm%Nl = N

end subroutine distrib_setup_serial_mat

subroutine distrib_setup_mat_1d(dm, M, N, pool, row_or_col)
  class(distrib_mat_t), intent(inout) :: dm
  integer, intent(in) :: M
  integer, intent(in) :: N
  type(mpi_pool_t), target, intent(in) :: pool
  character, optional, intent(in) :: row_or_col

  integer :: info
  character :: row_or_col_

  row_or_col_ = 'c'
  if (present(row_or_col)) row_or_col_ = row_or_col

  dm%pool => pool

  ! Global matrix size
  dm%N = N
  dm%M = M
  if (row_or_col_=='c'.or.row_or_col_=='C') then
    dm%cntxt = pool%cntxt_c
    ! Blacs processor grid
    dm%nprow = 1
    dm%npcol = pool%nprocs
    dm%myprow = 0
    dm%mypcol = pool%rank
    ! Block sizes
    dm%Nb = DIVUP(N, pool%nprocs)
    dm%Mb = dm%Nb
    ! Local matrix buffer size
    dm%Nl = dm%Nb
    dm%Ml = M
    ! Number of rows/cols I own
    dm%Nown = numroc(N, dm%Nb, pool%rank, 0, pool%nprocs)
    dm%Mown = M
  elseif (row_or_col_=='r'.or.row_or_col_=='R') then
    dm%cntxt = pool%cntxt_r
    ! Blacs processor grid
    dm%npcol = 1
    dm%nprow = pool%nprocs
    dm%mypcol = 0
    dm%myprow = pool%rank
    ! Block sizes
    dm%Mb = DIVUP(M, pool%nprocs)
    dm%Nb = dm%Mb
    ! Local matrix buffer size
    dm%Ml = dm%Mb
    dm%Nl = N
    ! Number of rows/cols I own
    dm%Mown = numroc(M, dm%Mb, pool%rank, 0, pool%nprocs)
    dm%Nown = N
  else
    call errore('distrib_mat_t', 'Unknown value for row_of_col', 1)
  endif

  call descinit(dm%desc, dm%M, dm%N, dm%Mb, dm%Nb, 0, 0, dm%cntxt, dm%Ml, info)
  if (info/=0) call errore('distrib_setup_mat_1d', 'got info/=0 in descinit', info)

end subroutine distrib_setup_mat_1d

subroutine distrib_setup_mat_2d(dm, M, N, pool)
  class(distrib_mat_t), intent(inout) :: dm
  integer, intent(in) :: M
  integer, intent(in) :: N
  integer :: info
  type(mpi_pool_t), target, intent(in) :: pool

  dm%pool => pool

  dm%cntxt = pool%cntxt_2d
  ! Global matrix size
  dm%N = N
  dm%M = M
  ! Block sizes
  if (BLOCK_SIZE*pool%nprocs<=dm%M) then
    dm%Mb = BLOCK_SIZE
  else
    dm%Mb = max(1,int(dm%M/pool%nprocs))
  endif

  if (BLOCK_SIZE*pool%nprocs<=dm%N) then
    dm%Nb = BLOCK_SIZE
  else
    dm%Nb = max(1,int(dm%N/pool%nprocs))
  endif

  ! Blacs processor grid
  dm%nprow = pool%nprow
  dm%npcol = pool%npcol
  dm%myprow = pool%myprow
  dm%mypcol = pool%mypcol

  ! Number of rows/cols I own
  if (dm%cntxt>=0) then
    dm%Mown = numroc(dm%M, dm%Mb, dm%myprow, 0, dm%nprow)
    dm%Nown = numroc(dm%N, dm%Nb, dm%mypcol, 0, dm%npcol)
  else
    dm%Mown = 0
    dm%Nown = 0
  endif
  ! Local matrix buffer size
  dm%Ml = max(1, dm%Mown)
  dm%Nl = max(1, dm%Nown)
  call descinit(dm%desc, dm%M, dm%N, dm%Mb, dm%Nb, 0, 0, dm%cntxt, dm%Ml, info)
  if (info/=0) call errore('distrib_setup_mat_2d', 'got info/=0 in descinit', info)

end subroutine distrib_setup_mat_2d

! write Z_global(IA:IA+M-1, JA:JA+N-1) to fn
subroutine write_matrix(fn, Zmat, Z, M, N, IA, JA)
  character(len=256), intent(in) :: fn
  class(distrib_mat_t), intent(in) :: Zmat
  complex(dp), intent(in) :: Z(Zmat%Ml,Zmat%Nl)
  integer, intent(in) :: M, N, IA, JA

  integer :: ierr
  integer, dimension(2) :: pdims, dims, distribs, dargs
  integer :: outfile
  integer, dimension(MPI_STATUS_SIZE) :: mpistatus
  integer :: darray

  type(distrib_mat_t) :: EVmat
  complex(dp), allocatable :: EV(:,:)
  integer(kind=MPI_OFFSET_KIND) :: disp

  call EVmat%setup_mat_2d(M, N, Zmat%pool)
  allocate(EV(EVmat%Ml, Evmat%Nl))
  call pzgemr2d(M, N, Z, IA, JA, Zmat%desc, EV, 1, 1, EVmat%desc, Zmat%cntxt)

  pdims = [EVmat%nprow, EVmat%npcol]
  dims = [EVmat%M, EVmat%N]
  distribs = [MPI_DISTRIBUTE_CYCLIC, MPI_DISTRIBUTE_CYCLIC]
  dargs = [EVmat%Mb, EVmat%Nb]
  call MPI_Type_create_darray(Zmat%pool%nprocs, Zmat%pool%rank, 2, dims, distribs, dargs, &
                            pdims, MPI_ORDER_FORTRAN, MPI_DOUBLE_COMPLEX, &
                            darray, ierr)
  call MPI_Type_commit(darray,ierr)

  call MPI_File_open(Zmat%pool%comm, trim(fn), MPI_MODE_CREATE+MPI_MODE_WRONLY, MPI_INFO_NULL, outfile, ierr)
  call mpi_check_call(ierr)
  disp = 0
  call MPI_File_set_view(outfile, disp, MPI_DOUBLE_COMPLEX, darray, "native", MPI_INFO_NULL, ierr)
  call mpi_check_call(ierr)

  call MPI_File_write_all(outfile, EV, EVmat%Ml*EVmat%Nl, MPI_DOUBLE_COMPLEX, MPI_STATUS_IGNORE, ierr)
  call mpi_check_call(ierr)
  call MPI_File_close(outfile,ierr)
  call mpi_check_call(ierr)
  call mpi_type_free(darray,ierr)

  deallocate(EV)

end subroutine write_matrix

subroutine write_matrix_all(fn, Zmat, Z)
  character(len=256), intent(in) :: fn
  class(distrib_mat_t), intent(in) :: Zmat
  complex(dp), intent(in) :: Z(Zmat%Ml,Zmat%Nl)

  integer :: ierr
  integer, dimension(2) :: pdims, dims, distribs, dargs
  integer :: outfile
  integer, dimension(MPI_STATUS_SIZE) :: mpistatus
  integer :: darray
  integer(kind=MPI_OFFSET_KIND) :: disp

  pdims = [Zmat%nprow, Zmat%npcol]
  dims = [Zmat%M, Zmat%N]
  distribs = [MPI_DISTRIBUTE_CYCLIC, MPI_DISTRIBUTE_CYCLIC]
  dargs = [Zmat%Mb, Zmat%Nb]
  call MPI_Type_create_darray(Zmat%pool%nprocs, Zmat%pool%rank, &
    2, dims, distribs, dargs, pdims, MPI_ORDER_FORTRAN, &
    MPI_DOUBLE_COMPLEX, darray, ierr)
  call mpi_check_call(ierr)
  call MPI_Type_commit(darray,ierr)
  call mpi_check_call(ierr)

  call MPI_File_open(Zmat%pool%comm, trim(fn), &
    MPI_MODE_CREATE+MPI_MODE_WRONLY, MPI_INFO_NULL, outfile, ierr)
  call mpi_check_call(ierr)

  disp = 0
  call MPI_File_set_view(outfile, disp, MPI_DOUBLE_COMPLEX, &
    darray, "native", MPI_INFO_NULL, ierr)
  call mpi_check_call(ierr)

  call MPI_File_write_all(outfile, Z, Zmat%Ml*Zmat%Nl, &
    MPI_DOUBLE_COMPLEX, MPI_STATUS_IGNORE, ierr)
  call mpi_check_call(ierr)

  call MPI_File_close(outfile, ierr)
  call mpi_check_call(ierr)
  call mpi_type_free(darray,ierr)

end subroutine write_matrix_all

subroutine read_matrix(fn, Zmat, Z)
  character(len=256), intent(in) :: fn
  class(distrib_mat_t), intent(in) :: Zmat
  complex(dp), intent(out) :: Z(Zmat%Ml,Zmat%Nl)

  integer :: ierr
  integer, dimension(2) :: pdims, dims, distribs, dargs
  integer :: outfile
  integer, dimension(MPI_STATUS_SIZE) :: mpistatus
  integer :: darray

  integer(kind=MPI_OFFSET_KIND) :: disp
  integer :: tt

  pdims = [Zmat%nprow, Zmat%npcol]
  dims = [Zmat%M, Zmat%N]
  distribs = [MPI_DISTRIBUTE_CYCLIC, MPI_DISTRIBUTE_CYCLIC]
  dargs = [Zmat%Mb, Zmat%Nb]
  call MPI_Type_create_darray(Zmat%pool%nprocs, Zmat%pool%rank, &
    2, dims, distribs, dargs, pdims, MPI_ORDER_FORTRAN, &
    MPI_DOUBLE_COMPLEX, darray, ierr)
  call mpi_check_call(ierr)
  call MPI_Type_commit(darray,ierr)
  call mpi_check_call(ierr)

  call MPI_File_open(Zmat%pool%comm, trim(fn), MPI_MODE_RDONLY, &
    MPI_INFO_NULL, outfile, ierr)
  call mpi_check_call(ierr)

  disp = 0
  call MPI_File_set_view(outfile, disp, MPI_DOUBLE_COMPLEX, darray, &
    "native", MPI_INFO_NULL, ierr)
  call mpi_check_call(ierr)

  call MPI_File_read_all(outfile, Z, Zmat%Ml*Zmat%Nl, &
    MPI_DOUBLE_COMPLEX, MPI_STATUS_IGNORE, ierr)
  call mpi_check_call(ierr)
  call MPI_File_close(outfile,ierr)
  call mpi_check_call(ierr)
  call mpi_type_free(darray,ierr)

end subroutine read_matrix

! Integer division of a/b rounded up
integer function DIVUP(a,b) result (c)
  integer, intent(in) :: a,b
  c = (a+b-1)/b
end function DIVUP

end module distrib_mat
