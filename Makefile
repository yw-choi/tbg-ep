
include make.inc

PROGRAMS=ep.x
# OBJECTS=mpi.o kinds.o utils.o constants.o invmat.o global_m.o\
#        tetra.o sort.o input.o tbg.o lattice.o dynmat.o epmat.o \
# 			 kgrid.o hoppings.o phonons.o electrons.o \
# 			 diag.o distrib_mat.o eliashberg.o dos.o
OBJECTS=version.o mpi.o mpi_pool.o input.o utils.o \
			tbg.o lattice.o global_m.o tetra.o kgrid.o \
			hoppings.o distrib_mat.o dos.o diag.o \
			electrons.o dynmat.o phonons.o conjgr.o relax.o \
			epmat.o eliashberg.o

UTIL_DIR=qe_utils
IFLAGS+=-I${UTIL_DIR}/Modules \
        -I${UTIL_DIR}/UtilXlib
LIBS+=${UTIL_DIR}/UtilXlib/libutil.a \
      ${UTIL_DIR}/Modules/libqemod.a \
      ${UTIL_DIR}/clib/clib.a

all: $(PROGRAMS)
ep.o: $(OBJECTS)
version.o: version
ep.x: utils $(OBJECTS) ep.o 
	$(FC) -o ep.x ep.o $(IFLAGS) $(FCFLAGS) $(LDFLAGS) $(OBJECTS) $(LIBS)

utils:
	cd ${UTIL_DIR}/clib; make
	cd ${UTIL_DIR}/UtilXlib; make
	cd ${UTIL_DIR}/Modules; make

VERSION=$(shell git describe --dirty --always --tags)
version:
	@echo "module version" > version.f90
	@echo "  implicit none" >> version.f90
	@echo '  character(len=*), parameter :: commit="$(VERSION)"' >> version.f90
	@echo "end module version" >> version.f90

# Utility targets
.PHONY: clean veryclean

clean:
	find . -type f | xargs touch
	rm -f *.o *.mod *.MOD *.x

veryclean: clean
	rm -f *~ $(PROGRAMS)
	cd ${UTIL_DIR}/Modules; make clean
	cd ${UTIL_DIR}/UtilXlib; make clean
	cd ${UTIL_DIR}/clib; make clean


electrons.o: diag.o distrib_mat.o hoppings.o dos.o
phonons.o: diag.o distrib_mat.o kgrid.o dos.o
epmat.o: phonons.o kgrid.o electrons.o
lattice.o: tbg.o
calcfg.o: dynmat.o relax.o
