module diag

  use mpi
  use global_m, only: verbosity, VERBOSITY_DEBUG
  use kinds, only: dp
  use distrib_mat, only: distrib_mat_t
  use constants, only: eps12
#if defined(__ELPA)
  use elpa
#endif

  implicit none

  public :: diag_setup, diag_cleanup, diagonalize

  private

  save

#if defined(__ELPA)
  class(elpa_t), pointer :: myelpa
#endif

  integer :: LWORK, LRWORK, LIWORK, INFO
  complex(dp), allocatable :: WORK(:)
  real(dp), allocatable :: RWORK(:), GAP(:)
  integer, allocatable :: IWORK(:), IFAIL(:), ICLUSTR(:)
  class(distrib_mat_t), pointer :: Amat
  integer :: nev
  integer :: solver

  integer :: nev_found, nz

  real(dp) :: ABSTOL, ORFAC
  real(dp), external :: PDLAMCH

contains

subroutine diag_setup (Amat_, nev_, solver_)
  class(distrib_mat_t), target, intent(in) :: Amat_
  integer, intent(in) :: nev_, solver_

  ! Dummies for SCALAPACK setup
  complex(dp) :: A(1,1)
  real(dp) :: W(1)
  complex(dp) :: Z(1,1)

  solver = solver_
  Amat => Amat_
  nev = nev_

  if (solver==1) then
#if defined(__ELPA)
    if (elpa_init(20180525) /= elpa_ok) then
      call errore("diag_elpa", "elpa api version not supported", 1)
    endif

    myelpa => elpa_allocate()

    call myelpa%set("na", Amat%N, info)
    call myelpa%set("nev", nev, info)
    call myelpa%set("local_nrows", Amat%Ml, info)
    call myelpa%set("local_ncols", Amat%Nl, info)
    call myelpa%set("nblk", Amat%Nb, info)
    call myelpa%set("mpi_comm_parent", Amat%pool%comm, info)
    call myelpa%set("process_row", Amat%myprow, info)
    call myelpa%set("process_col", Amat%mypcol, info)
    call myelpa%set("blacs_context", Amat%cntxt, info)
    call myelpa%set("solver", elpa_solver_2stage, info)

    info = myelpa%setup()

    if (info.ne.0) then
      call errore("diag_setup", "elpa setup failed", info)
    endif
#else
    call errore('diag_setup', 'ELPA not compiled', 1)
#endif
  else if (solver==2) then

    ABSTOL = PDLAMCH( Amat%cntxt, 'U')
    ORFAC = 1.d-3

    LWORK = -1
    LRWORK = -1
    LIWORK = -1
    ALLOCATE(WORK(1), RWORK(1), IWORK(1))
    ALLOCATE(IFAIL(Amat%N), GAP(Amat%nprow*Amat%npcol))
    ALLOCATE(ICLUSTR(2*Amat%nprow*Amat%npcol))

    call PZHEEVX( 'V', 'I', 'U', Amat%N, A, 1, 1, Amat%DESC, 0.d0, &
          0.d0, 1, NEV, ABSTOL, NEV_FOUND, NZ, W, ORFAC, Z, 1, &
          1, Amat%DESC, WORK, LWORK, RWORK, LRWORK, IWORK, &
          LIWORK, IFAIL, ICLUSTR, GAP, INFO )

    LWORK = INT(2.d0*DBLE(WORK(1)))
    LRWORK = INT(2.d0*RWORK(1))
    LIWORK = 2*IWORK(1)

    DEALLOCATE(WORK, RWORK, IWORK)
    ALLOCATE(WORK(LWORK), RWORK(LRWORK), IWORK(LIWORK))
  else
    call errore('diag_setup', 'invalid solver', solver)
  endif

end subroutine diag_setup

subroutine diag_cleanup

  nullify(Amat)

  if (solver==1) then
#if defined(__ELPA)
    call elpa_deallocate(myelpa)
    call elpa_uninit()
#endif
  else if (solver==2) then
    DEALLOCATE(WORK, RWORK, IWORK)
    DEALLOCATE(IFAIL, GAP)
    DEALLOCATE(ICLUSTR)
  endif

end subroutine diag_cleanup

subroutine diagonalize (A, W, Z)
  complex(dp), intent(in) :: A(Amat%Ml, Amat%Nl)
  real(dp), intent(out) :: W(Amat%N)
  complex(dp), intent(out) :: Z(Amat%Ml, Amat%Nl)

  if (solver==1) then
#if defined(__ELPA)
    call myelpa%eigenvectors(a, w, z, info)
    
    if (info/=ELPA_OK) then
      print *, elpa_strerr(info)
      call errore("diagonalize", "ELPA failed", INFO)
    endif
#else
    call errore('diagonalize', 'ELPA not compiled', 1)
#endif
  else if (solver==2) then

    call PZHEEVX( 'V', 'I', 'U', Amat%N, A, 1, 1, Amat%DESC, 0.d0, &
          0.d0, 1, NEV, ABSTOL, NEV_FOUND, NZ, W, ORFAC, Z, 1, &
          1, Amat%DESC, WORK, LWORK, RWORK, LRWORK, IWORK, &
          LIWORK, IFAIL, ICLUSTR, GAP, INFO )

    if (info/=0) then
      call errore("diagonalize", "PZHEEVX failed", INFO)
    endif
  else
    call errore('diagonalize', 'invalid solver', solver)
  endif

  if (verbosity>=VERBOSITY_DEBUG) then
    call check_ortho (Amat, Z, NEV)
  endif

end subroutine diagonalize

! Debugging routines
subroutine check_ortho ( Zmat, Z, NEV )
  class(distrib_mat_t), intent(in) :: Zmat
  integer, intent(in) :: NEV
  complex(dp), intent(in) :: Z(Zmat%Ml, Zmat%Nl)

  complex(dp), allocatable :: work(:,:)

  integer :: iloc, ig, jloc, jg, iprow, ipcol
  integer, external :: indxl2g
  real(dp) :: residue, maxres
  allocate(work(Zmat%Ml,Zmat%Nl))

  call PZGEMM ('C', 'N', Zmat%N, Zmat%N, Zmat%N, &
    (1.d0, 0.d0), &
    Z, 1, 1, Zmat%desc, &
    Z, 1, 1, Zmat%desc, &
    (0.d0, 0.d0), &
    work, 1, 1, Zmat%desc)

  maxres = 0.d0
  do iloc = 1, Zmat%Ml
    do jloc = 1, Zmat%Nl
      ig = indxl2g(iloc, Zmat%Mb, Zmat%pool%myprow, 0, Zmat%pool%nprow)
      jg = indxl2g(jloc, Zmat%Nb, Zmat%pool%mypcol, 0, Zmat%pool%npcol)
      if (ig>NEV .or. jg>NEV) cycle

      if (ig==jg) then
        residue = abs(1.d0-work(iloc,jloc))
      else
        residue = abs(work(iloc,jloc))
      endif
      if (residue > eps12) then
        print *, 'ig, jg, residue = ', ig, jg, residue
        call errore('check_ortho', 'eigenvectors are not orthonormal', 1)
      endif
      if (maxres < residue) then
        maxres = residue
      endif
    enddo
  enddo

  write(stdout, '(1x,a,E12.4)') 'eigenvectors are orthonormal. max residue = ', maxres

  deallocate(work)
end subroutine check_ortho

end module diag
