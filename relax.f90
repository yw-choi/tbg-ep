module relax
  use mpi
  use kinds, only: DP
  use constants, only: ANGSTROM_AU, RYTOEV, PI, eps16
  use lattice, only: struct_t
  use global_m, only: outdir, verbosity, VERBOSITY_INFO
  use input, only: params_t
  use m_conjgr, only: conjgr
  use dynmat, only: NNB_INTRA, dv_kc, v_kc, fc_intra

  implicit none

  public :: do_relax, calc_etot

  private

  real(dp), allocatable :: dists(:)
  integer, allocatable :: ind(:)

contains

  subroutine calc_etot ( params, struct )
    class(params_t), intent(in) :: params
    class(struct_t), intent(inout) :: struct

    integer :: n, icg, max_cg, info, nlayer, na_uc_l, il, ia_uc, ia_uc_l
    integer, parameter :: iopt = 2
    real(dp) :: cntrl(0:19), dxmax, gtol, f, z_avg, f_intra, f_inter
    real(dp), allocatable :: x(:), g(:), h(:,:), &
      atoms_uc_rlx(:,:,:), dtau(:,:,:)

    real(dp), external :: get_clock

    n = 3 * struct%na_uc
    nlayer = struct%nlayer
    na_uc_l = struct%na_uc_l
    max_cg = 3 * n
    dxmax = 0.1 * angstrom_au
    gtol = params%rlx_conv_thr

    allocate ( x(n), g(n), h(n,iopt) )
    allocate ( dists(struct%na_sc_l), ind(struct%na_sc_l) )
    x = 0.d0
    g = 0.d0
    f = 0.d0
    cntrl = 0

    icg = 0
    info = 1

    call calcfg ( params, struct, n, x, f, f_intra, f_inter, g )

    write(stdout, "(1x,A,F24.16)") 'Total Energy (eV) =', f*RYTOEV
    write(stdout, "(1x,A,2F24.16)") 'E(intra), E(inter) (eV) =', f_intra*RYTOEV, f_inter*RYTOEV
    write(stdout, "(1x,A,F24.16)") 'max|g| (eV/Ang) = ', maxval(abs(g))*RYTOEV*ANGSTROM_AU

    deallocate ( x, g, h )
    deallocate ( dists, ind )

  end subroutine calc_etot

  subroutine do_relax ( params, struct )
    class(params_t), intent(in) :: params
    class(struct_t), intent(inout) :: struct

    integer :: n, icg, max_cg, info, nlayer, na_uc_l, il, ia_uc, ia_uc_l
    integer, parameter :: iopt = 2
    real(dp) :: cntrl(0:19), dxmax, gtol, f, z_avg, f_intra, f_inter, gsum(3)
    real(dp), allocatable :: x(:), g(:), h(:,:), &
      atoms_uc_rlx(:,:,:), dtau(:,:,:)

    real(dp), external :: get_clock

    n = 3 * struct%na_uc
    nlayer = struct%nlayer
    na_uc_l = struct%na_uc_l
    max_cg = 3 * n
    dxmax = 0.1 * angstrom_au
    gtol = params%rlx_conv_thr

    allocate ( x(n), g(n), h(n,iopt) )
    allocate ( dists(struct%na_sc_l), ind(struct%na_sc_l) )
    x = 0.d0
    g = 0.d0
    f = 0.d0
    cntrl = 0

    icg = 0
    info = 1

    write(stdout, "(1x,a,F10.1,a)") "Start CG loop @ ", get_clock('ep')," s"

    write(stdout, "(1x,a10,4a20)") "CG step", "U (eV)", "U_intra (eV)", "U_inter (eV)", "max|g| (eV/Ang)"

    do while (.true.)

      icg = icg + 1

      call calcfg ( params, struct, n, x, f, f_intra, f_inter, g )

      write(stdout, "(1x,I10,4F20.8)") icg, f*RYTOEV, f_intra*RYTOEV, f_inter*RYTOEV, maxval(abs(g))* RYTOEV * ANGSTROM_AU

      call conjgr ( n, x, g, dxmax, gtol, cntrl, h )

      if (int(cntrl(0)) == 0) then
        info = 0
        exit
      endif

      if (icg > max_cg) then
        info = -1
        exit
      endif

    enddo

    if (info == -1) then
      call errore ('relax', 'CG iteration failed to converge', max_cg)
      return
    endif

    write(stdout, "(1x,a,F10.1,a)") "End CG loop @ ", get_clock('ep')," s"

    write(stdout, "(1x,A,F24.16)") 'Total Energy (eV) =', f*RYTOEV
    write(stdout, "(1x,A,F24.16)") 'max|g| (eV/Ang) = ', maxval(abs(g))*RYTOEV*ANGSTROM_AU

    allocate(atoms_uc_rlx(3, na_uc_l, nlayer))
    allocate(dtau(3, na_uc_l, nlayer))

    do il = 1, nlayer
      do ia_uc_l = 1, na_uc_l
        ia_uc = (il-1)*na_uc_l+ia_uc_l
        atoms_uc_rlx(1:3,ia_uc_l,il) = struct%atoms_uc(1:3,ia_uc_l,il)+x(3*(ia_uc-1)+1:3*(ia_uc-1)+3)
        dtau(1:3,ia_uc_l,il) = struct%dtau(1:3,ia_uc_l,il)+x(3*(ia_uc-1)+1:3*(ia_uc-1)+3)
      enddo
    enddo

    z_avg = sum(atoms_uc_rlx(3,:,:))/struct%na_uc
    atoms_uc_rlx(3,:,:) = atoms_uc_rlx(3,:,:)-z_avg
    if (verbosity>=VERBOSITY_INFO) then
      write(stdout, "(1x,A)") 'Relaxed atomic positions (Ang)'
      do il = 1, nlayer
        do ia_uc_l = 1, na_uc_l
          ia_uc = (il-1)*na_uc_l+ia_uc_l
          write(stdout, '(1x,I6,3F20.12)') ia_uc, atoms_uc_rlx(1:3,ia_uc_l,il)/ANGSTROM_AU
        enddo
      enddo
    endif

    write(stdout, "(1x,A)") 'Updating atomic structure...'
    call struct%update_atoms_uc(atoms_uc_rlx, dtau)
    call struct%to_file(trim(outdir)//'/struct.dat')

    deallocate ( x, g, h )
    deallocate ( dists, ind )
    deallocate ( atoms_uc_rlx )
    deallocate ( dtau)

  end subroutine do_relax

  ! calculates total energy (f) and force (g) at x
  ! note that g is -gradient of f at x
  subroutine calcfg(params,struct,n,x,f,f_intra,f_inter,g)
    class(params_t), intent(in) :: params
    class(struct_t), intent(in) :: struct

    integer, intent(in) :: n
    real(dp), intent(inout) :: x(n)
    real(dp), intent(out) :: f, g(n), f_intra, f_inter

    real(dp) :: rvec(3), res, dv(3), &
      maxg, tau1(3), tau2(3), x_avg(3), xj(3), xrot(3), g_avg(3), gj(3), grot(3), dvec(3), fc(3,3), dtau1, dtau2, gsum(3)
    integer :: &
      isym, ifc, il, ia_uc_l, ia_uc, jl, ja, ja_sc, ja_sc_l, ja_uc_l, ja_uc, &
      i, k, j, m1, m2, &
      inb, jj, R(2), start_ia, end_ia, nlayer, na_uc_l, na_uc, na_sc, na_sc_l

    call divide ( comm_world, struct%na_uc, start_ia, end_ia )

    nlayer = struct%nlayer
    na_uc = struct%na_uc
    na_uc_l = struct%na_uc_l
    na_sc = struct%na_sc
    na_sc_l = struct%na_sc_l

    g = 0.d0

    !! interlayer binding energy
    f_inter = 0.d0
    f_intra = 0.d0

    do ia_uc = start_ia, end_ia
      il = int((ia_uc-1)/na_uc_l)+1
      ia_uc_l = mod(ia_uc-1,na_uc_l)+1

      do jl = 1, nlayer
      ! do jl = il-1, il+1 ! vdw interaction only between adjacent layers
      !   if (jl<1 .or. jl>nlayer) cycle

        if (il==jl) then
          ! Intralayer force constants
          do ja_sc_l = 1, struct%na_sc_l
            dvec = struct%atoms_sc(1:3,ja_sc_l,jl)-struct%atoms_uc(1:3,ia_uc_l,il)
            dists(ja_sc_l) = norm2(dvec)
          enddo

          ind(1) = 0
          call hpsort_eps ( struct%na_sc_l, dists, ind, eps16)

          ! intralayer energy & force
          do inb = 2, NNB_INTRA+1 ! exclude the same atom
            ja_sc_l = ind(inb)
            ja_uc_l = struct%sc_uc_map(ja_sc_l,jl)
            ja_uc = na_uc_l*(jl-1)+ja_uc_l

            ! intralayer force constants should be evaluated at nonrelaxed positions
            dvec = struct%atoms_sc(1:3,ja_sc_l,jl)-struct%dtau(1:3,struct%sc_uc_map(ja_sc_l,jl),jl) &
              -(struct%atoms_uc(1:3,ia_uc_l,il)-struct%dtau(1:3,ia_uc_l,il))

            call fc_intra(inb-1, dvec, fc)

            do m1 = 1, 3
              do m2 = 1, 3
                i = 3*(ia_uc-1)+m1
                j = 3*(ja_uc-1)+m2

                dtau1 = x(i) + struct%dtau(m1,ia_uc_l,il)
                dtau2 = x(j) + struct%dtau(m2,ja_uc_l,jl)

                f_intra = f_intra + 0.5d0*fc(m1,m2)*dtau1*dtau2

                g(i) = g(i) - fc(m1,m2)*dtau2

                ! ASR
                j = 3*(ia_uc-1)+m2
                dtau2 = x(j) + struct%dtau(m2,ia_uc_l,il)
                f_intra = f_intra - 0.5d0*fc(m1,m2)*dtau1*dtau2
                g(i) = g(i) + fc(m1,m2)*dtau2
              enddo
            enddo
          enddo
        else
          ! interlayer energy & force
          do ja_sc_l = 1, struct%na_sc_l
            ja_uc_l = struct%sc_uc_map(ja_sc_l,jl)
            ja_uc = na_uc_l*(jl-1)+ja_uc_l

            rvec = struct%atoms_uc(1:3, ia_uc_l, il)-struct%atoms_sc(1:3, ja_sc_l, jl)

            if (norm2(rvec)>params%rcut_vdw) then
              cycle
            endif

            do m1 = 1, 3
              tau1(m1) = struct%atoms_uc(m1, ia_uc_l, il) + x(3*(ia_uc-1)+m1)
            enddo

            do m2 = 1, 3
              tau2(m2) = struct%atoms_sc(m2, ja_sc_l, jl) + x(3*(ja_uc-1)+m2)
            enddo

            rvec = tau1 - tau2

            call v_kc ( rvec, params%rcut_vdw, res )

            f_inter = f_inter + 0.5d0*res

            call dv_kc ( rvec, dv )

            do m1 = 1, 3
              i = 3*(ia_uc-1)+m1
              g(i) = g(i) - dv(m1)
            enddo
          enddo
        endif
      enddo

    enddo ! ia_uc

    call mp_sum ( f_inter, comm_world )
    call mp_sum ( f_intra, comm_world )
    call mp_sum ( g, comm_world )

    ! g(1:3) = 0.d0

    ! gsum = 0.0d0
    ! do il = 1, nlayer
    !   do ia_uc_l = 1, na_uc_l
    !     ia_uc = na_uc_l*(il-1)+ia_uc_l
    !     gsum = gsum + g(3*(ia_uc-1)+1:3*ia_uc)
    !   enddo
    ! enddo

    ! do il = 1, nlayer
    !   do ia_uc_l = 1, na_uc_l
    !     ia_uc = na_uc_l*(il-1)+ia_uc_l
    !     g(3*(ia_uc-1)+1:3*ia_uc) = g(3*(ia_uc-1)+1:3*ia_uc)-gsum
    !   enddo
    ! enddo

    ! symmetrize g
    ! if (struct%nsym > 0) then
    !   call errore('relax', 'do not use rlx_symmetrize_force', 1)
    !   do il = 1, nlayer
    !     do ia_uc_l = 1, na_uc_l
    !       ia_uc = na_uc_l*(il-1)+ia_uc_l

    !       x_avg = 0.d0
    !       g_avg = 0.d0
    !       do isym = 1, struct%nsym
    !         ja_uc = struct%sym_map(ia_uc_l,il,isym)
    !         jl = (ja_uc-1)/na_uc_l+1
    !         ja_uc_l = mod(ja_uc-1,na_uc_l)+1
    !         gj = g(3*(ja_uc-1)+1:3*(ja_uc-1)+3)
    !         xj = x(3*(ja_uc-1)+1:3*(ja_uc-1)+3)
    !         grot = matmul(struct%sr(:,:,struct%invs(isym)), gj)
    !         xrot = matmul(struct%sr(:,:,struct%invs(isym)), xj)
    !         g_avg = g_avg + grot
    !         x_avg = x_avg + xrot
    !       enddo
    !       g_avg = g_avg / struct%nsym
    !       x_avg = x_avg / struct%nsym

    !       do isym = 1, struct%nsym
    !         ja_uc = struct%sym_map(ia_uc_l,il,isym)
    !         jl = (ja_uc-1)/na_uc_l+1
    !         ja_uc = mod(ja_uc-1,na_uc_l)+1
    !         g(3*(ja_uc-1)+1:3*(ja_uc-1)+3) = matmul(struct%sr(:,:,isym), g_avg)
    !         x(3*(ja_uc-1)+1:3*(ja_uc-1)+3) = matmul(struct%sr(:,:,isym), x_avg)
    !       enddo
    !     enddo
    !   enddo
    ! endif

    maxg = maxval(abs(g))

    f = f_intra + f_inter

  end subroutine calcfg

end module relax
