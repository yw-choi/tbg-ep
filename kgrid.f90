module kgrid

  use mpi
  use mpi_pool

  use kinds, only: DP
  use constants, only: &
    TPI, ANGSTROM_AU, RYTOEV, eps16


  use lattice, only: struct_t

  use ktetra, only: tetra_init

  use input, only: params_t

  use utils, only: distribute_indices

  implicit none

  type, public :: kpoints_t

    integer :: &
      dimk,    & ! k-points grid dimension
      nkmax,   & ! nkmax=dimk*dimk
      nk

    real(dp), allocatable :: &
      xkg(:,:), & ! full k-point grid
      xk(:,:), & ! k-points in frac. cooridnate
      wk(:)

    integer, allocatable :: &
      equiv(:),  &
      equiv_s(:), &
      n_stars(:), &
      stars(:,:)

    real(dp) :: relatt(3,3)

    integer :: ntetra
    integer, allocatable :: tetra(:,:)

    type(mpi_pool_t) :: pool

    integer :: nk_pool
    integer :: startk
    integer :: endk
    integer, allocatable :: displs(:), recvcounts(:)

    contains

      procedure :: setup
      procedure :: dump
      procedure :: from_file

      procedure :: cleanup => kpoints_cleanup

  end type kpoints_t
  
  public :: get_kq

  private

contains

  subroutine setup(this, dimk, npool, use_symmetry, struct)
    class(kpoints_t), intent(inout) :: this
    integer, intent(in) :: dimk, npool
    logical, intent(in) :: use_symmetry
    class(struct_t), intent(in) :: struct

    integer :: ik, ik1, ik2, ik_equiv, ik_irr, itet, nsym
    real(dp) :: xk_cart(3), xkg(3)

    this%relatt = struct%relatt

    this%dimk = dimk
    this%nkmax = this%dimk*this%dimk
    allocate(this%xk(3,this%nkmax), this%wk(this%nkmax), this%xkg(3,this%nkmax))
    allocate(this%equiv(this%nkmax), this%equiv_s(this%nkmax))
    this%equiv = 0
    this%equiv_s = 0
    this%xk = 0.d0
    this%wk = 0.d0

    call kpoint_grid (struct%nsym, struct%time_reversal, .not.use_symmetry, &
      struct%s, struct%t_rev, this%nkmax, 0,0,0,this%dimk,this%dimk,1, this%nk, this%xkg, this%xk, this%wk, &
      this%equiv, this%equiv_s) 

    allocate(this%n_stars(this%nk))
    allocate(this%stars(2*struct%nsym,this%nk))

    this%n_stars = 0

    if (use_symmetry) then

      do ik = 1, this%nkmax
        ik_irr = this%equiv(ik)
        this%n_stars(ik_irr) = this%n_stars(ik_irr) + 1
        this%stars(this%n_stars(ik_irr),ik_irr) = ik 
      enddo

    else

      do ik = 1, this%nkmax
        this%n_stars(ik) = 1
        this%stars(1,ik) = ik
      enddo

    endif

    if (use_symmetry) then
      nsym = struct%nsym
    else
      nsym = 1
    endif

    call tetra_init (nsym, struct%s, struct%time_reversal, &
      struct%t_rev, struct%latt, struct%relatt/TPI, &
      this%nkmax, 0, 0, 0, this%dimk, this%dimk, 1, this%nk, this%xk, this%ntetra, this%tetra)

    if (npool > this%nk) then
      call errore('kgrid', 'npool > nk is not allowed', npool)
    endif

    call this%pool%setup( npool )

    allocate(this%displs(0:npool-1), this%recvcounts(0:npool-1))
    call distribute_indices( this%nk, npool, this%displs, this%recvcounts )
    this%startk = this%displs(this%pool%mypool) + 1
    this%endk = this%displs(this%pool%mypool) + this%recvcounts(this%pool%mypool)
    this%nk_pool = this%recvcounts(this%pool%mypool)

  end subroutine setup

  subroutine from_file(this, fn, npool)
    class(kpoints_t), intent(inout) :: this
    integer, intent(in) :: npool 
    character(len=256), intent(in) :: fn

    logical :: exst
    integer :: ik

    if (ionode) then

      inquire(file=trim(fn), exist=exst)
      if (.not.exst) then
        call errore('kgrid', 'kpath file not found', 1)
      endif

      open(11, file=trim(fn), form='formatted')
      read(11,*) this%nk
      this%nkmax = this%nk
      allocate(this%xk(3,this%nk),this%wk(this%nk))
      do ik = 1, this%nk
        read(11,*) this%xk(1:3,ik)
      enddo
      this%wk = 1.0d0/this%nk
      close(11)

    endif

    call mp_bcast(this%nk,0,comm_world)
    call mp_bcast(this%nkmax,0,comm_world)
    if (.not.ionode) then
      allocate(this%xk(3,this%nk),this%wk(this%nk))
    endif
    call mp_bcast(this%xk,0,comm_world)
    call mp_bcast(this%wk,0,comm_world)

    if (npool > this%nk) then
      call errore('kgrid', 'npool > nk is not allowed', npool)
    endif

    call this%pool%setup( npool )

    allocate(this%displs(0:npool-1), this%recvcounts(0:npool-1))
    call distribute_indices( this%nk, npool, this%displs, this%recvcounts )
    this%startk = this%displs(this%pool%mypool) + 1
    this%endk = this%displs(this%pool%mypool) + this%recvcounts(this%pool%mypool)
    this%nk_pool = this%recvcounts(this%pool%mypool)

  end subroutine from_file

  subroutine dump(this, data_dir, prefix)

    class(kpoints_t), intent(in) :: this
    character(len=*), intent(in) :: data_dir, prefix

    integer :: ik, ik1, ik2
    real(dp) :: xk_cart(3), xkg(3)

    character(len=512) :: fn

    if (ionode) then

      open(11, file=trim(data_dir)//'/'//trim(prefix)//'.dat', form="formatted")
      write(11, *) this%nkmax, this%nk
      do ik = 1, this%nk
        xk_cart = matmul(this%relatt,this%xk(:,ik))*ANGSTROM_AU
        write(11, "(4F10.6,1x,F12.8)") this%xk(1:2,ik), xk_cart(1:2), this%wk(ik)
      enddo
      close(11)

      open(11, file=trim(data_dir)//'/'//trim(prefix)//'_equiv.dat', form="formatted")
      write(11, *) this%nkmax
      do ik = 1, this%nkmax
        ik1 = int((ik-1)/this%dimk)+1
        ik2 = mod(ik-1,this%dimk)+1
        xkg(1) = dble(ik1-1)/this%dimk
        xkg(2) = dble(ik2-1)/this%dimk
        xkg(3) = 0.d0
        xk_cart = matmul(this%relatt,xkg)*ANGSTROM_AU
        write(11, "(4F10.6,I10,I5)") xkg(1:2), xk_cart(1:2), this%equiv(ik), this%equiv_s(ik)
      enddo
      close(11)

    endif

    call mp_barrier(comm_world)

  end subroutine dump

  subroutine kpoints_cleanup (this)
    class(kpoints_t), intent(inout) :: this

    call this%pool%cleanup()

    deallocate(this%xkg,this%xk,this%wk,this%equiv,this%equiv_s)
    deallocate(this%n_stars,this%stars,this%tetra)
    deallocate(this%displs,this%recvcounts)

  end subroutine kpoints_cleanup

  SUBROUTINE kpoint_grid ( nrot, time_reversal, skip_equivalence, s, t_rev, &
                           npk, k1,k2,k3, nk1,nk2,nk3, nks, xkg, xk, wk, equiv_irr, equiv_s)
  !-----------------------------------------------------------------------
  !
  !  Automatic generation of a uniform grid of k-points
  !
    USE kinds, ONLY: DP
    IMPLICIT NONE
    !
    INTEGER, INTENT(in):: nrot, npk, k1, k2, k3, nk1, nk2, nk3, &
                          t_rev(48), s(3,3,48)
    LOGICAL, INTENT(in):: time_reversal, skip_equivalence
    ! real(DP), INTENT(in):: bg(3,3)
    !
    INTEGER, INTENT(out) :: nks
    real(DP), INTENT(out):: xk(3,npk), xkg(3,npk)
    real(DP), INTENT(out):: wk(npk)
    INTEGER, INTENT(out):: equiv_irr(npk)
    INTEGER, INTENT(out):: equiv_s(npk)
    ! LOCAL:
    real(DP), PARAMETER :: eps=1.0d-5
    real(DP) :: xkr(3), fact, xx, yy, zz
    INTEGER, ALLOCATABLE:: equiv(:), ik2irr(:)
    real(DP), ALLOCATABLE:: wkk(:)
    INTEGER :: nkr, i,j,k, ns, n, nk
    LOGICAL :: in_the_list
    !
    nkr=nk1*nk2*nk3
    ALLOCATE (wkk(nkr),equiv(nkr),ik2irr(nkr))
    !
    DO i=1,nk1
       DO j=1,nk2
          DO k=1,nk3
             !  this is nothing but consecutive ordering
             n = (k-1) + (j-1)*nk3 + (i-1)*nk2*nk3 + 1
             !  xkg are the components of the complete grid in crystal axis
             xkg(1,n) = dble(i-1)/nk1 + dble(k1)/2/nk1
             xkg(2,n) = dble(j-1)/nk2 + dble(k2)/2/nk2
             xkg(3,n) = dble(k-1)/nk3 + dble(k3)/2/nk3
          ENDDO
       ENDDO
    ENDDO

    !  equiv(nk) =nk : k-point nk is not equivalent to any previous k-point
    !  equiv(nk)!=nk : k-point nk is equivalent to k-point equiv(nk)

    DO nk=1,nkr
       equiv(nk)=nk
    ENDDO

    IF ( skip_equivalence ) THEN
      ! CALL infomsg('kpoint_grid', 'ATTENTION: skip check of k-points equivalence')
      wkk = 1.d0
    ELSE
      DO nk=1,nkr
      !  check if this k-point has already been found equivalent to another
        IF (equiv(nk) == nk) THEN
          wkk(nk)   = 1.0d0
          !  check if there are equivalent k-point to this in the list
          !  (excepted those previously found to be equivalent to another)
          !  check both k and -k
          DO ns=1,nrot
             DO i=1,3
                xkr(i) = s(i,1,ns) * xkg(1,nk) &
                       + s(i,2,ns) * xkg(2,nk) &
                       + s(i,3,ns) * xkg(3,nk)
                xkr(i) = xkr(i) - nint( xkr(i) )
             ENDDO
             IF(t_rev(ns)==1) xkr = -xkr
             xx = xkr(1)*nk1 - 0.5d0*k1
             yy = xkr(2)*nk2 - 0.5d0*k2
             zz = xkr(3)*nk3 - 0.5d0*k3
             in_the_list = abs(xx-nint(xx))<=eps .and. &
                           abs(yy-nint(yy))<=eps .and. &
                           abs(zz-nint(zz))<=eps
             IF (in_the_list) THEN
                i = mod ( nint ( xkr(1)*nk1 - 0.5d0*k1 + 2*nk1), nk1 ) + 1
                j = mod ( nint ( xkr(2)*nk2 - 0.5d0*k2 + 2*nk2), nk2 ) + 1
                k = mod ( nint ( xkr(3)*nk3 - 0.5d0*k3 + 2*nk3), nk3 ) + 1
                n = (k-1) + (j-1)*nk3 + (i-1)*nk2*nk3 + 1
                IF (n>nk .and. equiv(n)==n) THEN
                   equiv(n) = nk
                   equiv_s(n) = ns
                   wkk(nk)=wkk(nk)+1.0d0
                ELSE
                   IF (equiv(n)/=nk .or. n<nk ) CALL errore('kpoint_grid', &
                      'something wrong in the checking algorithm',1)
                ENDIF
             ENDIF
             IF ( time_reversal ) THEN
                xx =-xkr(1)*nk1 - 0.5d0*k1
                yy =-xkr(2)*nk2 - 0.5d0*k2
                zz =-xkr(3)*nk3 - 0.5d0*k3
                in_the_list=abs(xx-nint(xx))<=eps.and.abs(yy-nint(yy))<=eps &
                                                   .and. abs(zz-nint(zz))<=eps
                IF (in_the_list) THEN
                   i = mod ( nint (-xkr(1)*nk1 - 0.5d0 * k1 + 2*nk1), nk1 ) + 1
                   j = mod ( nint (-xkr(2)*nk2 - 0.5d0 * k2 + 2*nk2), nk2 ) + 1
                   k = mod ( nint (-xkr(3)*nk3 - 0.5d0 * k3 + 2*nk3), nk3 ) + 1
                   n = (k-1) + (j-1)*nk3 + (i-1)*nk2*nk3 + 1
                   IF (n>nk .and. equiv(n)==n) THEN
                      equiv(n) = nk
                      equiv_s(n) = -ns
                      wkk(nk)=wkk(nk)+1.0d0
                   ELSE
                      IF (equiv(n)/=nk.or.n<nk) CALL errore('kpoint_grid', &
                      'something wrong in the checking algorithm',2)
                   ENDIF
                ENDIF
             ENDIF
          ENDDO
        ENDIF
      ENDDO
    ENDIF

    !  count irreducible points and order them
    nks=0
    fact=0.0d0
    DO nk=1,nkr
       IF (equiv(nk)==nk) THEN
          nks=nks+1
          ik2irr(nk) = nks
          IF (nks>npk) CALL errore('kpoint_grid','too many k-points',1)
          wk(nks) = wkk(nk)
          fact    = fact+wk(nks)
          !  bring back into to the first BZ
          DO i=1,3
             xk(i,nks) = xkg(i,nk) - floor(xkg(i,nk))
          ENDDO

       ENDIF
    ENDDO

    DO nk=1,nkr
      equiv_irr(nk) = ik2irr(equiv(nk))
    ENDDO

    !  go to cartesian axis (in units 2pi/a0)
    ! CALL cryst_to_cart(nks,xk,bg,1)
    !  normalize weights to one
    DO nk=1,nks
       wk(nk) = wk(nk)/fact
    ENDDO

    DEALLOCATE(wkk)

    RETURN
  END SUBROUTINE kpoint_grid

  subroutine get_kq(kpt, qpt, ik, iq, ikq)
    class(kpoints_t), intent(in) :: kpt, qpt
    integer, intent(in) :: ik, iq
    integer, intent(out) :: ikq

    integer :: ik1, ik2, iq1, iq2
    real(dp) :: ikq1, ikq2

    ! 0-base
    ik1 = int((ik-1)/kpt%dimk)
    ik2 = mod(ik-1, kpt%dimk)

    iq1 = int((iq-1)/qpt%dimk)
    iq2 = mod(iq-1, qpt%dimk)

    ikq1 = ik1+iq1*kpt%dimk/dble(qpt%dimk)
    ikq2 = ik2+iq2*kpt%dimk/dble(qpt%dimk)

    if (abs(ikq1-nint(ikq1))>eps16.or.abs(ikq2-nint(ikq2))>eps16) then
      call errore('get_kq', 'k and q grid is not commensurate', ik)
    endif

    if (ikq1.ge.kpt%dimk) then
      ikq1 = ikq1 - kpt%dimk
    endif

    if (ikq2.ge.kpt%dimk) then
      ikq2 = ikq2 - kpt%dimk
    endif

    ikq = int(ikq1)*kpt%dimk + int(ikq2+1) 

    ! print '(6I5,2F6.1,I5)', ik, ik1, ik2, iq, iq1, iq2, ikq1, ikq2, ikq
  end subroutine get_kq
end module kgrid
