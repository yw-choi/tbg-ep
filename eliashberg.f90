module eliashberg
  use mpi
  use mpi_pool
  use global_m, only: outdir, VERBOSITY_INFO, verbosity
  use kinds, only: dp
  use constants, only: TPI, eps16, RYTOEV, SQRTPM1, SQRT2
  use input, only: params_t
  use lattice, only: struct_t
  use kgrid, only: kpoints_t, get_kq
  use electrons_m, only: electrons_t
  use phonons_m, only: phonons_t
  use distrib_mat, only: distrib_mat_t, read_matrix, write_matrix
  use utils, only: distribute_indices
  use ktetra, only: tetra_weights
  use epmat_m, only: g2mat_reclen, &
    g2mat_filename, g2mat_file, &
    g2mat_filename2, g2mat_file2, &
    g2mat_filename3, g2mat_file3

  implicit none

  public

contains

  subroutine compute_eliashberg (params, electrons, phonons, g2_type)
    class(params_t), intent(in) :: params
    class(electrons_t), target, intent(inout) :: electrons
    class(phonons_t), target, intent(inout) :: phonons
    integer, intent(in) :: g2_type ! 1: all, 2: intra, 3: inter

    class(struct_t), pointer :: struct
    class(kpoints_t), pointer :: kpt, qpt

    type(distrib_mat_t) :: gmat
    real(dp), allocatable :: &
      g2(:,:), dwnk(:,:,:), N_F(:), efgrid(:), &
      lambda_nk(:,:,:), lambda_qv(:,:), &
      lambda_nk_tot(:), lambda_qv_tot(:), &
      a2F(:,:,:)

    integer, allocatable :: kqmap(:), displs(:), recvcounts(:)

    character(len=256) :: fn
    integer :: &
      na_uc, nbnd, nmode, nk_irr, nq_irr, nk_tot, nq_tot, nef, &
      nbk, nbk_l, nmode_l

    integer :: ik, ibnd, jbnd, ief, ibk_l, ibk_g, ikq, indx, &
      nu_l, nu_g, i, iw, ismear, iq_irr, iq_star, isym, istar, &
      start_nu, end_nu

    integer, parameter :: nw = 1000, nsmear = 7
    real(dp) :: wgrid(nw), smearings(nsmear), wmin, wmax, phdos, ef, &
      dw, dwvq, lqv2, wvq

    integer, external :: indxl2g
    real(dp), external :: get_clock

    struct => electrons%struct
    kpt => electrons%kpoints
    qpt => phonons%qpoints

    nbnd = electrons%nbnd
    nmode = phonons%nmode
    na_uc = struct%na_uc
    nk_irr = kpt%nk
    nk_tot = kpt%nkmax
    nq_irr = qpt%nk
    nq_tot = qpt%nkmax
    nef = params%nef

    nbk = nbnd * nbnd * nk_tot

    call gmat%setup_mat_2d(nbk, nmode, qpt%pool)
    nbk_l = gmat%Ml
    nmode_l = gmat%Nl

    allocate(efgrid(nef),N_F(nef),kqmap(nk_tot))
    allocate(dwnk(nbnd,nk_tot,nef))
    allocate(lambda_nk(nbnd,nk_tot,nef))
    allocate(lambda_nk_tot(nef))
    allocate(lambda_qv_tot(nef))
    allocate(g2(nbk_l,nmode_l))
    allocate(lambda_qv(nmode,nef))
    allocate(a2F(nsmear,nw,nef))

    allocate(displs(0:nprocs-1),recvcounts(0:nprocs-1))

    inquire(iolength=g2mat_reclen) g2
    g2mat_file = 201
    g2mat_file2 = 202
    g2mat_file3 = 203

    if (g2_type==1) then
      write(g2mat_filename, '(a,i0.5,a)') trim(outdir)//"/g2.", rank, ".dat"
      open(g2mat_file, file=trim(g2mat_filename), form="unformatted", &
        status="old", access="direct", recl=g2mat_reclen, action="read")
    else if (g2_type==2) then
      write(g2mat_filename2, '(a,i0.5,a)') trim(outdir)//"/g2.intra.", rank, ".dat"
      open(g2mat_file2, file=trim(g2mat_filename2), form="unformatted", &
        status="old", access="direct", recl=g2mat_reclen, action="read")
    else if (g2_type==3) then
      write(g2mat_filename3, '(a,i0.5,a)') trim(outdir)//"/g2.inter.", rank, ".dat"
      open(g2mat_file3, file=trim(g2mat_filename3), form="unformatted", &
        status="old", access="direct", recl=g2mat_reclen, action="read")
    else
      call errore('compute_eliashberg', 'invalid g2_type', 1)
    endif

    write(stdout, "(1x,a,F10.1,a)") "DOS weights @ ", get_clock('ep'), ' s'
    call start_clock('  eli-dwnk')
    do ief = 1, nef
      ef = params%efmin+(ief-1)*(params%efmax-params%efmin)/dble(nef-1)
      efgrid(ief) = ef
      call compute_dos_weights(nbnd, kpt, electrons%ef+ef, electrons%enk, dwnk(:,:,ief), N_F(ief))
      write(stdout,"(a,2F12.6)") 'ef, N_F = ', ef*RYTOEV, N_F(ief)/RYTOEV
    enddo
    call stop_clock('  eli-dwnk')

    ! smearing for frequency-dependent Eliashberg function
    smearings(1) = 0.1e-3/RYTOEV
    smearings(2) = 0.5e-3/RYTOEV
    smearings(3) = 1.0e-3/RYTOEV
    smearings(4) = 2.0e-3/RYTOEV
    smearings(5) = 3.0e-3/RYTOEV
    smearings(6) = 4.0e-3/RYTOEV
    smearings(7) = 5.0e-3/RYTOEV

    wmin = 0.d0
    wmax = 220 * 1e-3 / RYTOEV ! 220 meV
    do iw = 1, nw
      wgrid(iw) = wmin + (iw-1)*(wmax-wmin)/dble(nw-1)
    enddo

    write(stdout, "(1x,a,F10.1,a)") "Lambda calculations @ ", get_clock('ep'), ' s'

    lambda_nk = 0.d0
    lambda_qv_tot = 0.d0
    a2F = 0.d0

    do iq_irr = qpt%startk, qpt%endk
      write(stdout, "(3x,a,i5,a,i5,a,F10.1,a)") "iq_irr # ", iq_irr, "/", qpt%nk_pool, ' @ ', get_clock('ep'), ' s'

      do istar = 1, qpt%n_stars(iq_irr)
        iq_star = qpt%stars(istar, iq_irr)
        isym = qpt%equiv_s(iq_star)
        write(stdout, "(7x,a,i5,a,i5,a,i5,a,F10.1,a)") "istar, iq_star, isym # ", istar, ',', iq_star, ',', isym, ' @ ', get_clock('ep'), ' s'

        call start_clock('  eli-read_g2')
        if (g2_type==1) then
          read(g2mat_file,rec=iq_star) g2
        else if (g2_type==2) then
          read(g2mat_file2,rec=iq_star) g2
        else if (g2_type==3) then
          read(g2mat_file3,rec=iq_star) g2
        else
          call errore('compute_eliashberg', 'invalid g2_type', 1)
        endif

        if (verbosity>=VERBOSITY_INFO) then
          write(stdout, "(7x,a,f16.8)") "sum(g2) = ", sum(g2)
        end if
        call stop_clock('  eli-read_g2')

        ! multiply phonon length factor considering acoustic cutoff
        ! Note that lqv2 = 1/(2*M*wvq^2).
        ! This includes additional 1/wvq factors in lambda calculations
        call start_clock('  eli-lqv2')
        do nu_l = 1, nmode_l
          nu_g = indxl2g(nu_l, gmat%Nb, qpt%pool%mypcol, 0, qpt%pool%npcol)
          if (nu_g<1.or.nu_g>nmode) cycle
          wvq = phonons%wvq(nu_g, iq_irr)

          if (wvq < params%acoustic_cut) then
            g2(:,nu_l) = 0.d0
          else
            lqv2 = 1.d0/(2.d0*phonons%mass*phonons%wvq2(nu_g, iq_irr))
            g2(:,nu_l) =lqv2*g2(:,nu_l)
          endif
        enddo
        call stop_clock('  eli-lqv2')

        call start_clock('  eli-kqmap')
        ! pre-compute kqmap
        do ik = 1, nk_tot
          call get_kq(kpt, qpt, ik, iq_star, kqmap(ik))
        enddo
        call stop_clock('  eli-kqmap')

        call start_clock('  eli-lambda')
        lambda_qv = 0.d0
        do ief = 1, nef
          do ibk_l = 1, nbk_l
            ibk_g = indxl2g(ibk_l, gmat%Mb, qpt%pool%myprow, 0, qpt%pool%nprow)
            if (ibk_g<1.or.ibk_g>nbk) cycle
            ! Note: ibk_g = ibnd + nbnd*(jbnd-1) + nbnd*nbnd*(ik-1)
            ik = int((ibk_g-1)/(nbnd*nbnd))+1
            ikq = kqmap(ik)
            indx = mod(ibk_g-1, nbnd*nbnd)+1
            jbnd = int((indx-1)/nbnd)+1
            ibnd = mod(indx-1, nbnd)+1

            lambda_nk(ibnd,ik,ief) = lambda_nk(ibnd,ik,ief) + 2.d0*N_F(ief)*nk_tot/dble(nq_tot)*sum(g2(ibk_l,:))*dwnk(jbnd,ikq,ief)
            do nu_l = 1, nmode_l
              nu_g = indxl2g(nu_l, gmat%Nb, qpt%pool%mypcol, 0, qpt%pool%npcol)
              if (nu_g<1.or.nu_g>nmode) cycle

              ! lambda_qv(nu_g,ief) = lambda_qv(nu_g,ief) + dwnk(ibnd,ik,ief)*dwnk(jbnd,ikq,ief)
              lambda_qv(nu_g,ief) = lambda_qv(nu_g,ief) + 2.d0*N_F(ief)*nk_tot*g2(ibk_l,nu_l)*dwnk(ibnd,ik,ief)*dwnk(jbnd,ikq,ief)

            enddo
          enddo ! ibk_l


          if (verbosity>=VERBOSITY_INFO) then
            write(stdout, "(7x,a,i6,2f16.8)") "ief, sum(lambda_qv), sum(dwnk) = ", ief, sum(lambda_qv(:,ief)), sum(dwnk(:,:,ief))
          end if
        enddo ! ief

        call mp_sum ( lambda_qv, qpt%pool%comm )

        call stop_clock('  eli-lambda')

        call start_clock('  eli-io_lambda_qv')
        if (g2_type==1) then
          write(fn, '(a,i0.5,a)') trim(outdir)//'/lambda_qv.iq.',iq_star,'.dat'
        else if (g2_type==2) then
          write(fn, '(a,i0.5,a)') trim(outdir)//'/lambda_qv.intra.iq.',iq_star,'.dat'
        else if (g2_type==3) then
          write(fn, '(a,i0.5,a)') trim(outdir)//'/lambda_qv.inter.iq.',iq_star,'.dat'
        endif
        if (qpt%pool%rank==0) then
          open(11, file=trim(fn), form='unformatted')
          write(11) lambda_qv
          close(11)
        endif
        call stop_clock('  eli-io_lambda_qv')


        call mp_barrier ( qpt%pool%comm )

        do ief = 1, nef
          lambda_qv_tot(ief) = lambda_qv_tot(ief) + sum(lambda_qv(:,ief))/nq_tot
        enddo

        call divide ( qpt%pool%comm, nmode, start_nu, end_nu )

        call start_clock('  eli-a2F')
        do nu_g = start_nu, end_nu
          do ief = 1, nef
            do iw = 1, nw
              dw = wgrid(iw)-phonons%wvq(nu_g,iq_irr)
              do ismear = 1, nsmear
                dwvq = gaussian(dw/smearings(ismear), smearings(ismear))

                a2F(ismear,iw,ief) = a2F(ismear,iw,ief) + phonons%wvq(nu_g,iq_irr) * lambda_qv(nu_g,ief) * dwvq

              enddo
            enddo
          enddo
        enddo
        call stop_clock('  eli-a2F')
      enddo
    enddo ! iq_irr

    a2F = a2F * 0.5d0 / nq_tot
    call mp_sum ( a2F, qpt%pool%comm )
    call mp_sum ( a2F, qpt%pool%inter_comm )

    call mp_sum ( lambda_nk, comm_world )
    call mp_sum ( lambda_qv_tot, qpt%pool%inter_comm )

    do ief = 1, nef
      lambda_nk_tot(ief) = sum(lambda_nk(:,:,ief)*dwnk(:,:,ief))
    enddo

    write(stdout, "(1x,a,F10.1,a)") "Writing Eliashberg quantities @ ", get_clock('ep'), ' s'

    call start_clock('  eli-io')
    if (ionode) then
      if (g2_type==1) then
        write(fn, '(a)') trim(outdir)//'/lambda_tot.dat'
      else if (g2_type==2) then
        write(fn, '(a)') trim(outdir)//'/lambda_tot.intra.dat'
      else if (g2_type==3) then
        write(fn, '(a)') trim(outdir)//'/lambda_tot.inter.dat'
      endif
      open(11, file=trim(fn), form='formatted', status='replace')
      do ief = 1, nef
        write(11,'(I5,4F16.8)') ief, efgrid(ief)*RYTOEV, N_F(ief)/RYTOEV, lambda_nk_tot(ief), lambda_qv_tot(ief)
      enddo
      close(11)

      write(fn, '(a)') trim(outdir)//'/dwnk.dat'
      open(11, file=trim(fn), form='unformatted', status='replace')
      write(11) dwnk
      close(11)

      if (g2_type==1) then
        write(fn, '(a)') trim(outdir)//'/lambda_nk.dat'
      else if (g2_type==2) then
        write(fn, '(a)') trim(outdir)//'/lambda_nk.intra.dat'
      else if (g2_type==3) then
        write(fn, '(a)') trim(outdir)//'/lambda_nk.inter.dat'
      endif

      open(11, file=trim(fn), form='unformatted', status='replace')
      write(11) lambda_nk
      close(11)

      if (g2_type==1) then
        write(fn, '(a)') trim(outdir)//'/a2F.dat'
      else if (g2_type==2) then
        write(fn, '(a)') trim(outdir)//'/a2F.intra.dat'
      else if (g2_type==3) then
        write(fn, '(a)') trim(outdir)//'/a2F.inter.dat'
      endif
      open(11, file=trim(fn), form='formatted', status='replace')
      write(11, *) nsmear, nw, nef
      do ief = 1, nef
        write(11, *) ief, efgrid(ief)*RYTOEV, lambda_nk_tot(ief)
        do iw = 1, nw
          write(11,'(F16.8)',advance='no') wgrid(iw)*RYTOEV
          do ismear = 1, nsmear
            write(11,'(F16.8)',advance='no') a2F(ismear,iw,ief)
          enddo
          write(11,*)
        enddo
      enddo
      close(11)
    endif
    call stop_clock('  eli-io')

    call mp_barrier ( comm_world )

    write(stdout, "(1x,a,F10.1,a)") "End of Eliashberg calculations @ ", get_clock('ep'), ' s'

    deallocate(efgrid,N_F,g2)
    deallocate(dwnk)
    deallocate(lambda_nk)
    deallocate(lambda_nk_tot)
    deallocate(lambda_qv)
    deallocate(lambda_qv_tot)

  end subroutine compute_eliashberg

  subroutine compute_dos_weights (nbnd, kpt, ef, eig, dwnk, N_F)
    integer, intent(in) :: nbnd
    class(kpoints_t), intent(in) :: kpt
    real(dp), intent(in) :: ef, eig(nbnd,kpt%nk)
    real(dp), intent(out) :: dwnk(nbnd,kpt%nkmax), N_F

    integer :: i, ibnd, ik, ik_irr
    character(len=100) :: fn
    real(dp), allocatable :: wnk_irr(:,:), dwnk_irr(:,:)
    real(dp) :: ef1

    allocate(wnk_irr(nbnd,kpt%nk))
    allocate(dwnk_irr(nbnd,kpt%nk))

    call tetra_weights (kpt%nk, nbnd, &
      kpt%ntetra, kpt%tetra, &
      eig, ef, wnk_irr, dwnk_irr)

    ! DOS per spin at ef
    N_F = sum(dwnk_irr)

    dwnk_irr = dwnk_irr / N_F

    if (N_F.lt.eps16) then
      dwnk = 0.d0
    else
      do ik = 1, kpt%nkmax
        ik_irr = kpt%equiv(ik)
        dwnk(:,ik) = dwnk_irr(:,ik_irr)/kpt%n_stars(ik_irr)
      enddo
    endif

  end subroutine compute_dos_weights

  real(dp) function gaussian (x, sig)
    ! x = dx/sig
    real(dp), intent(in) :: x, sig

    gaussian = sqrtpm1/(sqrt2*sig) * exp(-0.5d0 * x*x)
  end function gaussian

end module eliashberg
