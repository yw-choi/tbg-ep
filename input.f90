module input
  use mpi
  use kinds, only: dp
  use constants, only: RYTOEV, ANGSTROM_AU, AMU_RY

  implicit none

  type, public :: params_t

    ! General
    integer            :: verbosity
    character(len=256) :: outdir
    logical            :: use_symmetry
    integer            :: solver

    ! Atomic Structure
    logical            :: read_struct
    character(len=256) :: struct_fn
    integer            :: struct_type

    ! Structural Relaxation
    logical            :: lrlx
    integer            :: nlayer1
    integer            :: nlayer2
    integer            :: M
    integer            :: N
    real(dp)           :: alat
    real(dp)           :: d_layer
    real(dp)           :: shift(2)
    real(dp)           :: rcut_vdw
    real(dp)           :: rlx_conv_thr
    logical            :: rlx_symmetrize_force

    ! Tight-Binding Calculation
    logical            :: ltb
    logical            :: read_electrons
    character(len=256) :: tbdir
    real(dp)           :: rcut_tb
    real(dp)           :: efield
    real(dp)           :: delta
    integer            :: lowest_band
    integer            :: highest_band
    integer            :: dimk
    integer            :: nkpool
    logical            :: write_cnk_grid
    logical            :: write_cnk_path
    character(len=256) :: kpath_file
    integer            :: nkpool_path
    integer            :: edos_ne
    real(dp)           :: edos_emin
    real(dp)           :: edos_emax
    logical            :: analyze_wfn

    ! Phonon
    logical            :: lph
    logical            :: read_phonons
    character(len=256) :: phdir
    real(dp)           :: rcut_ph
    real(dp)           :: mass
    integer            :: dimq
    integer            :: nqpool
    logical            :: write_evq_grid
    logical            :: write_evq_path
    character(len=256) :: qpath_file
    integer            :: nqpool_path
    integer            :: phdos_ne
    real(dp)           :: phdos_emin
    real(dp)           :: phdos_emax

    ! epmat
    logical            :: lepmat
    real(dp)           :: rcut_ep
    integer            :: start_iq

    ! eliashberg
    logical            :: leliashberg
    real(dp)           :: acoustic_cut
    integer            :: nef
    real(dp)           :: efmin
    real(dp)           :: efmax

    contains

      procedure :: read => read_input

  end type params_t

  private
  save

  ! Default values
  ! General
  integer            :: verbosity = 1
  character(len=256) :: outdir = 'data'
  logical            :: use_symmetry = .false.
  integer            :: solver

  ! Atomic Structure
  logical            :: read_struct = .false.
  character(len=256) :: struct_fn = ''
  integer            :: struct_type = 1

  logical            :: lrlx = .false.
  integer            :: nlayer1 = 1
  integer            :: nlayer2 = 1
  integer            :: M = 1
  integer            :: N = 0
  real(dp)           :: alat = 2.46d0 * ANGSTROM_AU
  real(dp)           :: d_layer = 3.35d0 * ANGSTROM_AU
  real(dp)           :: shift(2) = 0.d0
  real(dp)           :: rcut_vdw = 30d0 * ANGSTROM_AU
  real(dp)           :: rlx_conv_thr = 1d-3
  logical            :: rlx_symmetrize_force = .false.

  logical            :: ltb = .false.
  logical            :: read_electrons = .false.
  character(len=256) :: tbdir = 'data'
  real(dp)           :: rcut_tb = 10.d0 * ANGSTROM_AU
  real(dp)           :: efield = 0.d0
  real(dp)           :: delta = 0.d0
  integer            :: lowest_band = -1
  integer            :: highest_band = -1
  integer            :: dimk = 1
  integer            :: nkpool = 1
  integer            :: nkpool_path = 1
  logical            :: write_cnk_grid
  logical            :: write_cnk_path
  character(len=256) :: kpath_file = ''
  integer            :: edos_ne = 0
  real(dp)           :: edos_emin = 0.d0
  real(dp)           :: edos_emax = 0.d0
  logical            :: analyze_wfn = .false.

  logical            :: lph = .false.
  logical            :: read_phonons = .false.
  character(len=256) :: phdir = 'data'
  real(dp)           :: rcut_ph = 30.d0 * ANGSTROM_AU
  integer            :: dimq = 1
  integer            :: nqpool = 1
  integer            :: nqpool_path = 1
  real(dp)           :: mass = 12.0107 * AMU_RY
  logical            :: write_evq_grid
  logical            :: write_evq_path
  character(len=256) :: qpath_file = ''
  integer            :: phdos_ne = 0
  real(dp)           :: phdos_emin = 0.d0
  real(dp)           :: phdos_emax = 0.d0

  logical            :: lepmat = .false.
  real(dp)           :: rcut_ep = 5.d0 * ANGSTROM_AU
  integer            :: start_iq = 1

  logical            :: leliashberg = .false.
  real(dp)           :: acoustic_cut = 1e-3 * RYTOEV
  integer            :: nef = 0
  real(dp)           :: efmin = 0.d0
  real(dp)           :: efmax = 0.d0

contains

  subroutine read_input(this)
    class(params_t), intent(out) :: this

    logical :: exst
    logical, external :: makedirqq

    namelist/input/ &
      verbosity              , &
      outdir                 , &
      use_symmetry           , &
      solver                 , &
      read_struct            , &
      struct_fn              , &
      struct_type            , &
      lrlx                   , &
      nlayer1                , &
      nlayer2                , &
      M                      , &
      N                      , &
      alat                   , &
      d_layer                , &
      shift                  , &
      rcut_vdw               , &
      rlx_conv_thr           , &
      rlx_symmetrize_force   , &
      ltb                    , &
      read_electrons         , &
      tbdir                  , &
      rcut_tb                , &
      efield                 , &
      delta                  , &
      lowest_band            , &
      highest_band           , &
      dimk                   , &
      nkpool                 , &
      nkpool_path            , &
      write_cnk_grid         , &
      write_cnk_path         , &
      kpath_file             , &
      edos_ne                , &
      edos_emin              , &
      edos_emax              , &
      analyze_wfn            , &
      lph                    , &
      read_phonons           , &
      phdir                  , &
      rcut_ph                , &
      dimq                   , &
      nqpool                 , &
      nqpool_path            , &
      write_evq_grid         , &
      write_evq_path         , &
      mass                   , &
      qpath_file             , &
      phdos_ne               , &
      phdos_emin             , &
      phdos_emax             , &
      lepmat                 , &
      rcut_ep                , &
      start_iq               , &
      leliashberg            , &
      acoustic_cut           , &
      nef                    , &
      efmin                  , &
      efmax

    if (ionode) then
      read(5, input)
      write(6, input)

      inquire(directory=trim(outdir), exist=exst)

      if (.not.exst) then
        if (.not.makedirqq(outdir)) then
          call errore('input', 'Failed to create data directory', 1)
        endif
      endif

      alat = alat * ANGSTROM_AU
      d_layer = d_layer * ANGSTROM_AU
      shift = shift * ANGSTROM_AU
      rcut_vdw = rcut_vdw * ANGSTROM_AU
      rlx_conv_thr = rlx_conv_thr / RYTOEV / ANGSTROM_AU

      rcut_tb = rcut_tb * ANGSTROM_AU
      efield = efield / RYTOEV / ANGSTROM_AU
      delta = delta / RYTOEV
      edos_emin = edos_emin / RYTOEV
      edos_emax = edos_emax / RYTOEV

      rcut_ph = rcut_ph * ANGSTROM_AU
      mass = mass * AMU_RY
      phdos_emin = phdos_emin / RYTOEV
      phdos_emax = phdos_emax / RYTOEV

      rcut_ep = rcut_ep * ANGSTROM_AU
      acoustic_cut = acoustic_cut / RYTOEV
      efmin = efmin / RYTOEV
      efmax = efmax / RYTOEV
    endif

    call mp_bcast(verbosity             , 0, comm_world)
    call mp_bcast(outdir                , 0, comm_world)
    call mp_bcast(use_symmetry          , 0, comm_world)
    call mp_bcast(solver                , 0, comm_world)
    call mp_bcast(read_struct           , 0, comm_world)
    call mp_bcast(struct_fn             , 0, comm_world)
    call mp_bcast(struct_type           , 0, comm_world)
    call mp_bcast(lrlx                  , 0, comm_world)
    call mp_bcast(nlayer1               , 0, comm_world)
    call mp_bcast(nlayer2               , 0, comm_world)
    call mp_bcast(M                     , 0, comm_world)
    call mp_bcast(N                     , 0, comm_world)
    call mp_bcast(alat                  , 0, comm_world)
    call mp_bcast(d_layer               , 0, comm_world)
    call mp_bcast(shift                 , 0, comm_world)
    call mp_bcast(rcut_vdw              , 0, comm_world)
    call mp_bcast(rlx_conv_thr          , 0, comm_world)
    call mp_bcast(rlx_symmetrize_force  , 0, comm_world)
    call mp_bcast(ltb                   , 0, comm_world)
    call mp_bcast(read_electrons        , 0, comm_world)
    call mp_bcast(tbdir                 , 0, comm_world)
    call mp_bcast(rcut_tb               , 0, comm_world)
    call mp_bcast(efield                , 0, comm_world)
    call mp_bcast(delta                 , 0, comm_world)
    call mp_bcast(lowest_band           , 0, comm_world)
    call mp_bcast(highest_band          , 0, comm_world)
    call mp_bcast(dimk                  , 0, comm_world)
    call mp_bcast(nkpool                , 0, comm_world)
    call mp_bcast(nkpool_path           , 0, comm_world)
    call mp_bcast(write_cnk_grid        , 0, comm_world)
    call mp_bcast(write_cnk_path        , 0, comm_world)
    call mp_bcast(kpath_file            , 0, comm_world)
    call mp_bcast(edos_ne               , 0, comm_world)
    call mp_bcast(edos_emin             , 0, comm_world)
    call mp_bcast(edos_emax             , 0, comm_world)
    call mp_bcast(analyze_wfn           , 0, comm_world)
    call mp_bcast(lph                   , 0, comm_world)
    call mp_bcast(read_phonons          , 0, comm_world)
    call mp_bcast(phdir                 , 0, comm_world)
    call mp_bcast(rcut_ph               , 0, comm_world)
    call mp_bcast(dimq                  , 0, comm_world)
    call mp_bcast(nqpool                , 0, comm_world)
    call mp_bcast(nqpool_path           , 0, comm_world)
    call mp_bcast(write_evq_grid        , 0, comm_world)
    call mp_bcast(write_evq_path        , 0, comm_world)
    call mp_bcast(mass                  , 0, comm_world)
    call mp_bcast(qpath_file            , 0, comm_world)
    call mp_bcast(phdos_ne              , 0, comm_world)
    call mp_bcast(phdos_emin            , 0, comm_world)
    call mp_bcast(phdos_emax            , 0, comm_world)
    call mp_bcast(lepmat                , 0, comm_world)
    call mp_bcast(rcut_ep               , 0, comm_world)
    call mp_bcast(start_iq              , 0, comm_world)
    call mp_bcast(leliashberg           , 0, comm_world)
    call mp_bcast(acoustic_cut          , 0, comm_world)
    call mp_bcast(nef                   , 0, comm_world)
    call mp_bcast(efmin                 , 0, comm_world)
    call mp_bcast(efmax                 , 0, comm_world)

    this%verbosity            = verbosity
    this%outdir               = outdir
    this%use_symmetry         = use_symmetry
    this%solver               = solver
    this%read_struct          = read_struct
    this%struct_fn            = struct_fn
    this%struct_type          = struct_type
    this%lrlx                 = lrlx
    this%nlayer1              = nlayer1
    this%nlayer2              = nlayer2
    this%M                    = M
    this%N                    = N
    this%alat                 = alat
    this%d_layer              = d_layer
    this%shift                = shift
    this%rcut_vdw             = rcut_vdw
    this%rlx_conv_thr         = rlx_conv_thr
    this%rlx_symmetrize_force = rlx_symmetrize_force
    this%ltb                  = ltb
    this%read_electrons       = read_electrons
    this%tbdir                = tbdir
    this%rcut_tb              = rcut_tb
    this%efield               = efield
    this%delta                = delta
    this%lowest_band          = lowest_band
    this%highest_band         = highest_band
    this%dimk                 = dimk
    this%nkpool               = nkpool
    this%nkpool_path          = nkpool_path
    this%write_cnk_grid       = write_cnk_grid
    this%write_cnk_path       = write_cnk_path
    this%kpath_file           = kpath_file
    this%edos_ne              = edos_ne
    this%edos_emin            = edos_emin
    this%edos_emax            = edos_emax
    this%analyze_wfn          = analyze_wfn
    this%lph                  = lph
    this%read_phonons         = read_phonons
    this%phdir                = phdir
    this%rcut_ph              = rcut_ph
    this%dimq                 = dimq
    this%write_evq_grid       = write_evq_grid
    this%write_evq_path       = write_evq_path
    this%nqpool               = nqpool
    this%nqpool_path          = nqpool_path
    this%mass                 = mass
    this%qpath_file           = qpath_file
    this%phdos_ne             = phdos_ne
    this%phdos_emin           = phdos_emin
    this%phdos_emax           = phdos_emax
    this%lepmat               = lepmat
    this%rcut_ep              = rcut_ep
    this%start_iq             = start_iq
    this%leliashberg          = leliashberg
    this%acoustic_cut         = acoustic_cut
    this%nef                  = nef
    this%efmin                = efmin
    this%efmax                = efmax

  end subroutine read_input

end module input
