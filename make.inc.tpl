#
###############################
# palm
###############################
FC=mpiifort 

ELPA_HOME=${HOME}/opt/elpa/elpa-2018.11.001

IFLAGS=-I. \
	 -I${ELPA_HOME}/include/elpa-2018.11.001/elpa \
	 -I${ELPA_HOME}/include/elpa-2018.11.001/modules
FCFLAGS=-fpp -traceback -D__MPI -D__ELPA -mkl=cluster -O3
LDFLAGS=

LIBS=${ELPA_HOME}/lib/libelpa.a -L${MKLROOT}/lib/intel64 \
		 -mkl=cluster -lmkl_intel_lp64 -lmkl_sequential \
     -lmkl_core -lmkl_scalapack_lp64 -lmkl_blacs_intelmpi_lp64 -lpthread -lm -ldl

###############################
# OSX
###############################
# FC=mpifort -fc=ifort
# IFLAGS=-I.
# FCFLAGS=-fpp -traceback -D__MPI -mkl=sequential -O3
# LDFLAGS=
# LIBS= -Wl,-rpath,${MKLROOT}/lib -lmkl_scalapack_lp64 -lmkl_blacs_mpich_lp64 -lpthread -lm -ldl

CC=icc

TODIR=${PWD}

# ======================================================================
# And now the general rules, these should not require modification
# ======================================================================
.SUFFIXES :
.SUFFIXES : .o .c .f .f90

# General rule for building prog from prog.o; $^ (GNU extension) is
# used in order to list additional object files on which the
# executable depends
%: %.o
	$(FC) $(IFLAGS) $(FCFLAGS) $(LIBS) -o $@ $^ $(LDFLAGS)

# General rules for building prog.o from prog.f90 or prog.F90; $< is
# used in order to list only the first prerequisite (the source file)
# and not the additional prerequisites such as module or include files
%.o: %.f90
	$(FC) $(IFLAGS) $(FCFLAGS) -c $<
%.o: %.F90
	$(FC) $(IFLAGS) $(FCFLAGS) -c $<
%.o: %.f
	$(FC) $(IFLAGS) $(FCFLAGS) -c $<
%.o: %.F
	$(FC) $(IFLAGS) $(FCFLAGS) -c $<
.c.o:
	$(CC) $(CFLAGS) -c $<
