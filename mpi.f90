module mpi

  use mp, only: mp_bcast, mp_barrier, mp_start, mp_end, mp_allgather, mp_sum

  implicit none
  public 
  
  include 'mpif.h'
  
  integer :: nprocs
  integer :: rank
  integer :: comm_world
  integer :: stdout

  logical :: ionode

  integer :: mpierr

contains

  subroutine mpi_world_start

    comm_world = MPI_Comm_world

    call MPI_Init ( mpierr )

    call mp_start (nprocs, rank, comm_world)

    ionode = ( rank == 0 )

    if (ionode) then
      stdout = 6
    else
      stdout = 999
      open(newunit=stdout,file='/dev/null',action='write')
    endif

  end subroutine mpi_world_start

  subroutine mpi_world_end

    call mp_barrier ( comm_world )
    call mp_end ( comm_world, .true. )
    call MPI_Finalize ( mpierr )

  end subroutine mpi_world_end

  subroutine mpi_check_call(ierr)
    integer, intent(in) :: ierr
    integer :: nerr, resultlen
    character(len=mpi_max_error_string) :: serr

    if(ierr /= mpi_success) then
      call mpi_error_string(ierr,serr,resultlen,nerr)
      write(6,'(a)') trim(serr)
      call MPI_Abort(comm_world,1,mpierr)
      stop
    end if

  end subroutine mpi_check_call
end module mpi
