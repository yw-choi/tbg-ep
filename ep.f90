program ep

  use mpi
  use kinds, only: dp
  use input, only: params_t
  use utils, only: printblock_start
  use global_m, only: verbosity, outdir, use_symmetry, VERBOSITY_INFO, LOG_FILE
  use lattice, only: struct_t
  use kgrid, only: kpoints_t
  use electrons_m, only: electrons_t
  use phonons_m, only: phonons_t
  use relax, only: do_relax, calc_etot
  use version, only: commit
  use epmat_m, only: compute_epmat
  use eliashberg, only: compute_eliashberg

  implicit none

  type(params_t) :: params
  type(struct_t), target :: struct

  type(kpoints_t), target :: kpoints, qpoints, kpath, qpath
  type(electrons_t) :: electrons, electrons_path
  type(phonons_t) :: phonons, phonons_path
  character(len=256) :: log_filename
  real(dp), external :: get_clock

  call mpi_world_start
  call init_clocks ( .true. )
  call start_clock ( 'ep' )

  call printblock_start('EP')
  write(stdout, "(1x,a)") 'Electron-Phonon Coupling in Twisted Graphene Layers'
  write(stdout, "(1x,a)") 'written by Young Woo Choi in 2018-2019'
  write(stdout, "(1x,a,a)") 'git commit version: ', commit

  call printblock_start('MPI info')
  if (ionode) then
    print '(1x,a,I,a)', 'Running on ', nprocs, ' procs'
  endif
  !
  !----------------------
  ! Input
  !----------------------
  !
  call printblock_start('Input Parameters')
  call params%read()

  verbosity = params%verbosity
  outdir = params%outdir
  use_symmetry = params%use_symmetry

  if (verbosity>=VERBOSITY_INFO) then
    write(log_filename, '(a,i0.5)') "log.proc.", rank
    open(LOG_FILE, file=trim(log_filename), form="formatted", status="replace")
  endif
  !
  !----------------------
  ! Lattice
  !----------------------
  !
  call printblock_start('Lattice Structure')
  call start_clock ( 'lattice' )

  if (params%read_struct) then
    call struct%from_file ( params )
  else
    call struct%from_tbg_unitcell ( params )
    call struct%to_file(trim(outdir)//'/struct.dat')
  endif
  ! Print the total energy.
  call calc_etot ( params, struct )
  call stop_clock ( 'lattice' )
  !
  !----------------------
  ! Structural Relaxation
  !----------------------
  !
  if (params%lrlx) then
    call printblock_start ('Structural Relaxation')
    call start_clock ('relax')
    call do_relax ( params, struct )
    call stop_clock ('relax')
  endif
  !
  !----------------------
  ! k,q-grid
  !----------------------
  !
  call printblock_start ('k,q-point grid')
  call kpoints%setup(params%dimk, params%nkpool, params%use_symmetry, struct)
  call qpoints%setup(params%dimq, params%nqpool, params%use_symmetry, struct)
  !
  if (ionode) then
      print *, "k-grid dimension = ", kpoints%dimk, "x", kpoints%dimk
      print *, "Number of k-points = ", kpoints%dimk*kpoints%dimk
      if (params%use_symmetry) then
        print *, "Number of irreducible k-points = ", kpoints%nk
      endif
      print *, "Number of k-point pools = ", params%nkpool

      print *, "q-grid dimension = ", qpoints%dimk, "x", qpoints%dimk
      print *, "Number of q-points = ", qpoints%dimk*qpoints%dimk
      if (params%use_symmetry) then
        print *, "Number of irreducible q-points = ", qpoints%nk
      endif
      print *, "Number of q-point pools = ", params%nqpool
      print *, ''
  endif
  call kpoints%dump(params%outdir,'kpoints')
  call qpoints%dump(params%outdir,'qpoints')
  !
  !
  !----------------------
  ! Electrons
  !----------------------
  !
  if (params%ltb) then
    electrons = electrons_t(params, struct, kpoints)
    call start_clock ('electrons')

    if (params%read_electrons) then
      call printblock_start('Reading electrons')
      call electrons%read_enk('grid')
    else
      call printblock_start('Solving electrons')
      call electrons%solve_all(params%solver, params%write_cnk_grid, data_prefix='grid')
      call electrons%find_fermi_energy
      call electrons%write_enk(data_prefix='grid')
    endif
    !
    if (params%edos_ne>0) then
      call printblock_start('Electrons DOS')
      call electrons%calc_dos(params%edos_ne, params%edos_emin, params%edos_emax)
    endif
    !
    if (params%nef>0) then
      call electrons%nesting_fn ( params )
    endif
    call stop_clock ('electrons')
  endif
  !
  !----------------------
  ! Phonons
  !----------------------
  !
  if (params%lph) then
    call start_clock ('phonons')
    phonons = phonons_t(params, struct, qpoints)
    if (params%read_phonons) then
      call printblock_start('Reading phonons')
      call phonons%read_wvq('grid')
    else
      call printblock_start('Solving phonons')
      call phonons%setup_fc()
      call phonons%solve_all(params%solver, params%write_evq_grid, data_prefix='grid')
      call phonons%write_wvq(data_prefix='grid')
    endif

    if (params%phdos_ne>0) then
      call printblock_start('Phonon DOS')
      call phonons%calc_dos(params%phdos_ne, params%phdos_emin, params%phdos_emax)
    endif
    call stop_clock ('phonons')
  endif
  !

  !----------------------
  ! EPMAT
  !----------------------
  !
  if (params%lepmat) then
    call printblock_start ('EP-matrix elements')
    call start_clock ('epmat')
    call struct%setup_neighbors ( params%rcut_ep )
    call compute_epmat (params, electrons, phonons)
    call stop_clock ('epmat')
  endif
  !
  !----------------------
  ! Eliashberg
  !----------------------
  !
  if (params%leliashberg) then
    call printblock_start('Eliashberg Calculations')
    call start_clock ('eliashberg')
    call compute_eliashberg (params,electrons,phonons,1)
    call compute_eliashberg (params,electrons,phonons,2)
    call compute_eliashberg (params,electrons,phonons,3)
    call stop_clock ('eliashberg')
  endif
  !
  !----------------------
  ! Band path
  !----------------------
  !
  if (params%ltb.and.trim(params%kpath_file)/='') then
    call start_clock ('electrons-path')
    call printblock_start('Solving electrons along kpath')
    call kpath%from_file(params%kpath_file, params%nkpool_path)
    electrons_path = electrons_t(params, struct, kpath)
    electrons_path%ef = electrons%ef
    call electrons_path%solve_all(params%solver, params%write_cnk_path, data_prefix='path')
    call electrons_path%write_enk(data_prefix='path')
    call stop_clock ('electrons-path')
  endif
  !
  if (params%lph.and.trim(params%qpath_file)/='') then
    call start_clock ('phonons-path')
    call printblock_start('Solving phonons along kpath')
    call qpath%from_file(params%qpath_file, params%nqpool_path)
    phonons_path = phonons_t(params, struct, qpath)
    call phonons_path%setup_fc()
    call phonons_path%solve_all(params%solver, params%write_evq_path, data_prefix='path')
    call phonons_path%write_wvq(data_prefix='path')
    call stop_clock ('phonons-path')
  endif
  !
  if (params%analyze_wfn) then
    call electrons%analyze_wfn (data_prefix='grid')
    call electrons_path%analyze_wfn (data_prefix='path')
  endif
  !
  call stop_clock ( 'ep' )

  if (verbosity>=VERBOSITY_INFO) then
    write(log_filename, '(a,i0.5)') "log.proc.", rank
    close(LOG_FILE)
  endif
  call printblock_start('Timing Info')

  if (ionode) then
    call print_clock ( ' ' )
    call system('rm -f CRASH')
  endif

  call mpi_world_end

end program ep
