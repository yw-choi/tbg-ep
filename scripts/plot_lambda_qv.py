from numpy import *
import matplotlib.pyplot as plt
from scipy.io import FortranFile
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable
from mpl_toolkits.axes_grid1.colorbar import colorbar

dimq = 60
nmode = 168
nef = 11
nq = dimq*dimq
relatt = array([[ 2.94926726,     0.00000000],
                [-1.47463363,     2.55414037]])

q = arange(dimq)/dimq
q1, q2 = meshgrid(q,q)
qx = q1*relatt[0,0] + q2*relatt[1,0]
qy = q1*relatt[0,1] + q2*relatt[1,1]
lambda_qv = zeros((nef,dimq,dimq,nmode))
for iq in range(nq):
    print (iq)
    iq1 = int(iq/dimq)
    iq2 = mod(iq, dimq)
    lambda_qv[:,iq1,iq2,:] = FortranFile(f'./data/lambda_qv.iq.{iq+1:05d}.dat').read_reals().reshape((nef,nmode))

lambda_tot = loadtxt('./data/lambda_tot.dat')
lambda_qv_tot = einsum('exyv->exy', lambda_qv)

data = FortranFile(f'./data/nesting_fn.dat').read_reals().reshape((nef,nq))
fq = zeros((nef,dimq,dimq))
for iq in range(nq):
    iq1 = int(iq/dimq)
    iq2 = mod(iq, dimq)
    fq[:,iq1,iq2] = data[:,iq]

from multiprocessing import Pool

def plot(ief):
    print (ief)
    fig, (ax1, ax2) = plt.subplots(1,2)
    plt.suptitle('MLG, ef=%4.2f' % lambda_tot[ief,1])
    plt.subplots_adjust(top=0.8)

    lsc = 3

    q = arange(-(lsc-1)*dimq, lsc*dimq)/dimq
    q1, q2 = meshgrid(q,q)
    qx = q1*relatt[0,0] + q2*relatt[1,0]
    qy = q1*relatt[0,1] + q2*relatt[1,1]

    lq = (2*lsc-1)*dimq

    dat1 = zeros((lq,lq))
    dat2 = zeros((lq,lq))

    for n1 in range(-(lsc-1),lsc):
        for n2 in range(-(lsc-1),lsc):
            i1 = (n1+lsc-1)*dimq
            i2 = (n1+lsc)*dimq
            j1 = (n2+lsc-1)*dimq
            j2 = (n2+lsc)*dimq
            dat1[i1:i2,j1:j2] = lambda_qv_tot[ief,:,:]
            dat2[i1:i2,j1:j2] = fq[ief,:,:]

    p1 = ax1.pcolormesh(qx, qy, dat1, cmap='terrain')
    ax1_divider = make_axes_locatable(ax1)
    cax1 = ax1_divider.append_axes("top", size="7%", pad="2%")
    cax1.xaxis.set_ticks_position("top")
    cb1 = colorbar(p1, cax=cax1, orientation="horizontal")
    p2 = ax2.pcolormesh(qx, qy, dat2, cmap='terrain')
    ax2_divider = make_axes_locatable(ax2)
    cax2 = ax2_divider.append_axes("top", size="7%", pad="2%")
    cax2.xaxis.set_ticks_position("top")
    cb2 = colorbar(p2, cax=cax2, orientation="horizontal")

    cax1.tick_params(axis='both', which='major', labelsize=6)
    cax2.tick_params(axis='both', which='major', labelsize=6)
    cax1.set_title('$\lambda_{q}$', fontsize=10)
    cax2.set_title('Nesting Function', fontsize=10)

    ax1.set_xticks([])
    ax1.set_yticks([])
    ax2.set_xticks([])
    ax2.set_yticks([])
    ax1.set_xlabel('$k_x$')
    ax1.set_ylabel('$k_y$')
    ax2.set_xlabel('$k_x$')
    ax2.set_ylabel('$k_y$')
    ax1.set_xlim(-relatt[0,0], relatt[0,0])
    ax1.set_ylim(-relatt[0,0], relatt[0,0])
    ax2.set_xlim(-relatt[0,0], relatt[0,0])
    ax2.set_ylim(-relatt[0,0], relatt[0,0])
    plt.savefig('lambda_qv.ief.%d.png'%ief, dpi=300)
    plt.close()
with Pool(16) as p:
    p.map(plot, arange(nef))
