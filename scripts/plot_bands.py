from numpy import *
import os, f90nml
import matplotlib.pyplot as plt

M = 2
N = M-1
costh = (N*N+4.0*N*M+M*M)/(2.0*(N*N+N*M+M*M))
theta = 180/pi*arccos(costh)

dirname = './'
case = 'M%dN%d' % ( M, N )

enk = loadtxt('%s/data/path_enk.txt'%dirname)
ef = float(open('%s/data/grid_enk.txt'%dirname,'r').readline().split()[-1])
enk -= ef
# enk *= 1e3
nk, nb = enk.shape

na = nb

relatt = array([[0.06018495,    -0.03474780],
              [0.00000000,     0.06949559]])
qpoints = loadtxt('./kpath', skiprows=1)
nq = qpoints.shape[0]
qpath = zeros(nq)
for i in range(1,nq):
    qpath[i] = qpath[i-1]+linalg.norm(relatt.T.dot(qpoints[i,:2]-qpoints[i-1,:2]))

labels = ['$\Gamma$','$K$','$M$','$\Gamma$','$K^{\'}$',]
ticks = qpath[[0,30,60,90,120,]]

fig, ax = plt.subplots()
for ib in range(nb):
    ax.plot(qpath, enk[:,ib],'k-')
ax.set_title('M%dN%d, $\\theta=%.3f^\circ$'%(M,N,theta))
ax.set_ylim(-1.5,1.5)
ax.set_xticks(ticks)
ax.set_xticklabels(labels)
gridlinespec = { 'color': 'darkgrey',
               'linestyle': ':',
               'linewidth': 0.5 }
ax.grid(True, axis='x', **gridlinespec)
ax.axhline(0,  **gridlinespec)
ax.set_ylabel('Energy (eV)')
ax.set_xlim(ticks[0], ticks[-1])
# plt.savefig('M%dN%d.png'%(M,N),dpi=200)
plt.show()

