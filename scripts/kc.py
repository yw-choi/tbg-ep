from numpy import *
import matplotlib.pyplot as plt

# KC potential parameters
C0 = 15.71e-3
C2 = 12.29e-3
C4 = 4.933e-3
C  = 3.030e-3
delta = 0.578
ld = 3.629
A_KC = 10.238e-3
z0 = 3.34

def f_kc(x):
    return exp(-x)*(C0+C2*x+C4*x*x)

def df_kc(x):
    return exp(-x)*(C2-C0 + (2*C4-C2)*x - C4*x*x)

def d2f_kc(x):
    return exp(-x)*(2*C4-2*C2+C0+(C2-4*C4)*x+C4*x*x)

def vc_kc(rvec, rcut):
    r = linalg.norm(rvec)
    x = (rvec[0]*rvec[0]+rvec[1]*rvec[1])/(delta*delta)

    res = exp(-ld*(r-z0))*(C+2.0*f_kc(x)) - A_KC * (z0/r)**6 + A_KC * (z0/rcut)**6

    return res

def dv_kc(rvec):
    rvec = array(rvec)
    xy = array([rvec[0], rvec[1], 0.0])

    r = linalg.norm(rvec)
    x = (rvec[0]*rvec[0]+rvec[1]*rvec[1])/(delta*delta)

    res = exp(-ld*(r-z0))*(-ld*rvec/r*(C+2.0*f_kc(x)) \
            + 4./(delta*delta)*df_kc(x)*xy) \
            + 6*A_KC*(z0/r)**6 * rvec / (r*r)
    return res

def fc_kc(rvec):
    r = linalg.norm(rvec)
    r_xy = array([ rvec[0], rvec[1], 0.0 ])
    dlt = zeros((3,3))
    dlt[0,0] = 1.0
    dlt[1,1] = 1.0
    dlt[2,2] = 1.0
    dlt_xy = zeros((3,3))
    dlt_xy[0,0] = 1.0
    dlt_xy[1,1] = 1.0
    x = (rvec[0]*rvec[0]+rvec[1]*rvec[1])/(delta*delta)
    expfac = exp(-ld*(r-z0))

    f = f_kc(x)
    df = df_kc(x)
    d2f = d2f_kc(x)

    fc = zeros((3,3))
    for a in range(3):
        for b in range(3):
            fc[a,b] = expfac*( ld**2*rvec[a]*rvec[b]/r**2*(C+2*f) \
                               - 8*ld*r_xy[a]*r_xy[b]/r/delta**2*df_kc(x) \
                               - ld*(dlt[a,b]/r-rvec[a]*rvec[b]/r**3)*(C+2*f) \
                               + 4/delta**2*dlt_xy[a,b]*df \
                               + 8/delta**4*r_xy[a]*r_xy[b]*d2f) \
              +6*A_KC*(z0/r)**6 * (dlt[a,b]/r**2-8*rvec[a]*rvec[b]/r**4)

    fc = -fc
    return fc

rcut = 200
x = linspace(0,rcut+50,1000)
r = []
v = []
dv = []
d2v = []
for xx in x:
    rvec = [xx,0,z0]
    v.append(vc_kc(rvec, rcut))
    r.append(linalg.norm(rvec))
    dv.append(linalg.norm(dv_kc(rvec)))
    d2v.append(abs(fc_kc(rvec)).max())
v = array(v)
r = array(r)
dv = array(dv)
d2v = array(d2v)

plt.figure(1)
plt.semilogy(r,abs(v),'.-')
plt.ylabel('ABS(V_KC) (eV)')
plt.xlabel('r (Ang)')
plt.savefig('v.png',dpi=300)

plt.figure(2)
plt.semilogy(r,abs(dv),'.-')
plt.ylabel('ABS(dV_KC) (eV/Ang)')
plt.xlabel('r (Ang)')
plt.savefig('dv.png',dpi=300)

plt.figure(3)
plt.semilogy(r,abs(d2v),'.-')
plt.ylabel('ABS(fc_KC) (eV/Ang)')
plt.xlabel('r (Ang)')
plt.savefig('d2v.png',dpi=300)

plt.figure(4)
plt.semilogy(r,abs(v),'-',label="v")
plt.semilogy(r,abs(dv),'-',label="dv")
plt.semilogy(r,abs(d2v),'-',label="d2v")
plt.legend(frameon=False)
plt.savefig('cmp.png')

plt.show()
