from numpy import *
import matplotlib.pyplot as plt
from scipy.io import FortranFile

f = open('./data/a2F.dat','r')
nsmear, nw, nef = int32(f.readline().split())
print (nsmear,nw,nef)

a2F = zeros((nef,nw,nsmear))
wgrid = zeros(nw)
efgrid = zeros(nef)
ltot = zeros(nef)
for ief in range(nef):
    row = f.readline().split()
    # print (row[0], row[1], row[2])
    ltot[ief] = float(row[2])
    efgrid[ief] = float(row[1])
    for iw in range(nw):
        data = float64(f.readline().split())
        wgrid[iw] = data[0]
        a2F[ief,iw,:] = data[1:]
f.close()

from scipy.integrate import simps
def plot(ief):
  for i in range(nsmear):
      ismear = i
      plt.figure(i)
      plt.title('ef=%.5f eV, smear=%d'%(efgrid[ief],i))
      plt.plot(wgrid*1e3, a2F[ief,:,i], '-', lw=1)

      lambda_w = zeros(nw)
      for iw in range(2,nw):
          lambda_w[iw] = 2*simps(a2F[ief,1:iw,i]/wgrid[1:iw],wgrid[1:iw])
      plt.plot(wgrid*1e3, lambda_w, '--', lw=1)
      plt.axhline(ltot[ief], linestyle='--', lw=1)
      print (i, lambda_w[200], lambda_w[-1], lambda_w[200]/lambda_w[-1])

      plt.xlabel('omega (meV)')
      plt.ylabel('a2F')
      plt.savefig('a2F.ief.%d.ismear.%d.png'%(ief,ismear))
      plt.close()

with Pool(16) as p:
    p.map(plot, arange(nef))
