from numpy import *
from pymatgen.symmetry.bandstructure import *
from pymatgen import Structure

pts = array([
        [0.00000000,0.00000000,0.0],
        [0.66666666,0.33333333,0.0],
        [0.50000000,0.50000000,0.0],
        [0.00000000,0.00000000,0.0],
        ])

nk = 30
print (nk*(len(pts)-1)+1)
print ('%12.8f %12.8f %12.8f'%(pts[0,0], pts[0,1], pts[0,2]))
for i in range(1,len(pts)):

    for ii in range(nk):
        k = pts[i-1,:]+(pts[i,:]-pts[i-1,:])*(ii+1)/nk

        print ('%12.8f %12.8f %12.8f'%(k[0], k[1], k[2]))
