from numpy import *
import matplotlib.pyplot as plt

# plt.figure(1)
# data = loadtxt('./data/path_enk.dat')
# nk, nb = data.shape

# for i in range(nb):
#     plt.plot(data[:,i],'k-')
plt.figure(2)
data = loadtxt('./data/path_wvq.dat')
nk, nb = data.shape

for i in range(nb):
    plt.plot(1e3*data[:,i],'k-')
plt.ylim(-5,15)
plt.show()
