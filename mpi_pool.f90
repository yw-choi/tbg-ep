! This is a modified version of kpoint_pool_m of ParaBands/kpoint_pool.f90 from BGW
module mpi_pool
  use mpi
  use mp, only: mp_comm_split, mp_barrier

  implicit none

    type mpi_pool_t
      !> BLACS context wherein all processors in this pool are distributed in
      !! a single column. There are no leftover processors.
      integer :: cntxt_c = -1
      !> BLACS context wherein all processors in this pool are distributed in
      !! a single row. There are no leftover processors.
      integer :: cntxt_r = -1
      !> BLACS context wherein processors in this pool are distributed in a 2D
      !! grid. There might be leftover processors that are not part of the context.
      integer :: cntxt_2d = -1
      integer :: npool = 1
      integer :: mypool = 0
      integer :: rank = 0
      integer :: nprocs = 0
      integer :: nprocs_diag = 0
      integer :: nprow
      integer :: npcol
      integer :: myprow
      integer :: mypcol
      integer :: group = 0
      integer :: comm = 0
      integer :: inter_comm = 0 
      integer :: iunit = 6
    contains
      procedure :: setup => mpi_pool_setup
      procedure :: cleanup => mpi_pool_cleanup
    endtype mpi_pool_t

  public :: mpi_pool_t
  private

contains

!> Note: this routine must be called by ALL MPI PROCESSORS!
subroutine mpi_pool_setup(this, npools_)
  integer, intent(in) :: npools_
  class(mpi_pool_t), intent(inout) :: this
  integer, allocatable :: ranks(:,:)
  integer :: world_group, mypool, ii, tmp_cntxt
  integer :: dims(2)

  this%npool = npools_

  if (this%npool<1) then
    call errore('mpi_pool_setup', 'invalid number of pools', this%npool)
  endif

  if (mod(nprocs, this%npool)/=0) then
    call errore('mpi_pool_setup', 'nprocs shoudl be dividable by npool', this%npool)
  endif

  this%nprocs = nprocs / this%npool ! procs per pool
  this%mypool = rank / this%nprocs ! my pool
  this%rank = MOD ( rank, this%nprocs )
  !
  ! ... the intra_pool_comm communicator is created
  !
  CALL mp_comm_split ( comm_world, this%mypool, rank, this%comm )
  !
  CALL mp_barrier( comm_world )
  !
  ! ... the inter_pool_comm communicator is created
  !
  CALL mp_comm_split ( comm_world, this%rank, rank, this%inter_comm )

  ! Create 1D BLACS contexts. They have to be created globally!
  allocate(ranks(1,this%nprocs))
  do mypool = 0, this%npool-1
    ranks(1,:) = (/ (ii, ii=0, this%nprocs-1) /)
    ranks = ranks + mypool*this%nprocs
    call blacs_get(-1, 0, tmp_cntxt)
    call blacs_gridmap(tmp_cntxt, ranks, 1, 1, this%nprocs) ! This will modify cntxt
    if (mypool==this%mypool) this%cntxt_c = tmp_cntxt
  enddo
  deallocate(ranks)

  allocate(ranks(this%nprocs,1))
  do mypool = 0, this%npool-1
    ranks(:,1) = (/ (ii, ii=0, this%nprocs-1) /)
    ranks = ranks + mypool*this%nprocs
    call blacs_get(-1, 0, tmp_cntxt)
    call blacs_gridmap(tmp_cntxt, ranks, this%nprocs, this%nprocs, 1) ! This will modify cntxt
    if (mypool==this%mypool) this%cntxt_r = tmp_cntxt
  enddo
  deallocate(ranks)

  ! Create 2D BLACS contexts. They have to be created globally!
  if (this%nprocs>1) then
    this%nprow = nint(sqrt(dble(this%nprocs)))
    do while (.true.) 
      if (mod(this%nprocs, this%nprow)==0) then
        exit
      endif
      this%nprow = this%nprow - 1
    enddo
    this%npcol = this%nprocs / this%nprow
  else
    this%nprow = 1
    this%npcol = 1
  endif

  this%nprocs_diag = this%nprow*this%npcol
  allocate(ranks(this%nprow, this%npcol))
  do mypool = 0, this%npool-1
    do ii = 0, this%nprow*this%npcol-1
      ranks(ii/this%npcol+1, mod(ii,this%npcol)+1) = ii
    enddo
    ranks = ranks + mypool*this%nprocs
    call blacs_get(-1, 0, tmp_cntxt)
    call blacs_gridmap(tmp_cntxt, ranks, this%nprow, this%nprow, this%npcol) ! This will modify cntxt
    if (mypool==this%mypool) then
      this%cntxt_2d = tmp_cntxt
    endif
  enddo
  deallocate(ranks)

  call blacs_gridinfo(this%cntxt_2d, this%nprow, this%npcol, this%myprow, this%mypcol)

end subroutine mpi_pool_setup


subroutine mpi_pool_cleanup(this)
  class(mpi_pool_t), intent(inout) :: this

  if (this%cntxt_c>=0) call blacs_gridexit(this%cntxt_c)
  if (this%cntxt_r>=0) call blacs_gridexit(this%cntxt_r)
  if (this%cntxt_2d>=0) call blacs_gridexit(this%cntxt_2d)
  this%cntxt_c = -1
  this%cntxt_r = -1
  this%cntxt_2d = -1

end subroutine mpi_pool_cleanup

end module mpi_pool
