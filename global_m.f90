module global_m

  use kinds, only: dp

  implicit none

  integer            :: verbosity
  character(len=256) :: outdir
  logical            :: use_symmetry

  integer, parameter :: VERBOSITY_INFO = 2
  integer, parameter :: VERBOSITY_DEBUG = 10
  integer, parameter :: LOG_FILE = 27

  public
  save

end module global_m
