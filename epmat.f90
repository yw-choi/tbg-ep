module epmat_m
  use mpi
  use mpi_pool
  use kinds, only: dp
  use global_m, only: outdir, verbosity, VERBOSITY_INFO, LOG_FILE
  use constants, only: TPI, eps16, RYTOEV
  use input, only: params_t
  use lattice, only: struct_t
  use kgrid, only: kpoints_t, get_kq
  use electrons_m, only: electrons_t, elgm => gamma_matrix
  use phonons_m, only: phonons_t, phgm => gamma_matrix
  use distrib_mat, only: distrib_mat_t, read_matrix, write_matrix
  use utils, only: distribute_indices
  use hoppings, only: dtdx, t_sk

  implicit none

  public

  integer :: g2mat_reclen

  character(len=256) :: g2mat_filename
  integer :: g2mat_file

  character(len=256) :: g2mat_filename2
  integer :: g2mat_file2

  character(len=256) :: g2mat_filename3
  integer :: g2mat_file3

contains

  subroutine compute_epmat (params, electrons, phonons)
    class(params_t), intent(in) :: params
    class(electrons_t), intent(inout) :: electrons
    class(phonons_t), intent(inout) :: phonons

    class(struct_t), pointer :: struct
    class(kpoints_t), pointer :: kpt, qpt
    type(distrib_mat_t) :: evqmat
    type(distrib_mat_t) :: gmat

    ! ----------------------------------
    ! Arrays that are common to all procs
    ! ----------------------------------
    complex(dp), allocatable :: &
      cnk(:,:,:), exp_ikR(:,:)

    integer, allocatable :: kqmap(:)

    ! ----------------------------------
    ! Distributed arrays
    ! ----------------------------------
    complex(dp), allocatable :: &
      evq(:,:), evq_irr(:,:), gam(:,:), gij(:,:), &
      M_orb(:,:), M_orb1(:,:), M_orb2(:,:)

    real(dp), allocatable :: &
      g2(:,:)

    ! ----------------------------------
    ! Local variables
    ! ----------------------------------
    integer :: &
      nbnd, nmode, na_uc_l, na_uc, nlayer, na_sc_l, na_sc, &
      nk_tot, nk_irr, nq_tot, nq_irr, nmode_lr, nmode_lc, &
      nbk_l, nbk, npr, npc

    integer :: &
      ik_irr, iq_irr, ik, isym, irot, imode, istar, &
      ibnd, jbnd, ibk_l, ibk_g, indx, iprow, ipcol, ka_l, ka_g, &
      ka_uc, ka_uc_l, kl, il, inb, ia_sc_l, ia_uc_l, ia_uc, isc, kx, &
      nu_l, nu_g, ikq, i, iq_star, ksite, isite, start_iq, end_iq

    real(dp) :: &
      xq_irr(3), xq_star(3), dvec(3), dist, dhop

    character(len=256) :: fn, timestr

    integer, external :: indxl2g
    real(dp), external :: get_clock

    struct => electrons%struct
    kpt => electrons%kpoints
    qpt => phonons%qpoints

    if (params%start_iq>1) then
      if (params%nqpool>1) then
        call errore('compute_epmat', 'start_iq>1 and nqpool>1 is not possible', 1)
      endif
      start_iq = params%start_iq
      end_iq = qpt%endk
    else
      start_iq = qpt%startk
      end_iq = qpt%endk
    endif

    ! dimensions
    nbnd = electrons%nbnd
    nmode = phonons%nmode

    nlayer = struct%nlayer
    na_uc = struct%na_uc
    na_uc_l = struct%na_uc_l
    na_sc = struct%na_sc
    na_sc_l = struct%na_sc_l

    nk_tot = kpt%nkmax
    nq_tot = qpt%nkmax
    nk_irr = kpt%nk
    nq_irr = qpt%nk

    nbk = nbnd * nbnd * nk_tot

    call evqmat%setup_mat_2d(nmode, nmode, qpt%pool)
    call gmat%setup_mat_2d(nbk, nmode, qpt%pool)

    nmode_lr = evqmat%Ml
    nmode_lc = evqmat%Nl
    nbk_l = gmat%Ml

    ! This should not happen
    if (gmat%Nl /= nmode_lc) then
      print *, 'gmat%Nl, nmode_lc = ', gmat%Nl, nmode_lc
      call mp_barrier ( comm_world )
      call errore('epmat', 'Dimension inconsistency', 1)
    endif

    !
    !-------------------------------------------------------------------
    ! 1. Read electron eigenvectors for all procs
    !-------------------------------------------------------------------
    !
    write(stdout, "(1x,a,F10.1,a)") "Reading electron eigenvectors @ ", get_clock('ep'), ' s'
    allocate(cnk(na_uc, nbnd, nk_tot))
    call start_clock('  ep-read_cnk')
    call read_cnk ( params, electrons, cnk )
    call stop_clock('  ep-read_cnk')
    !
    !-------------------------------------------------------------------
    ! 2. setup exp(i k * R) factors
    !-------------------------------------------------------------------
    !
    write(stdout, "(1x,a,F10.1,a)") "Setup phae factors @ ", get_clock('ep'), ' s'
    allocate(exp_ikR(struct%nsc, nk_tot))
    call setup_phase_fac(struct, kpt, exp_ikR)
    !
    !-------------------------------------------------------------------
    ! 3. Main loop for epmat calculation
    !-------------------------------------------------------------------
    !
    allocate(evq_irr(nmode_lr, nmode_lc), evq(nmode_lr, nmode_lc))
    allocate(gam(nmode_lr, nmode_lc), gij(nbk_l, nmode_lc))
    allocate(M_orb(nbk_l, nmode_lc),M_orb1(nbk_l, nmode_lc),M_orb2(nbk_l, nmode_lc))
    allocate(kqmap(nk_tot))
    allocate(g2(nbk_l, nmode_lc))

    inquire(iolength=g2mat_reclen) g2

    g2mat_file = 201
    g2mat_file2 = 202
    g2mat_file3 = 203

    write(g2mat_filename, '(a,i0.5,a)') trim(outdir)//"/g2.", rank, ".dat"
    open(g2mat_file, file=trim(g2mat_filename), form="unformatted", &
      status="unknown", access="direct", recl=g2mat_reclen, action="write")

    write(g2mat_filename2, '(a,i0.5,a)') trim(outdir)//"/g2.intra.", rank, ".dat"
    open(g2mat_file2, file=trim(g2mat_filename2), form="unformatted", &
      status="unknown", access="direct", recl=g2mat_reclen, action="write")

    write(g2mat_filename3, '(a,i0.5,a)') trim(outdir)//"/g2.inter.", rank, ".dat"
    open(g2mat_file3, file=trim(g2mat_filename3), form="unformatted", &
      status="unknown", access="direct", recl=g2mat_reclen, action="write")

    write(stdout, "(1x,a,F10.1,a)") "Start epmat calculations @ ", get_clock('ep'), ' s'

    do iq_irr = start_iq, end_iq
      write(stdout, "(3x,a,i5,a,i5,a,F10.1,a)") "Computing epmat iq_irr # ", iq_irr, "/", qpt%nk_pool, ' @ ', get_clock('ep'), ' s'

      call mp_barrier ( qpt%pool%comm )

      xq_irr = qpt%xk(1:3, iq_irr)

      write(stdout,"(5x,a,3F12.6)") "q (cryst.) = ", xq_irr
      write(stdout,"(5x,a,I4)") "Number of stars = ", qpt%n_stars(iq_irr)
      do istar = 1, qpt%n_stars(iq_irr)
        iq_star = qpt%stars(istar, iq_irr)
        xq_star = qpt%xkg(1:3, iq_star)

        write(stdout,"(5x,a,I4,a,3F12.6)") "q_star (", istar, ") = ", xq_star
      enddo

      write(fn, '(a,i0.5,a)') trim(outdir)//'/grid_evq.',iq_irr,'.dat'
      call start_clock('  ep-read_evq')
      call read_matrix (fn, evqmat, evq_irr)
      call stop_clock('  ep-read_evq')

      xq_irr = qpt%xk(1:3, iq_irr)

      do istar = 1, qpt%n_stars(iq_irr)
        iq_star = qpt%stars(istar, iq_irr)
        isym = qpt%equiv_s(iq_star)

        xq_star = qpt%xkg(1:3,iq_star)

        write(stdout, "(7x,a,i5,a,i5,a,i5,a,F10.1,a)") "istar, iq_star, isym # ", istar, ',', iq_star, ',', isym, ' @ ', get_clock('ep'), ' s'

        if (verbosity>=VERBOSITY_INFO.and.qpt%pool%rank==0) then
          write(6, "(3x,a,i5,a,i4,a,f10.1,a)") &
            "iq_star # ", iq_star, ' by pool = ', qpt%pool%mypool ,' @ ', get_clock('ep'), ' s'
        endif

        ! rotate evq_irr
        call start_clock('  ep-rotate_evq')
        if (verbosity>=VERBOSITY_INFO) then
          write(LOG_FILE, "(a,i4,a,f10.1,a)") &
            "start ep-rotate_evq, pool = ", qpt%pool%mypool ,' @ ', get_clock('ep'), ' s'
        endif
        call phgm (struct, evqmat, xq_irr, isym, gam)
        call PZGEMM ('N', 'N', nmode, nmode, nmode, &
          (1._dp, 0._dp), &
          gam, 1, 1, evqmat%desc, &
          evq_irr, 1, 1, evqmat%desc, &
          (0._dp, 0._dp), &
          evq, 1, 1, evqmat%desc)
        call stop_clock('  ep-rotate_evq')

        ! pre-compute kqmap
        call start_clock('  ep-kqmap')
        if (verbosity>=VERBOSITY_INFO) then
          write(LOG_FILE, "(a,i4,a,f10.1,a)") &
            "start ep-kqmap, pool = ", qpt%pool%mypool ,' @ ', get_clock('ep'), ' s'
        endif
        do ik = 1, nk_tot
          call get_kq(kpt, qpt, ik, iq_star, kqmap(ik))
        enddo
        call stop_clock('  ep-kqmap')

        ! Build orbital part of the matrix elements:
        ! M_orb{(ibnd,jbnd,k),(kappa,a)} =
        ! \sum_{i} d/dx_a { t(tau_{0,kappa}-tau_{i}) }
        ! * (e^{ikR_{i}} c^{*}_{jbnd,k+q,kappa}) c_{ibnd,k,i}
        ! + e^{-i(k+q)R_{i}} c^{*}_{jbnd,k+q,i}) c_{ibnd,k,kappa})
        ! Ref) Eq (6). of PRB 98, 241412(R) (2018)
        M_orb = 0.d0
        M_orb1 = 0.d0
        M_orb2 = 0.d0

        call start_clock('  ep-M_orb')
        if (verbosity>=VERBOSITY_INFO) then
          write(LOG_FILE, "(a,i4,a,f10.1,a)") &
            "start ep-M_orb, pool = ", qpt%pool%mypool ,' @ ', get_clock('ep'), ' s'
        endif
        do ka_l = 1, nmode_lc
          ka_g = indxl2g(ka_l, gmat%Nb, qpt%pool%mypcol, 0, qpt%pool%npcol)
          if (ka_g<1 .or. ka_g>nmode) cycle

          kx = mod(ka_g-1,3)+1
          ka_uc = int((ka_g-1)/3)+1
          ksite = mod(ka_uc-1,2)+1
          ka_uc_l = mod(ka_uc-1, na_uc_l)+1
          kl = int((ka_uc-1)/na_uc_l)+1

          do il = 1, struct%nlayer
            do inb = 1, struct%num_neighbors(il,ka_uc_l,kl)
              ia_sc_l = struct%neighbors(inb,il,ka_uc_l,kl)

              ia_uc_l = struct%sc_uc_map(ia_sc_l, il)
              ia_uc = (il-1)*struct%na_uc_l+ia_uc_l
              isite = mod(ia_uc-1,2)+1
              isc = struct%isc_map(ia_sc_l,il)

              dvec = struct%atoms_uc(1:3,ka_uc_l,kl) &
                   - struct%atoms_sc(1:3,ia_sc_l,il)
              dist = norm2(dvec)
              if (dist>params%rcut_ep) then
                cycle
              endif

              dhop = dtdx(dvec, kx)

              do ibk_l = 1, nbk_l
                ibk_g = indxl2g(ibk_l, gmat%Mb, qpt%pool%myprow, 0, qpt%pool%nprow)
                if (ibk_g<1 .or. ibk_g>nbk) cycle
                ! Note: ibk_g = ibnd + nbnd*(jbnd-1) + nbnd*nbnd*(ik-1)
                ik = int((ibk_g-1)/(nbnd*nbnd))+1
                ikq = kqmap(ik)
                indx = mod(ibk_g-1, nbnd*nbnd)+1
                jbnd = int((indx-1)/nbnd)+1
                ibnd = mod(indx-1, nbnd)+1

                M_orb(ibk_l, ka_l) =  M_orb(ibk_l, ka_l) + &
                  dhop *( exp_ikR(isc,ik)*conjg(cnk(ka_uc,jbnd,ikq))*cnk(ia_uc,ibnd,ik) &
                        + conjg(exp_ikR(isc,ikq)*cnk(ia_uc,jbnd,ikq))*cnk(ka_uc,ibnd,ik) )

                if (ksite==isite) then
                  M_orb1(ibk_l, ka_l) =  M_orb1(ibk_l, ka_l) + &
                    dhop *( exp_ikR(isc,ik)*conjg(cnk(ka_uc,jbnd,ikq))*cnk(ia_uc,ibnd,ik) &
                          + conjg(exp_ikR(isc,ikq)*cnk(ia_uc,jbnd,ikq))*cnk(ka_uc,ibnd,ik) )
                else
                  M_orb2(ibk_l, ka_l) =  M_orb2(ibk_l, ka_l) + &
                    dhop *( exp_ikR(isc,ik)*conjg(cnk(ka_uc,jbnd,ikq))*cnk(ia_uc,ibnd,ik) &
                          + conjg(exp_ikR(isc,ikq)*cnk(ia_uc,jbnd,ikq))*cnk(ka_uc,ibnd,ik) )
                endif

                ! if (kl==il.and.dist<1.5/.529177.and.(kx==1.or.kx==2)) then
                !   M_orb(ibk_l, ka_l) =  M_orb(ibk_l, ka_l) + dvec(kx)/dist*7.28/13.605*.529177* &
                !                        conjg(exp_ikR(isc,iq_star)*cnk(ia_uc,jbnd,ikq))*cnk(ia_uc,ibnd,ik)

                !   if (ksite==isite) then
                !     M_orb1(ibk_l, ka_l) =  M_orb1(ibk_l, ka_l) + dvec(kx)/dist*7.28/13.605*.529177* &
                !                          conjg(exp_ikR(isc,iq_star)*cnk(ia_uc,jbnd,ikq))*cnk(ia_uc,ibnd,ik)

                !   else
                !     M_orb2(ibk_l, ka_l) =  M_orb2(ibk_l, ka_l) + dvec(kx)/dist*7.28/13.605*.529177* &
                !                          conjg(exp_ikR(isc,iq_star)*cnk(ia_uc,jbnd,ikq))*cnk(ia_uc,ibnd,ik)
                !   endif
                ! endif

              enddo ! ibk_l

            enddo ! inb
          enddo ! il

        enddo ! ka_l
        call stop_clock('  ep-M_orb')

        call start_clock('  ep-g2')
        if (verbosity>=VERBOSITY_INFO) then
          write(LOG_FILE, "(a,i4,a,f10.1,a)") &
            "start ep-g2, pool = ", qpt%pool%mypool ,' @ ', get_clock('ep'), ' s'
        endif
        ! g_{(ibnd,jbnd,k),(nu)} = \sum_{kappa,a} M_{(ibnd,jbnd,k),(kappa,a)} * evq_{(kappa,a),(nu)}
        call PZGEMM ('N', 'N', nbk, nmode, nmode, &
          (1.d0, 0.d0), &
          M_orb, 1, 1, gmat%desc, &
          evq, 1, 1, evqmat%desc, &
          (0.d0, 0.d0), &
          gij, 1, 1, gmat%desc)

        g2 = dconjg(gij)*gij
        call stop_clock('  ep-g2')

        call start_clock('  ep-write_g2')
        if (verbosity>=VERBOSITY_INFO) then
          write(LOG_FILE, "(a,i4,a,f10.1,a)") &
            "start ep-write_g2, pool = ", qpt%pool%mypool ,' @ ', get_clock('ep'), ' s'
        endif
        write(g2mat_file, rec=iq_star) g2
        call stop_clock('  ep-write_g2')

        call start_clock('  ep-g2')
        if (verbosity>=VERBOSITY_INFO) then
          write(LOG_FILE, "(a,i4,a,f10.1,a)") &
            "start ep-g2_1, pool = ", qpt%pool%mypool ,' @ ', get_clock('ep'), ' s'
        endif
        ! g_{(ibnd,jbnd,k),(nu)} = \sum_{kappa,a} M_{(ibnd,jbnd,k),(kappa,a)} * evq_{(kappa,a),(nu)}
        call PZGEMM ('N', 'N', nbk, nmode, nmode, &
          (1.d0, 0.d0), &
          M_orb1, 1, 1, gmat%desc, &
          evq, 1, 1, evqmat%desc, &
          (0.d0, 0.d0), &
          gij, 1, 1, gmat%desc)

        g2 = dconjg(gij)*gij
        call stop_clock('  ep-g2')

        call start_clock('  ep-write_g2')
        if (verbosity>=VERBOSITY_INFO) then
          write(LOG_FILE, "(a,i4,a,f10.1,a)") &
            "start ep-write_g2_1, pool = ", qpt%pool%mypool ,' @ ', get_clock('ep'), ' s'
        endif
        write(g2mat_file2, rec=iq_star) g2
        call stop_clock('  ep-write_g2')

        call start_clock('  ep-g2')
        if (verbosity>=VERBOSITY_INFO) then
          write(LOG_FILE, "(a,i4,a,f10.1,a)") &
            "start ep-g2_2, pool = ", qpt%pool%mypool ,' @ ', get_clock('ep'), ' s'
        endif
        ! g_{(ibnd,jbnd,k),(nu)} = \sum_{kappa,a} M_{(ibnd,jbnd,k),(kappa,a)} * evq_{(kappa,a),(nu)}
        call PZGEMM ('N', 'N', nbk, nmode, nmode, &
          (1.d0, 0.d0), &
          M_orb2, 1, 1, gmat%desc, &
          evq, 1, 1, evqmat%desc, &
          (0.d0, 0.d0), &
          gij, 1, 1, gmat%desc)

        g2 = dconjg(gij)*gij
        call stop_clock('  ep-g2')

        call start_clock('  ep-write_g2')
        if (verbosity>=VERBOSITY_INFO) then
          write(LOG_FILE, "(a,i4,a,f10.1,a)") &
            "start ep-write_g2_2, pool = ", qpt%pool%mypool ,' @ ', get_clock('ep'), ' s'
        endif
        write(g2mat_file3, rec=iq_star) g2
        call stop_clock('  ep-write_g2')

      enddo ! istar

      write(stdout, *) " "

    enddo ! iq_irr

    call mp_barrier ( comm_world )

    write(stdout, "(1x,a,F10.1,a)") "End of epmat calculations @ ", get_clock('ep'), ' s'

    ! print *, rank, g2_tot
    ! call mp_sum ( g2_tot, comm_world )
    ! print *, 'g2_tot=',g2_tot
    ! call mp_barrier (comm_world)
    ! stop "DBG"
    !
    !-------------------------------------------------------------------
    ! Free resources
    !-------------------------------------------------------------------
    !
    deallocate(cnk, exp_ikR)
    deallocate(evq, evq_irr, gam, kqmap, M_orb, gij, g2, M_orb1, M_orb2)
    nullify(struct, kpt, qpt)

    close(g2mat_file)
    close(g2mat_file2)
    close(g2mat_file3)

  end subroutine compute_epmat

  subroutine setup_phase_fac (struct, kpoints, phase_fac)
    class(struct_t), intent(in) :: struct
    class(kpoints_t), intent(in) :: kpoints
    complex(dp), intent(out) :: phase_fac(struct%nsc,kpoints%nkmax)

    integer :: ik1, ik2, ik, isc

    real(dp) :: arg, xkg(2)

    phase_fac = 0.d0

    do ik = 1, kpoints%nkmax
      do isc = 1, struct%nsc
        ik1 = int((ik-1)/kpoints%dimk)+1
        ik2 = mod(ik-1,kpoints%dimk)+1
        xkg(1) = dble(ik1-1)/kpoints%dimk
        xkg(2) = dble(ik2-1)/kpoints%dimk

        arg = TPI*sum(struct%R_isc(1:2,isc)*xkg)
        phase_fac(isc,ik) = cmplx(cos(arg),sin(arg),kind=dp)
      enddo
    enddo

  end subroutine setup_phase_fac

  subroutine write_g2mat(fn, g2mat, g2)
    character(len=256), intent(in) :: fn
    type(distrib_mat_t), intent(in) :: g2mat
    real(dp), intent(in) :: g2(g2mat%Ml,g2mat%Nl)

    integer :: ierr
    integer, dimension(2) :: pdims, dims, distribs, dargs
    integer :: outfile
    integer, dimension(MPI_STATUS_SIZE) :: mpistatus
    integer :: darray

    integer(kind=MPI_OFFSET_KIND) :: disp
    integer :: tt, i, j

    pdims = [g2mat%nprow, g2mat%npcol]
    dims = [g2mat%M, g2mat%N]
    distribs = [MPI_DISTRIBUTE_CYCLIC, MPI_DISTRIBUTE_CYCLIC]
    dargs = [g2mat%Mb, g2mat%Nb]
    call MPI_Type_create_darray(g2mat%pool%nprocs, g2mat%pool%rank, 2, dims, distribs, dargs, &
                              pdims, MPI_ORDER_FORTRAN, MPI_DOUBLE_PRECISION, &
                              darray, ierr)
    call MPI_Type_commit(darray,ierr)

    call MPI_File_open(g2mat%pool%comm, trim(fn), MPI_MODE_CREATE+MPI_MODE_WRONLY, MPI_INFO_NULL, outfile, ierr)
    call mpi_check_call(ierr)
    disp = 0
    call MPI_File_set_view(outfile, disp, MPI_DOUBLE_PRECISION, darray, "native", MPI_INFO_NULL, ierr)
    call mpi_check_call(ierr)

    call MPI_File_write_all(outfile, g2, g2mat%Ml*g2mat%Nl, MPI_DOUBLE_PRECISION, MPI_STATUS_IGNORE, ierr)
    call mpi_check_call(ierr)
    call MPI_File_close(outfile,ierr)
    call mpi_check_call(ierr)

  end subroutine write_g2mat

  subroutine read_g2mat(fn, g2mat, g2)
    character(len=256), intent(in) :: fn
    class(distrib_mat_t), intent(in) :: g2mat
    real(dp), intent(out) :: g2(g2mat%Ml, g2mat%Nl)

    integer :: ierr
    integer :: outfile
    integer :: darray
    integer, dimension(2) :: pdims, dims, distribs, dargs
    integer, dimension(MPI_STATUS_SIZE) :: mpistatus

    integer(kind=MPI_OFFSET_KIND) :: disp
    integer :: tt, i, j

    pdims = [g2mat%nprow, g2mat%npcol]
    dims = [g2mat%M, g2mat%N]
    distribs = [MPI_DISTRIBUTE_CYCLIC, MPI_DISTRIBUTE_CYCLIC]
    dargs = [g2mat%Mb, g2mat%Nb]
    call MPI_Type_create_darray(g2mat%pool%nprocs, g2mat%pool%rank, 2, dims, distribs, dargs, &
                              pdims, MPI_ORDER_FORTRAN, MPI_DOUBLE_PRECISION, &
                              darray, ierr)
    call MPI_Type_commit(darray,ierr)

    call MPI_File_open(g2mat%pool%comm, trim(fn), MPI_MODE_RDONLY, MPI_INFO_NULL, outfile, ierr)
    call mpi_check_call(ierr)
    disp = 0
    call MPI_File_set_view(outfile, disp, MPI_DOUBLE_PRECISION, darray, "native", MPI_INFO_NULL, ierr)
    call mpi_check_call(ierr)

    call MPI_File_read_all(outfile, g2, g2mat%Ml*g2mat%Nl, MPI_DOUBLE_PRECISION, MPI_STATUS_IGNORE, ierr)
    call mpi_check_call(ierr)
    call MPI_File_close(outfile,ierr)
    call mpi_check_call(ierr)

  end subroutine read_g2mat

  subroutine read_cnk ( params, electrons, cnk)
    class(params_t), intent(in) :: params
    class(electrons_t), intent(in) :: electrons
    complex(dp), intent(out) :: &
      cnk(electrons%struct%na_uc, electrons%nbnd, electrons%kpoints%nkmax)

    complex(dp), allocatable :: zloc(:,:), zg(:,:), zrot(:,:), gam(:,:)

    real(dp) :: xk_star(3), xk_irr(3)
    integer :: ik_irr, iloc, jloc, i, j, istar, ik_star, isym
    integer :: na_uc, nbnd, nk_irr, nk_tot

    character(len=256) :: fn

    integer, external :: indxl2g

    nbnd = electrons%nbnd
    na_uc = electrons%struct%na_uc
    nk_irr = electrons%kpoints%nk
    nk_tot = electrons%kpoints%nkmax

    allocate(zloc(electrons%Zmat%Ml, electrons%Zmat%Nl), zrot(electrons%Zmat%Ml, electrons%Zmat%Nl))
    allocate(zg(na_uc, nbnd), gam(electrons%Hmat%Ml, electrons%Hmat%Nl))

    cnk = 0.d0

    do ik_irr = electrons%kpoints%startk, electrons%kpoints%endk
      write(fn, '(a,i0.5,a)') trim(outdir)//'/grid_cnk.',ik_irr,'.dat'

      call read_matrix ( fn, electrons%Zmat, zloc )
      xk_irr = electrons%kpoints%xk(1:3, ik_irr)

      do istar = 1, electrons%kpoints%n_stars(ik_irr)
        ik_star = electrons%kpoints%stars(istar, ik_irr)
        isym = electrons%kpoints%equiv_s(ik_star)

        xk_star = electrons%kpoints%xkg(1:3,ik_star)

        call elgm (electrons%struct, electrons%Hmat, xk_irr, isym, gam)

        call PZGEMM ('N', 'N', na_uc, nbnd, na_uc, &
          (1.d0, 0.d0), &
          gam, 1, 1, electrons%Hmat%desc, &
          zloc, 1, 1, electrons%Zmat%desc, &
          (0.d0, 0.d0), &
          zrot, 1, 1, electrons%Zmat%desc)

        do iloc = 1, electrons%Zmat%Ml
          do jloc = 1, electrons%Zmat%Nl
            i = indxl2g(iloc, electrons%Zmat%Mb, electrons%kpoints%pool%myprow, 0, electrons%kpoints%pool%nprow)
            j = indxl2g(jloc, electrons%Zmat%Nb, electrons%kpoints%pool%mypcol, 0, electrons%kpoints%pool%npcol)

            if (i<1 .or. i>electrons%Zmat%M .or. j<1 .or. j>electrons%Zmat%N) cycle

            cnk(i,j,ik_star) = zrot(iloc,jloc)
          enddo
        enddo
      enddo
    enddo

    call mp_sum ( cnk, comm_world )

    deallocate(zloc, zg, zrot, gam)

  end subroutine read_cnk

end module epmat_m

