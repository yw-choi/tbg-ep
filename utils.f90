
module utils

  use mpi
  use kinds, only: dp

  implicit none

  public

contains

  subroutine print_walltime (str)
    character(len=*) :: str
    real(dp) :: sec
    real(dp), external :: get_clock

    sec = get_clock(str)
    if (ionode) then
      write(6,"(1x,a,F10.1)") "walltime: ", sec
    endif
    call mp_barrier( comm_world )

  end subroutine print_walltime

  subroutine printblock_start(msg)
    character(len=*) :: msg
    integer :: i, lenmsg, left, right, ierr
    integer, parameter :: WIDTH=60

    if (ionode) then

      lenmsg = len(msg)
      write(6, *)
      write(6, '(a,a,a)', advance='no'), '== ', trim(msg), ' '
      do i=1,WIDTH-4-lenmsg
        write(6, '(a)', advance='no') '='
      enddo
      write(6, *)
      write(6, *)
    endif

    call mp_barrier(comm_world)

  end subroutine printblock_start

  subroutine distribute_indices(n, nprocs, displs, recvcounts)
    integer, intent(in) :: n, nprocs
    integer, intent(out) :: displs(0:nprocs-1),recvcounts(0:nprocs-1)

    integer :: iproc, nloc, iloc

    do iproc=0,nprocs-1

        nloc = int(n/nprocs)

        if (iproc < mod(n, nprocs)) then
          nloc = nloc + 1
          iloc = iproc*int(n/nprocs)+iproc
        else
          iloc = iproc*int(n/nprocs)+mod(n, nprocs)
        endif

        displs(iproc) = iloc
        recvcounts(iproc) = nloc
    enddo

  end subroutine distribute_indices

end module utils
