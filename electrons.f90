module electrons_m
  use mpi
  use mpi_pool

  use kinds, only: &
    DP

  use constants, only: &
    RYTOEV, PI, TPI, ANGSTROM_AU, eps16, eps8 

  use diag, only: diagonalize, diag_setup, diag_cleanup

  use lattice, only: struct_t
  use kgrid, only: kpoints_t, get_kq
  use input, only: params_t
  use distrib_mat, only: distrib_mat_t, read_matrix, write_matrix

  use hoppings, only: t_SK
  use ktetra, only: tetra_weights

  use dos, only: dos_tetra, compute_dos_weights

  use global_m, only: verbosity, outdir, VERBOSITY_DEBUG

  implicit none
  public

  type, public :: electrons_t
    real(dp) :: rcut
    real(dp) :: efield
    real(dp) :: delta

    integer :: lb, hb, nbnd
    real(dp) :: ef, nelec !, nelec_all

    real(dp), allocatable :: enk(:,:)
    ! real(dp), allocatable :: enk_all(:,:)
    class(struct_t), pointer :: struct
    class(kpoints_t), pointer :: kpoints
    class(mpi_pool_t), pointer :: pool

    type(distrib_mat_t) :: Hmat
    type(distrib_mat_t) :: Zmat

    contains
      procedure :: solve_all
      procedure :: read_enk
      procedure :: write_enk
      procedure :: find_fermi_energy
      procedure :: calc_dos
      procedure :: set_kpoints
      procedure :: nesting_fn
      procedure :: analyze_wfn

      procedure :: destroy

      procedure, private :: build_hamiltonian
      procedure, private :: check_eigvec_symmetry

  end type electrons_t

  interface electrons_t
    module procedure new_electrons_t
  end interface electrons_t

contains

type(electrons_t) function new_electrons_t(params,struct,kpoints) result (this)
  class(params_t), intent(in) :: params
  class(struct_t), target, intent(in) :: struct
  class(kpoints_t), target, intent(in) :: kpoints

  this%rcut = params%rcut_tb
  this%struct => struct
  this%kpoints => kpoints
  this%pool => kpoints%pool
  this%efield = params%efield
  this%delta = params%delta

  if (params%lowest_band.le.0) then
    this%lb = this%struct%na_uc/2 - 1
  else
    this%lb = params%lowest_band
  endif

  if (params%highest_band.le.0) then
    this%hb = this%struct%na_uc/2 + 2
  else
    this%hb = params%highest_band
  endif

  this%nbnd = this%hb-this%lb+1

  allocate(this%enk(this%nbnd,kpoints%nk)) 
  ! allocate(this%enk_all(this%hb,kpoints%nk))
  this%ef = 0.d0
  this%enk = 0.d0
  ! this%enk_all = 0.d0
  ! this%nelec_all = dble(this%struct%na_uc) ! - (this%lb-1)*2
  this%nelec = dble(this%struct%na_uc) - (this%lb-1)*2

  call this%Hmat%setup_mat_2d(struct%na_uc, struct%na_uc, this%pool)
  call this%Zmat%setup_mat_2d(struct%na_uc, this%nbnd, this%pool)

  if (ionode) then
    print *, 'na_uc / 2 = ', struct%na_uc / 2
    print *, "lowest band = ", this%lb
    print *, "highest band = ", this%hb
    print *, "nbnd = ", this%nbnd
  endif

  if (this%pool%rank.eq.0) then
    print *, "pool, Hmat%Mb, Hmat%Nb = ", this%pool%mypool, this%Hmat%Mb, this%Hmat%Nb 
  endif

  call mp_barrier ( comm_world )

end function new_electrons_t

subroutine set_kpoints(this, kpoints)
  class(electrons_t), intent(inout) :: this
  class(kpoints_t), target, intent(in) :: kpoints

  this%kpoints => kpoints
  if (allocated(this%enk)) deallocate(this%enk)
  ! if (allocated(this%enk_all)) deallocate(this%enk_all)
  allocate(this%enk(this%nbnd,kpoints%nk))
  allocate(this%enk(this%hb,kpoints%nk))

end subroutine set_kpoints

subroutine read_enk(this,data_prefix)
  class(electrons_t), intent(inout) :: this
  character(len=*), intent(in) :: data_prefix
  character(len=1) :: dummy
  integer :: n1, n2, ik

  if (ionode) then
    open(11,file=trim(outdir)//'/'//trim(data_prefix)//'_enk.dat',form='unformatted')
    read(11) this%ef
    read(11) this%enk
    close(11)
  endif

  call mp_bcast(this%ef,0,comm_world)
  call mp_bcast(this%enk,0,comm_world)

end subroutine read_enk

subroutine solve_all(this, solver, write_cnk, data_prefix)
  class(electrons_t), intent(inout) :: this
  integer, intent(in) :: solver
  character(len=*), intent(in) :: data_prefix
  logical, intent(in) :: write_cnk

  complex(dp), allocatable :: H(:,:), Z(:,:), work(:,:)
  real(dp), allocatable :: W(:)

  class(struct_t), pointer :: struct
  class(kpoints_t), pointer :: kpoints
  class(mpi_pool_t), pointer :: pool

  integer :: ik, info, ibnd, ibnd_g, ibnd_l, ia, ia_g, isym
  character(len=256) :: fn

  integer :: i, j, na_uc
  real(dp), external :: get_clock

  write(stdout, "(1x,a,F10.1,a)") "Electrons solve_all start @ ", get_clock('ep'), ' s' 

  struct => this%struct
  kpoints => this%kpoints
  pool => kpoints%pool

  na_uc = this%Hmat%M

  call start_clock ( '  el-setup' )
  allocate(H(this%Hmat%Ml,this%Hmat%Nl), Z(this%Hmat%Ml,this%Hmat%Nl), W(this%Hmat%N))
  allocate(work(this%Hmat%M,this%nbnd))
  call diag_setup (this%Hmat, this%hb, solver)
  call stop_clock ( '  el-setup' )

  !! main loop
  this%enk = 0.d0
  ! this%enk_all = 0.d0
  call start_clock ( '  el-solve_k' )
  do ik = kpoints%startk, kpoints%endk

    write(stdout, "(3x,a,i5,a,i5,a,F10.1,a)") "Solving kpt # ", ik, "/", kpoints%nk_pool, ' @ ', get_clock('ep'), ' s'

    call start_clock ( '    el-buildH' )
    call this%build_hamiltonian (this%kpoints%xk(1:3,ik), H)
    call stop_clock ( '    el-buildH' )

    call start_clock ( '    el-diag' )
    call diagonalize(H, W, Z)
    call stop_clock ( '    el-diag' )

    ! this%enk_all(:,ik) = W
    this%enk(1:this%nbnd,ik) = W(this%lb:this%hb)

    if (write_cnk) then
      call start_clock ( '    el-iovec' )
      write(fn, '(a,i0.5,a)') trim(outdir)//'/'//trim(data_prefix)//'_cnk.',ik,'.dat'
      call write_matrix (fn, this%Hmat, Z, this%Hmat%M, this%nbnd, 1, this%lb)
      call stop_clock ( '    el-iovec' )
    endif
  enddo
  call stop_clock ( '  el-solve_k' )

  write(stdout, "(3x,a)") "Waiting for unfinished pools.."
  call mp_barrier ( comm_world )

  call mp_sum (this%enk, pool%inter_comm)
  ! call mp_sum (this%enk_all, pool%inter_comm)

  write(stdout, "(1x,a,F10.1,a)") "Electrons solve_all end @ ", get_clock('ep'), ' s' 

  if (verbosity>=VERBOSITY_DEBUG.and.trim(data_prefix)=='grid') then
    call start_clock ( '  el-check' )
    call this%check_eigvec_symmetry
    call stop_clock ( '  el-check' )
  endif

  call start_clock ( '  el-cleanup' )

  deallocate(H, W, Z, work)
  nullify(kpoints, struct, pool)

  call diag_cleanup

  call stop_clock ( '  el-cleanup' )
      
end subroutine solve_all

subroutine write_enk(this,data_prefix)
  class(electrons_t), intent(in) :: this
  character(len=*), intent(in) :: data_prefix

  integer :: ik, ibnd

  if (ionode) then
    open(11,file=trim(outdir)//'/'//trim(data_prefix)//'_enk.txt',form='formatted')
    write(11,'(a,2I8,F20.12)') '#', this%kpoints%nk, this%nbnd, this%ef*RYTOEV
    do ik = 1, this%kpoints%nk
      do ibnd = 1, this%nbnd
        write(11,'(F20.12)',advance='no') this%enk(ibnd,ik)*RYTOEV 
      enddo
      write(11,*)
    enddo
    close(11)

    open(11,file=trim(outdir)//'/'//trim(data_prefix)//'_enk.dat',form='unformatted')
    write(11) this%ef
    write(11) this%enk
    close(11)
  endif

  call mp_barrier(comm_world)
end subroutine write_enk

subroutine find_fermi_energy(this)
  class(electrons_t), intent(inout) :: this
  integer, allocatable :: isk(:)
  real(dp), external :: efermit
  ! Finding fermi energy 
  call start_clock ('  el-fermi')
  allocate(isk(this%kpoints%nk))
  isk = 1

  this%ef = efermit (this%enk, this%nbnd, this%kpoints%nk, this%nelec, 1, this%kpoints%ntetra, this%kpoints%tetra, 0, isk)
  ! this%ef = efermit (this%enk_all, this%hb, this%kpoints%nk, this%nelec_all, 1, this%kpoints%ntetra, this%kpoints%tetra, 0, isk)
  if (ionode) then
    print *, "ef (eV) = ", this%ef * RYTOEV
  endif
  deallocate(isk)
  call stop_clock ('  el-fermi')

end subroutine find_fermi_energy

subroutine build_hamiltonian (this, k, H)
  class(electrons_t), intent(in) :: this
  real(dp), intent(in) :: k(3)
  complex(dp), intent(out) :: H(this%Hmat%Ml, this%Hmat%Nl)

  integer :: ia_loc, ja_loc, ia_uc_l, il, jl, inb, ja_sc_l, &
    ja_uc_l, ja_uc, ia_uc
  real(dp) :: d(3), dist, hop, arg
  complex(dp) :: matel

  integer :: iprow, ipcol
  integer, external :: indxl2g

  H = 0.d0
  do ia_uc = 1, this%Hmat%M
    call infog1l(ia_uc, this%Hmat%Mb, this%pool%nprow, this%pool%myprow, 0, ia_loc, iprow)
    if (iprow/=this%Hmat%myprow) then
      cycle
    endif

    ia_uc_l = mod(ia_uc-1,this%struct%na_uc_l)+1
    il = int((ia_uc-1)/this%struct%na_uc_l)+1 

    ! do jl = il-1, il+1
    !   if (jl<0 .or. jl>this%struct%nlayer) cycle
    do jl = 1, this%struct%nlayer
      do ja_sc_l = 1, this%struct%na_sc_l
        ja_uc_l = this%struct%sc_uc_map(ja_sc_l,jl)

        ja_uc = (jl-1)*this%struct%na_uc_l+ja_uc_l

        call infog1l(ja_uc, this%Hmat%Nb, this%Hmat%npcol, this%Hmat%mypcol, 0, ja_loc, ipcol)

        if (ipcol/=this%Hmat%mypcol) then
          cycle
        endif

        d = this%struct%atoms_sc(1:3,ja_sc_l,jl)-this%struct%atoms_uc(1:3,ia_uc_l,il)
        dist = norm2(d)

        if (dist>this%rcut .or. dist<eps8) then
          cycle
        endif

        hop = t_SK(d)

        arg = TPI*sum(k*this%struct%R_isc(1:3,this%struct%isc_map(ja_sc_l,jl)))

        matel = hop*cmplx(cos(arg),sin(arg),kind=dp)

        H(ia_loc, ja_loc) = H(ia_loc, ja_loc) + matel
      enddo
    enddo
  enddo

  do ia_loc = 1, this%Hmat%Ml
    ia_uc = indxl2g(ia_loc, this%Hmat%Mb, this%pool%myprow, 0, this%pool%nprow)
    if (ia_uc<1.or.ia_uc>this%Hmat%M) cycle

    ia_uc_l = mod(ia_uc-1,this%struct%na_uc_l)+1
    il = int((ia_uc-1)/this%struct%na_uc_l)+1 
    do ja_loc = 1, this%Hmat%Nl
      ja_uc = indxl2g(ja_loc, this%Hmat%Nb, this%pool%mypcol, 0, this%pool%npcol)
      if (ja_uc<1.or.ja_uc>this%Hmat%N) cycle

      if (ia_uc==ja_uc) then

        H(ia_loc,ja_loc) = H(ia_loc,ja_loc) + this%efield*this%struct%atoms_uc(3,ia_uc_l,il)

      endif
    enddo
  enddo

  call pzgeadd('C', this%Hmat%M, this%Hmat%N, (0.5d0, 0.0d0), H, 1, 1, this%Hmat%desc,  (0.5d0, 0.0d0), H, 1, 1, this%Hmat%desc)

end subroutine build_hamiltonian

subroutine calc_dos(this,ne,emin,emax)
  class(electrons_t), intent(inout) :: this
  integer, intent(in) :: ne
  real(dp), intent(in) :: emin, emax

  integer :: ie
  real(dp), allocatable :: dos(:), int_dos(:), egrid(:) 

  call start_clock('  el-dos')

  allocate(dos(ne),int_dos(ne),egrid(ne))

  call dos_tetra(this%nbnd,this%kpoints,ne,this%ef+emin,this%ef+emax,this%enk,&
    egrid,dos,int_dos)

  if (ionode) then
    print *, 'dos_ne = ', ne
    print *, 'dos_emin = ', emin*RYTOEV
    print *, 'dos_emax = ', emax*RYTOEV
    open(11,file=trim(outdir)//'/edos.dat',form='formatted')

    do ie = 1, ne
      write(11,"(3F24.12)") RYTOEV*(egrid(ie)-this%ef), dos(ie)/RYTOEV, int_dos(ie)
    enddo

    close(11)
  endif

  deallocate(dos,int_dos)

  call mp_barrier(comm_world)

  call stop_clock('  el-dos')

end subroutine calc_dos

subroutine destroy(this)
  class(electrons_t), intent(inout) :: this
  
  deallocate(this%enk)
  nullify(this%struct, this%kpoints, this%pool)
end subroutine

subroutine check_eigvec_symmetry(this)
  class(electrons_t), intent(inout) :: this

  complex(dp), allocatable :: &
    H_irr(:,:), &
    Z_irr(:,:), &
    H_star(:,:), &
    Z_rot(:,:), &
    work(:,:), &
    gam(:,:), &
    R(:,:)

  real(dp) :: residue, resmax, xk_irr(3), xk_rot(3), xk_star(3)
  complex(dp) :: overlap

  integer :: ik, info, ibnd, ibnd_g, ibnd_l, ia, ia_g, isym
  character(len=256) :: fn

  integer :: i, j, ik_star, irot, k, ia_uc, ia_uc_l, &
    il, jl, ja_uc, ja_uc_l, ik_irr, istar, ig, jg

  integer :: nk_irr, nk_tot, na_uc, nbnd

  integer, external :: indxl2g

  type(distrib_mat_t) :: Hmat, Zmat, Rmat

  real(dp), external :: get_clock

  na_uc = this%struct%na_uc
  nbnd = this%nbnd
  call Hmat%setup_mat_2d(na_uc, na_uc, this%pool)
  call Zmat%setup_mat_2d(na_uc, nbnd, this%pool)
  call Rmat%setup_mat_2d(nbnd, nbnd, this%pool)

  allocate(H_irr(Hmat%Ml,Hmat%Nl))
  allocate(Z_irr(Zmat%Ml,Zmat%Nl))
  allocate(H_star(Hmat%Ml,Hmat%Nl))
  allocate(gam(Hmat%Ml,Hmat%Nl))
  allocate(Z_rot(Zmat%Ml,Zmat%Nl))
  allocate(work(Zmat%Ml,Zmat%Nl))
  allocate(R(Rmat%Ml,Rmat%Nl))

  nk_irr = this%kpoints%nk
  nk_tot = this%kpoints%nkmax
  write(stdout, *)
  write(stdout, "(1x,a)") "Start checking symmetry of electron eigenvectors"
  write(stdout, *)

  do ik_irr = this%kpoints%startk, this%kpoints%endk
    write(stdout, "(1x,a,i5,a,i5,a,F10.1,a)") "Checking symmetry ik_irr # ", ik_irr, '/', nk_irr, ' @ ', get_clock('ep'), ' s'
    
    write(fn, '(a,i0.5,a)') trim(outdir)//'/grid_cnk.',ik_irr,'.dat'
    call read_matrix(fn, Zmat, Z_irr)

    xk_irr = this%kpoints%xk(1:3, ik_irr)

    do istar = 1, this%kpoints%n_stars(ik_irr)
      ik_star = this%kpoints%stars(istar, ik_irr)
      isym = this%kpoints%equiv_s(ik_star)

      xk_star = this%kpoints%xkg(1:3,ik_star)

      write(stdout, "(3x,a,i5,a,i5,a,i5,a,F10.1,a)") "istar, ik_star, isym # ", istar, ',', ik_star, ',', isym, ' @ ', get_clock('ep'), ' s'

      ! build H_star
      call this%build_hamiltonian (xk_star, H_star)

      ! rotate Z_irr
      call gamma_matrix (this%struct, Hmat, xk_irr, isym, gam)

      call PZGEMM ('N', 'N', na_uc, nbnd, na_uc, &
        (1.d0, 0.d0), &
        gam, 1, 1, Hmat%desc, &
        Z_irr, 1, 1, Zmat%desc, &
        (0.d0, 0.d0), &
        Z_rot, 1, 1, Zmat%desc)

      ! calculate residue
      ! R = Z_rot^H * H_star * Z_rot
      call PZGEMM ('N', 'N', na_uc, nbnd, na_uc, &
        (1.d0, 0.d0), &
        H_star, 1, 1, Hmat%desc, &
        Z_rot, 1, 1, Zmat%desc, &
        (0.d0, 0.d0), &
        work, 1, 1, Zmat%desc) ! Z_irr is here temporary array ( H_star * Z_rot )

      call PZGEMM ('C', 'N', nbnd, nbnd, na_uc, &
        (1.d0, 0.d0), &
        Z_rot, 1, 1, Zmat%desc, &
        work, 1, 1, Zmat%desc, &
        (0.d0, 0.d0), &
        R, 1, 1, Rmat%desc)

      resmax = 0.d0

      do i = 1, Rmat%Ml
        do j = 1, Rmat%Nl
          ig = indxl2g(i, Rmat%Mb, this%pool%myprow, 0, this%pool%nprow)
          jg = indxl2g(j, Rmat%Nb, this%pool%mypcol, 0, this%pool%npcol)
          if (ig<1.or.ig>Rmat%M) cycle
          if (jg<1.or.jg>Rmat%N) cycle

          if (ig==jg) then
            residue = abs(this%enk(ig,ik_irr)-R(i,j))
          else
            residue = abs(R(i,j))
          endif

          if (residue > eps8) then
            print *, 'ig, jg, residue = ', ig, jg, residue
            print '(3F12.6)', this%enk(ig,ik_irr), DBLE(R(i,j)), DIMAG(R(i,j))
            call errore('check_eigvec_symmetry', 'symmetry check failed', 1)
          endif
          if (resmax < residue) then
            resmax = residue
          endif

        enddo
      enddo

      write(stdout, '(3x,a,E12.4)') 'max residue = ', resmax
    enddo
  enddo

  call mp_barrier ( comm_world )

  deallocate(H_irr)
  deallocate(Z_irr)
  deallocate(H_star)
  deallocate(gam)
  deallocate(Z_rot)
  deallocate(work)
  deallocate(R)

end subroutine check_eigvec_symmetry

subroutine gamma_matrix (struct, Hmat, xk, isym, gam)
  class(struct_t), intent(in) :: struct
  class(distrib_mat_t), intent(in) :: Hmat
  real(dp), intent(in) :: xk(3)
  integer, intent(in) :: isym

  complex(dp), intent(out) :: gam(Hmat%Ml, Hmat%Nl)

  real(dp) :: k(3), dx(3), arg, scart(3,3)
  complex(dp) :: cfac
  integer, external :: indxl2g

  integer :: j_loc, j_g, i_g, i_loc, ia_uc, ia_uc_l, il, &
    iprow, ix, jx, ja_uc, ja_uc_l, jl, ism1, ipcol, irot

  gam = (0.d0, 0.d0)

  if (isym < 0) then
    call errore('gamma_matrix','isym<0 at gamma_matrix',isym)
  else if (isym == 1 .or. isym == 0) then
    ! Identity matrix
    do i_loc = 1, Hmat%Ml
      i_g = indxl2g(i_loc, Hmat%Mb, Hmat%pool%myprow, 0, Hmat%pool%nprow)
      if (i_g<1.or.i_g>Hmat%M) cycle
      call infog1l(i_g, Hmat%Nb, Hmat%pool%npcol, Hmat%pool%mypcol, 0, j_loc, ipcol)
      if (ipcol==Hmat%pool%mypcol) then
        gam(i_loc, j_loc) = (1.d0, 0.d0)
      endif
    enddo
    return
  endif
  
  ! Note:
  ! isym = equiv_s(ik_star) such that
  ! k_star = S_inv(isym) * k_irr
  ! Therefore, we use irot = invs(isym)
  irot = struct%invs(isym)

  k = matmul(struct%relatt, xk) ! xk_irr

  do j_loc = 1, Hmat%Nl
    ja_uc = indxl2g(j_loc, Hmat%Nb, Hmat%pool%mypcol, 0, Hmat%pool%npcol)
    if (ja_uc<1.or.ja_uc>Hmat%N) cycle

    ja_uc_l = mod(ja_uc-1,struct%na_uc_l)+1
    jl = int((ja_uc-1)/struct%na_uc_l)+1

    ia_uc = struct%sym_map(ja_uc_l,jl,irot)
    ia_uc_l = mod(ia_uc-1,struct%na_uc_l)+1
    il = int((ia_uc-1)/struct%na_uc_l)+1

    dx = matmul( struct%sr(:,:,struct%invs(irot)), struct%atoms_uc(:,ia_uc_l,il) ) - struct%atoms_uc(:,ja_uc_l,jl)
    arg = dot_product(k, dx)

    cfac = dcmplx(cos(arg), sin(arg))

    call infog1l(ia_uc, Hmat%Mb, Hmat%pool%nprow, Hmat%pool%myprow, 0, i_loc, iprow)
    if (iprow/=Hmat%pool%myprow) then
      cycle
    endif
    
    gam(i_loc, j_loc) =  cfac * struct%sr(3,3,irot) ! pz
  enddo

end subroutine gamma_matrix

subroutine nesting_fn(this, params)
  class(electrons_t), intent(in) :: this
  class(params_t), intent(in) :: params

  integer :: ief, ik, iq, ikq, ibnd, jbnd, nef
  real(dp) :: energy, N_F, efmin, efmax
  real(dp), allocatable :: dwnk(:,:), fq(:,:)

  character(len=256) :: fn

  allocate(dwnk(this%nbnd, this%kpoints%nkmax),fq(this%kpoints%nkmax,params%nef))

  nef = params%nef
  efmin = params%efmin
  efmax = params%efmax

  fq = 0.d0
  do ief = 1, params%nef
    energy = efmin + (ief-1)*(efmax-efmin)/(nef-1)
    call compute_dos_weights(this%nbnd, this%kpoints, this%ef+energy, this%enk, dwnk, N_F)

    do iq = 1, this%kpoints%nkmax
      do ik = 1, this%kpoints%nkmax
        call get_kq ( this%kpoints, this%kpoints, ik, iq, ikq )
        
        do ibnd = 1, this%nbnd
          do jbnd = 1, this%nbnd
            fq(iq,ief) = fq(iq,ief) + dwnk(ibnd, ik)*dwnk(jbnd, ikq)
          enddo
        enddo
      enddo
    enddo
  enddo

  if (ionode) then
    write(fn, '(a)') trim(outdir)//'/nesting_fn.dat'
    open(11, file=trim(fn), form='unformatted')
    write(11) fq
    close(11)
  endif

  call mp_barrier ( comm_world ) 

  deallocate(dwnk,fq)

end subroutine nesting_fn

subroutine analyze_wfn (this, data_prefix)
  class(electrons_t), intent(in) :: this
  character(len=*), intent(in) :: data_prefix

  complex(dp), allocatable :: &
    cnk(:,:,:), zloc(:,:)

  integer :: na_uc, nbnd, nk, ia_uc, ibnd, ik, il, ia_uc_l, isite, &
    na_uc_l
  integer :: iloc, jloc, i, j, nsite, nlayer

  character(len=256) :: fn

  logical :: exst

  real(dp), allocatable :: wnk(:,:,:,:)

  integer, external :: indxl2g

  na_uc = this%struct%na_uc
  na_uc_l = this%struct%na_uc_l
  nbnd = this%nbnd
  nk = this%kpoints%nk
  nsite = 2
  nlayer = this%struct%nlayer

  allocate(zloc(this%Zmat%Ml, this%Zmat%Nl)) 
  allocate(wnk(nsite,nlayer,nbnd,nk))

  wnk = 0.d0

  do ik = this%kpoints%startk, this%kpoints%endk
    write(fn, '(a,i0.5,a)') trim(outdir)//'/'//trim(data_prefix)//'_cnk.',ik,'.dat'
    inquire(file=fn, exist=exst)
    if (.not.exst) then
      write(6, "(1x,a,a)") "Skip analyze_wfn: File not found ", fn
      return
    endif

    call read_matrix ( fn, this%Zmat, zloc )
    do iloc = 1, this%Zmat%Ml
      do jloc = 1, this%Zmat%Nl
        i = indxl2g(iloc, this%Zmat%Mb, this%kpoints%pool%myprow, 0, this%kpoints%pool%nprow)
        j = indxl2g(jloc, this%Zmat%Nb, this%kpoints%pool%mypcol, 0, this%kpoints%pool%npcol)

        if (i<1 .or. i>this%Zmat%M .or. j<1 .or. j>this%Zmat%N) cycle

        ia_uc = i
        ia_uc_l = mod(ia_uc-1, na_uc_l)+1
        il = int((ia_uc-1)/na_uc_l)+1
        isite = mod(ia_uc-1,2)+1
        ibnd = j

        wnk(isite,il,ibnd,ik) = wnk(isite,il,ibnd,ik) + dconjg(zloc(iloc,jloc))*zloc(iloc,jloc)
      enddo
    enddo
  enddo

  deallocate(zloc)

  call mp_sum ( wnk, comm_world )

  if (ionode) then
    do ik = 1, nk
      write(6, *) 'ik = ', ik
      do ibnd = 1, nbnd
        write(6, *) 'ibnd = ', ibnd
        write(6, '(4x)', advance='no') 
        do il = 1, nlayer
          write(6, '(2x,I8)',advance='no') il
        enddo
        write(6, '(2x,a8)') 'Sum'

        do isite = 1, 2
          write(6, '(i4)', advance='no') isite

          do il = 1, nlayer
            write(6, '(2x,F8.6)',advance='no') wnk(isite,il,ibnd,ik) 
          enddo
          write(6, '(2x,F8.6)') sum(wnk(isite,:,ibnd,ik))
        enddo

        write(6, '(1x,a3)', advance='no') 'Sum'

        do il = 1, nlayer
          write(6, '(2x,F8.6)',advance='no') sum(wnk(:,il,ibnd,ik))
        enddo
        write(6, '(2x,F8.6)') sum(wnk(:,:,ibnd,ik))
        write(6,*)
      enddo
    enddo
  endif

  call mp_barrier ( comm_world )

end subroutine analyze_wfn

end module electrons_m
