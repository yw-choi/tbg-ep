module hoppings
  use mpi
  use kinds, only: DP
  use constants, only: eps12, eps6, RYTOEV, ANGSTROM_AU

  implicit none

  public :: t_SK, dtdx

  private
  
  ! Slater-Koster tight-binding parameters
  real(DP), parameter :: &
    Vpi  = -2.7d0/RYTOEV   , &
    Vsig =  0.48d0/RYTOEV  , &
    a0   =  2.46d0/dsqrt(3.d0)*ANGSTROM_AU,  &
    d0   =  3.35d0*ANGSTROM_AU, &
    r0   =  0.184d0 * 2.46d0*ANGSTROM_AU

contains

  ! Slater-Koster hopping function
  real(dp) function t_SK (d)
    real(dp), intent(in) :: d(3)
    real(dp) :: dfac, dist

    dist = norm2(d)
    dfac = (d(3)/dist)**2
    t_SK = Vpi*exp(-(dist-a0)/r0)*(1.d0-dfac)+Vsig*exp(-(dist-d0)/r0)*dfac

  end function t_SK

  ! derivative of hopping function w.r.t. ix axis,
  ! evaluated at d
  real(dp) function dtdx ( d, ix ) 
    integer, intent(in) :: ix
    real(dp), intent(in) :: d(3)
    real(dp) :: dist, fac1, fac2

    dist = norm2(d)

    !! Do not calculate duplicate parts
    if (ix.eq.1 .or. ix.eq.2) then

      fac1 = d(ix)*d(3)*d(3)/(r0*dist**3)
      fac2 = 2*d(ix)*d(3)*d(3)/dist**4

      dtdx = Vpi*exp(-(dist-a0)/r0)*( fac1 - d(ix)/(r0*dist) + fac2 ) &
             + Vsig*exp(-(dist-d0)/r0)*( -fac1 -fac2 ) 

    else
      fac1 = d(ix)*d(3)*d(3)/(r0*dist**3)
      fac2 = 2*d(3)*(dist**2-d(3)**2)/dist**4

      dtdx = Vpi*exp(-(dist-a0)/r0)*( fac1 - d(ix)/(r0*dist) - fac2 ) &
             + Vsig*exp(-(dist-d0)/r0)*( -fac1 + fac2 )
    endif

  end function dtdx

end module hoppings
