!
! Copyright (C) 2016 Quantum ESPRESSO Foundation
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!--------------------------------------------------------------------------
!
MODULE ktetra
  !
  ! ... Variables used by the tetrahedron method
  ! ... Three versions are implemented: Linear, Optimized, Bloechl
  ! ... Linear and Optimized tetrahedra contributed by Mitsuaki Kawamura,
  ! ... University of Tokyo
  !
  USE kinds, ONLY: dp
  ! use sort
  !
  IMPLICIT NONE
  !
  public
  !
CONTAINS
  !
  !-----------------------------------------------------------------------------
  SUBROUTINE tetra_init ( nsym, s, time_reversal, t_rev, at, bg, npk, &
     k1,k2,k3, nk1,nk2,nk3, nks, xk, ntetra, tetra )
  !-----------------------------------------------------------------------
  !
  ! Tetrahedron method according to P. E. Bloechl et al, PRB49, 16223 (1994)
  !
  USE kinds, ONLY: DP
  IMPLICIT NONE
  ! 
  INTEGER, INTENT(IN):: nks, nsym, t_rev(48), s(3,3,48), npk, &
                        k1, k2, k3, nk1, nk2, nk3
  LOGICAL, INTENT (IN) :: time_reversal
  real(DP), INTENT(IN) :: at(3,3), bg(3,3)
  real(DP), INTENT(INOUT) :: xk(3,npk)
  integer, intent(out) :: ntetra
  integer, allocatable, intent(out) :: tetra(:,:)
  !
  real(DP) :: xkr(3), deltap(3), deltam(3)
  real(DP), PARAMETER:: eps=1.0d-5
  real(DP), ALLOCATABLE :: xkg(:,:)
  INTEGER :: nkr, i,j,k, ns, n, nk, ip1,jp1,kp1, &
       n1,n2,n3,n4,n5,n6,n7,n8,nntetra
  INTEGER, ALLOCATABLE:: equiv(:)
  !
  ntetra =6*nk1*nk2*nk3
  nntetra=4
  IF(.NOT. ALLOCATED(tetra)) ALLOCATE ( tetra (nntetra, ntetra) )
  !
  ! Re-generate a uniform grid of k-points xkg
  !
  nkr=nk1*nk2*nk3
  ALLOCATE (xkg( 3,nkr))
  ALLOCATE (equiv( nkr))
!
  DO i=1,nk1
     DO j=1,nk2
        DO k=1,nk3
           !  this is nothing but consecutive ordering
           n = (k-1) + (j-1)*nk3 + (i-1)*nk2*nk3 + 1
           !  xkg are the components of the complete grid in crystal axis
           xkg(1,n) = dble(i-1)/nk1 + dble(k1)/2/nk1
           xkg(2,n) = dble(j-1)/nk2 + dble(k2)/2/nk2
           xkg(3,n) = dble(k-1)/nk3 + dble(k3)/2/nk3
        ENDDO
     ENDDO
  ENDDO

  !  locate k-points of the uniform grid in the list of irreducible k-points
  !  that was previously calculated
  !
  DO nk=1,nkr
     DO n=1,nks
        DO ns=1,nsym
           DO i=1,3
              xkr(i) = s(i,1,ns) * xk(1,n) + &
                       s(i,2,ns) * xk(2,n) + &
                       s(i,3,ns) * xk(3,n)
           ENDDO
           IF(t_rev(ns)==1) xkr = -xkr
           !  xkr is the n-th irreducible k-point rotated wrt the ns-th symmetry
           DO i=1,3
              deltap(i) = xkr(i)-xkg(i,nk) - nint (xkr(i)-xkg(i,nk) )
              deltam(i) = xkr(i)+xkg(i,nk) - nint (xkr(i)+xkg(i,nk) )
           ENDDO
           !  deltap is the difference vector, brought back in the first BZ
           !  deltam is the same but with k => -k (for time reversal)
           IF ( sqrt ( deltap(1)**2 + &
                       deltap(2)**2 + &
                       deltap(3)**2 ) < eps .or. ( time_reversal .and. &
                sqrt ( deltam(1)**2 +  &
                       deltam(2)**2 +  &
                       deltam(3)**2 ) < eps ) ) THEN
              !  equivalent irreducible k-point found
              equiv(nk) = n
              GOTO 15
           ENDIF
        ENDDO
     ENDDO
     !  equivalent irreducible k-point found - something wrong
     CALL errore('tetra_init','cannot locate  k point',nk)
15   CONTINUE
  ENDDO

  DO n=1,nks
     DO nk=1,nkr
        IF (equiv(nk)==n) GOTO 20
     ENDDO
     !  this failure of the algorithm may indicate that the displaced grid
     !  (with k1,k2,k3.ne.0) does not have the full symmetry of the lattice
     CALL errore('tetra_init','cannot remap grid on k-point list',n)
20   CONTINUE
  ENDDO

  !  construct tetrahedra

  DO i=1,nk1
     DO j=1,nk2
        DO k=1,nk3
           !  n1-n8 are the indices of k-point 1-8 forming a cube
           ip1 = mod(i,nk1)+1
           jp1 = mod(j,nk2)+1
           kp1 = mod(k,nk3)+1
           n1 = (  k-1) + (  j-1)*nk3 + (  i-1)*nk2*nk3 + 1
           n2 = (  k-1) + (  j-1)*nk3 + (ip1-1)*nk2*nk3 + 1
           n3 = (  k-1) + (jp1-1)*nk3 + (  i-1)*nk2*nk3 + 1
           n4 = (  k-1) + (jp1-1)*nk3 + (ip1-1)*nk2*nk3 + 1
           n5 = (kp1-1) + (  j-1)*nk3 + (  i-1)*nk2*nk3 + 1
           n6 = (kp1-1) + (  j-1)*nk3 + (ip1-1)*nk2*nk3 + 1
           n7 = (kp1-1) + (jp1-1)*nk3 + (  i-1)*nk2*nk3 + 1
           n8 = (kp1-1) + (jp1-1)*nk3 + (ip1-1)*nk2*nk3 + 1
           !  there are 6 tetrahedra per cube (and nk1*nk2*nk3 cubes)
           n  = 6 * ( (k-1) + (j-1)*nk3 + (i-1)*nk3*nk2 )

           tetra (1,n+1) = equiv(n1)
           tetra (2,n+1) = equiv(n2)
           tetra (3,n+1) = equiv(n3)
           tetra (4,n+1) = equiv(n6)

           tetra (1,n+2) = equiv(n2)
           tetra (2,n+2) = equiv(n3)
           tetra (3,n+2) = equiv(n4)
           tetra (4,n+2) = equiv(n6)

           tetra (1,n+3) = equiv(n1)
           tetra (2,n+3) = equiv(n3)
           tetra (3,n+3) = equiv(n5)
           tetra (4,n+3) = equiv(n6)

           tetra (1,n+4) = equiv(n3)
           tetra (2,n+4) = equiv(n4)
           tetra (3,n+4) = equiv(n6)
           tetra (4,n+4) = equiv(n8)

           tetra (1,n+5) = equiv(n3)
           tetra (2,n+5) = equiv(n6)
           tetra (3,n+5) = equiv(n7)
           tetra (4,n+5) = equiv(n8)

           tetra (1,n+6) = equiv(n3)
           tetra (2,n+6) = equiv(n5)
           tetra (3,n+6) = equiv(n6)
           tetra (4,n+6) = equiv(n7)
        ENDDO
     ENDDO
  ENDDO

  !  check

  DO n=1,ntetra
     DO i=1,nntetra
        IF ( tetra(i,n)<1 .or. tetra(i,n)>nks ) &
             CALL errore ('tetra_init','something wrong',n)
     ENDDO
  ENDDO

  DEALLOCATE(equiv)
  DEALLOCATE(xkg)

  RETURN
  END SUBROUTINE tetra_init
  !
  !--------------------------------------------------------------------
  subroutine tetra_weights (nks, nbnd, ntetra, &
       tetra, enk, ef, wg, dwg)

    USE kinds
    implicit none
    ! I/O variables
    integer, intent(in) :: nks, nbnd, ntetra, &
         tetra (4, ntetra)
    real(DP), intent(in) :: enk (nbnd, nks), ef
    ! wg must be (inout) and not (out) because if is/=0 only terms for
    ! spin=is are initialized; the remaining terms should be kept, not lost
    real(DP), intent(inout) :: wg (nbnd, nks), dwg(nbnd,nks)
    ! local variables
    real(DP) :: &
      ef1, ef2, ef3, ef4, e21, e31, e41, e32, e42, e43, &
      w1, w2, w3, w4, dw1, dw2, dw3, dw4, &
      e1, e2, e3, e4, c1, c2, c3, c, &
      dc1, dc2, dc3, dc, &
      etetra (4), dosef, ddosef, wg1, dwg1
    integer :: ik, ibnd, nt, nk, ns, i, kp1, kp2, kp3, kp4, itetra (4), jbnd, kbnd
    !
    wg = 0.d0
    dwg = 0.d0
    do nt = 1, ntetra
       do ibnd = 1, nbnd
          !
          ! etetra are the energies at the vertexes of the nt-th tetrahedron
          !
          do i = 1, 4
             etetra (i) = enk (ibnd, tetra (i, nt))
          enddo
          itetra (1) = 0
          call hpsort (4, etetra, itetra)
          
          ! ...sort in ascending order: e1 < e2 < e3 < e4
          !
          e1 = etetra (1)
          e2 = etetra (2)
          e3 = etetra (3)
          e4 = etetra (4)
          ef1 = ef-e1
          ef2 = ef-e2
          ef3 = ef-e3
          ef4 = ef-e4
          e21 = e2-e1
          e31 = e3-e1
          e41 = e4-e1
          e32 = e3-e2
          e42 = e4-e2
          e43 = e4-e3
          !
          ! kp1-kp4 are the irreducible k-points corresponding to e1-e4
          !
          kp1 = tetra (itetra (1), nt)
          kp2 = tetra (itetra (2), nt)
          kp3 = tetra (itetra (3), nt)
          kp4 = tetra (itetra (4), nt)
          if(ef.le.e1) then
             w1 = 0.0
             w2 = 0.0
             w3 = 0.0
             w4 = 0.0
             dw1 = 0.0
             dw2 = 0.0
             dw3 = 0.0
             dw4 = 0.0
          elseif(ef.le.e2) then
             c = ef1*ef1*ef1/(e21*e31*e41)*0.25D0
             dc = 3*ef1*ef1/(e21*e31*e41)*0.25D0
             w1 = c*(4.0-ef1*(1.0/e21+1.0/e31+1.0/e41))
             w2 = c*(ef1/e21)
             w3 = c*(ef1/e31)
             w4 = c*(ef1/e41)
             dw1 = dc*(4.0-ef1*(1.0/e21+1.0/e31+1.0/e41)) & 
               - c*(1.0/e21+1.0/e31+1.0/e41)
             dw2 = dc*(ef1/e21)+c/e21
             dw3 = dc*(ef1/e31)+c/e31
             dw4 = dc*(ef1/e41)+c/e41
          elseif(ef.le.e3) then
             c1 = (ef1*ef1)/(e41*e31)*0.25D0
             c2 = -(ef1*ef2*ef3)/(e41*e32*e31)*0.25D0
             c3 = -(ef2*ef2*ef4)/(e42*e32*e41)*0.25D0
             dc1 = 2*ef1/(e41*e31)*0.25D0
             dc2 = -(ef1*ef2+ef2*ef3+ef1*ef3)/(e41*e32*e31)*0.25D0
             dc3 = -(2*ef2*ef4+ef2*ef2)/(e42*e32*e41)*0.25D0
             w1 = c1-(c1+c2)*ef3/e31-(c1+c2+c3)*ef4/e41
             w2 = c1+c2+c3-(c2+c3)*ef3/e32-c3*ef4/e42
             w3 = (c1+c2)*ef1/e31+(c2+c3)*ef2/e32
             w4 = (c1+c2+c3)*ef1/e41+c3*ef2/e42
             dw1 = dc1-(dc1+dc2)*ef3/e31-(dc1+dc2+dc3)*ef4/e41 &
               -(c1+c2)/e31-(c1+c2+c3)/e41
             dw2 = dc1+dc2+dc3-(dc2+dc3)*ef3/e32-dc3*ef4/e42 &
               -(c2+c3)/e32-c3/e42
             dw3 = (dc1+dc2)*ef1/e31+(dc2+dc3)*ef2/e32 &
               + (c1+c2)/e31+(c2+c3)/e32
             dw4 = (dc1+dc2+dc3)*ef1/e41+dc3*ef2/e42 &
               +(c1+c2+c3)/e41+c3/e42
          elseif(ef.le.e4) then
             c = -ef4*ef4*ef4/(e41*e42*e43)*0.25D0
             dc = -3.0*ef4*ef4/(e41*e42*e43)*0.25D0
             w1 = 0.25D0+c*ef4/e41
             w2 = 0.25D0+c*ef4/e42
             w3 = 0.25D0+c*ef4/e43
             w4 = 0.25D0-c*(4.0+ef4*(1.0/e41+1.0/e42+1.0/e43)) 
             dw1 = dc*ef4/e41+c/e41
             dw2 = dc*ef4/e42+c/e42
             dw3 = dc*ef4/e43+c/e43
             dw4 = -dc*(4.0+ef4*(1.0/e41+1.0/e42+1.0/e43)) &
               -c*(1.0/e41+1.0/e42+1.0/e43)
          else
             w1 = 0.25D0
             w2 = 0.25D0
             w3 = 0.25D0
             w4 = 0.25D0
             dw1 = 0.0d0
             dw2 = 0.0d0
             dw3 = 0.0d0
             dw4 = 0.0d0
          endif

          wg(ibnd, kp1) = wg(ibnd,kp1) + w1
          wg(ibnd, kp2) = wg(ibnd,kp2) + w2
          wg(ibnd, kp3) = wg(ibnd,kp3) + w3
          wg(ibnd, kp4) = wg(ibnd,kp4) + w4
          dwg(ibnd, kp1) = dwg(ibnd,kp1) + dw1
          dwg(ibnd, kp2) = dwg(ibnd,kp2) + dw2
          dwg(ibnd, kp3) = dwg(ibnd,kp3) + dw3
          dwg(ibnd, kp4) = dwg(ibnd,kp4) + dw4
       enddo
    enddo

    wg = wg / ntetra
    dwg = dwg / ntetra

    !
    ! Average weights of degenerated states
    !
    DO ik = 1, nks
       DO ibnd = 1, nbnd
          !
          wg1 = wg(ibnd,ik)
          dwg1 = dwg(ibnd,ik)
          !
          DO jbnd = ibnd + 1, nbnd
             !
             IF(ABS(enk(ibnd,ik) - enk(jbnd,ik)) < 1e-6_dp) THEN
                wg1 = wg1 + wg(jbnd,ik)
                dwg1 = dwg1 + dwg(jbnd,ik)
             ELSE
                !
                DO kbnd = ibnd, jbnd - 1
                   wg(kbnd,ik) = wg1 / real(jbnd - ibnd, dp)
                   dwg(kbnd,ik) = dwg1 / real(jbnd - ibnd, dp)
                END DO
                !
                EXIT
             END IF
             !
          END DO
          !
       END DO
    END DO

    return
  end subroutine tetra_weights
  !----------------------------------------------------------------------------
!
END MODULE ktetra

!
! Copyright (C) 2001-2003 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
!-----------------------------------------------------------------------
function sumkg (et, nbnd, nks, wk, degauss, ngauss, e, is, isk)
  !-----------------------------------------------------------------------
  !
  !     This function computes the number of states under a given energy e
  !
  !
  USE kinds
  implicit none
  ! Output variable
  real(DP) :: sumkg
  ! Input variables
  integer, intent(in) :: nks, nbnd, ngauss
  ! input: the total number of K points
  ! input: the number of bands
  ! input: the type of smearing
  real(DP), intent(in) :: wk (nks), et (nbnd, nks), degauss, e
  ! input: the weight of the k points
  ! input: the energy eigenvalues
  ! input: gaussian broadening
  ! input: the energy to check
  integer, intent(in) :: is, isk(nks)
  !
  ! local variables
  !
  real(DP), external :: wgauss
  ! function which compute the smearing
  real(DP) ::sum1
  integer :: ik, ibnd
  ! counter on k points
  ! counter on the band energy
  !
  sumkg = 0.d0
  do ik = 1, nks
     sum1 = 0.d0
     if (is /= 0) then
        if (isk(ik).ne.is) cycle
     end if
     do ibnd = 1, nbnd
        sum1 = sum1 + wgauss ( (e-et (ibnd, ik) ) / degauss, ngauss)
     enddo
     sumkg = sumkg + wk (ik) * sum1
  enddo
  return
end function sumkg

!
! Copyright (C) 2001-2011 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!--------------------------------------------------------------------
FUNCTION sumkt (et, nbnd, nks, nspin, ntetra, tetra, e, is, isk)
  !--------------------------------------------------------------------
  !
  ! ... Sum over all states with tetrahedron method
  ! ... At Fermi energy e=E_F, sumkt(e) == number of electrons
  ! ... Generalization to noncollinear case courtesy of Iurii Timrov
  !
  USE kinds
  implicit none
  ! output variable
  real(DP) :: sumkt
  ! input variable
  integer, intent(in) :: nbnd, nks, nspin, ntetra, tetra (4, ntetra)
  real(DP), intent(in) :: et (nbnd, nks), e
  integer, intent(in) :: is, isk
  ! local variables
  real(DP) :: etetra (4), e1, e2, e3, e4
  integer :: nt, nk, ns, ibnd, i, nspin_lsda

  IF ( nspin == 2 ) THEN
     nspin_lsda = 2
  ELSE
     nspin_lsda = 1
  END IF
  sumkt = 0.0d0
  do ns = 1, nspin_lsda
     if (is /= 0) then
        if ( ns .ne. is) cycle
     end if
     !
     ! nk is used to select k-points with up (ns=1) or down (ns=2) spin
     !
     if (ns.eq.1) then
        nk = 0
     else
        nk = nks / 2
     endif
     do nt = 1, ntetra
        do ibnd = 1, nbnd
           !
           ! etetra are the energies at the vertexes of the nt-th tetrahedron
           !
           do i = 1, 4
              etetra (i) = et (ibnd, tetra (i, nt) + nk)
           enddo
           call piksort (4, etetra)
           !
           ! ...sort in ascending order: e1 < e2 < e3 < e4
           !
           e1 = etetra (1)
           e2 = etetra (2)
           e3 = etetra (3)
           e4 = etetra (4)
           !
           ! calculate sum over k of the integrated charge
           !
           if (e.ge.e4) then
              sumkt = sumkt + 1.d0 / ntetra
           elseif (e.lt.e4.and.e.ge.e3) then
              sumkt = sumkt + 1.d0 / ntetra * (1.0d0 - (e4 - e) **3 / (e4 - e1) &
                   / (e4 - e2) / (e4 - e3) )
           elseif (e.lt.e3.and.e.ge.e2) then
              sumkt = sumkt + 1.d0 / ntetra / (e3 - e1) / (e4 - e1) * &
                   ( (e2 - e1) **2 + 3.0d0 * (e2 - e1) * (e-e2) + 3.0d0 * (e-e2) **2 - &
                   (e3 - e1 + e4 - e2) / (e3 - e2) / (e4 - e2) * (e-e2) **3)
           elseif (e.lt.e2.and.e.ge.e1) then
              sumkt = sumkt + 1.d0 / ntetra * (e-e1) **3 / (e2 - e1) / &
                   (e3 - e1) / (e4 - e1)
           endif
        enddo
     enddo
  enddo

  ! add correct spin normalization (2 for LDA, 1 for other cases)

  IF ( nspin == 1 ) sumkt = sumkt * 2.d0

  return

end function sumkt

subroutine piksort (n, a)
  USE kinds
  implicit none
  integer :: n

  real(DP) :: a (n)
  integer :: i, j
  real(DP) :: temp
  !
  do j = 2, n
     temp = a (j)
     do i = j - 1, 1, - 1
        if (a (i) .le.temp) goto 10
        a (i + 1) = a (i)
     enddo
     i = 0
10   a (i + 1) = temp
  enddo
  !
  return
end subroutine piksort
!
! Copyright (C) 2001 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
!-----------------------------------------------------------------------
function w0gauss (x, n)
  !-----------------------------------------------------------------------
  !
  !     the derivative of wgauss:  an approximation to the delta function
  !
  ! --> (n>=0) : derivative of the corresponding Methfessel-Paxton wgauss
  !
  ! --> (n=-1 ): derivative of cold smearing:
  !              1/sqrt(pi)*exp(-(x-1/sqrt(2))**2)*(2-sqrt(2)*x)
  !
  ! --> (n=-99): derivative of Fermi-Dirac function: 0.5/(1.0+cosh(x))
  !
  USE kinds, ONLY : DP
  USE constants, ONLY : sqrtpm1
  implicit none
  real(DP) :: w0gauss, x
  ! output: the value of the function
  ! input: the point where to compute the function

  integer :: n
  ! input: the order of the smearing function
  !
  !    here the local variables
  !
  real(DP) :: a, arg, hp, hd
  ! the coefficients a_n
  ! the argument of the exponential
  ! the hermite function
  ! the hermite function

  integer :: i, ni
  ! counter on n values
  ! counter on 2n values

  ! Fermi-Dirac smearing

  if (n.eq. - 99) then
     if (abs (x) .le.36.0) then
        w0gauss = 1.0d0 / (2.0d0 + exp ( - x) + exp ( + x) )
        ! in order to avoid problems for large values of x in the e
     else
        w0gauss = 0.d0
     endif
     return

  endif
  ! cold smearing  (Marzari-Vanderbilt)
  if (n.eq. - 1) then
     arg = min (200.d0, (x - 1.0d0 / sqrt (2.0d0) ) **2)
     w0gauss = sqrtpm1 * exp ( - arg) * (2.0d0 - sqrt ( 2.0d0) * x)
     return

  endif

  if (n.gt.10 .or. n.lt.0) call errore('w0gauss','higher order smearing is untested and unstable',abs(n))

  ! Methfessel-Paxton
  arg = min (200.d0, x**2)
  w0gauss = exp ( - arg) * sqrtpm1
  if (n.eq.0) return
  hd = 0.0d0
  hp = exp ( - arg)
  ni = 0
  a = sqrtpm1
  do i = 1, n
     hd = 2.0d0 * x * hp - 2.0d0 * DBLE (ni) * hd
     ni = ni + 1
     a = - a / (DBLE (i) * 4.0d0)
     hp = 2.0d0 * x * hd-2.0d0 * DBLE (ni) * hp
     ni = ni + 1
     w0gauss = w0gauss + a * hp
  enddo
  return
end function w0gauss
!
! Copyright (C) 2001 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
!-----------------------------------------------------------------------
function wgauss (x, n)
  !-----------------------------------------------------------------------
  !
  !     this function computes the approximate theta function for the
  !     given order n, at the point x.
  !
  ! --> (n>=0) : Methfessel-Paxton case. See PRB 40, 3616 (1989).
  !
  ! --> (n=-1 ): Cold smearing (Marzari-Vanderbilt). See PRL 82, 3296 (1999)
  !       1/2*erf(x-1/sqrt(2)) + 1/sqrt(2*pi)*exp(-(x-1/sqrt(2))**2) + 1/2
  !
  ! --> (n=-99): Fermi-Dirac case: 1.0/(1.0+exp(-x)).
  !
  USE kinds, ONLY : DP
  USE constants, ONLY : pi
  implicit none
  real(DP) :: wgauss, x
  ! output: the value of the function
  ! input: the argument of the function
  integer :: n
  ! input: the order of the function
  !
  !    the local variables
  !

  real(DP) :: a, hp, arg, hd, xp
  ! the coefficient a_n
  ! the hermitean function
  ! the argument of the exponential
  ! the hermitean function
  ! auxiliary variable (cold smearing)
  integer :: i, ni
  ! counter on the n indices
  ! counter on 2n
  real(DP), external :: gauss_freq, qe_erf
  real(DP), parameter :: maxarg = 200.d0
  ! maximum value for the argument of the exponential

  ! Fermi-Dirac smearing
  if (n.eq. - 99) then
     if (x.lt. - maxarg) then
        wgauss = 0.d0
     elseif (x.gt.maxarg) then
        wgauss = 1.d0
     else
        wgauss = 1.0d0 / (1.0d0 + exp ( - x) )
     endif
     return

  endif
  ! Cold smearing
  if (n.eq. - 1) then
     xp = x - 1.0d0 / sqrt (2.0d0)
     arg = min (maxarg, xp**2)
     wgauss = 0.5d0 * qe_erf (xp) + 1.0d0 / sqrt (2.0d0 * pi) * exp ( - &
          arg) + 0.5d0
     return

  endif
  ! Methfessel-Paxton
  wgauss = gauss_freq (x * sqrt (2.0d0) )
  if (n.eq.0) return
  hd = 0.d0
  arg = min (maxarg, x**2)
  hp = exp ( - arg)
  ni = 0
  a = 1.d0 / sqrt (pi)
  do i = 1, n
     hd = 2.0d0 * x * hp - 2.0d0 * DBLE (ni) * hd
     ni = ni + 1
     a = - a / (DBLE (i) * 4.0d0)
     wgauss = wgauss - a * hd
     hp = 2.0d0 * x * hd-2.0d0 * DBLE (ni) * hp
     ni = ni + 1
  enddo
  return
end function wgauss
!
! Copyright (C) 2002-2009 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!---------------------------------------------------------------------
function qe_erf (x)  
  !---------------------------------------------------------------------
  !
  !     Error function - computed from the rational approximations of
  !     W. J. Cody, Math. Comp. 22 (1969), pages 631-637.
  !
  !     for abs(x) le 0.47 erf is calculated directly
  !     for abs(x) gt 0.47 erf is calculated via erf(x)=1-erfc(x)
  !
  use kinds, only : DP
  implicit none  
  real(DP), intent(in) :: x
  real(DP) :: x2, p1 (4), q1 (4)
  real(DP), external :: qe_erfc  
  real(DP) :: qe_erf
  data p1 / 2.426679552305318E2_DP, 2.197926161829415E1_DP, &
            6.996383488619136_DP,  -3.560984370181538E-2_DP /
  data q1 / 2.150588758698612E2_DP, 9.116490540451490E1_DP, &
            1.508279763040779E1_DP, 1.000000000000000_DP /
  !
  if (abs (x) > 6.0_DP) then  
     !
     !  erf(6)=1-10^(-17) cannot be distinguished from 1
     !
     qe_erf = sign (1.0_DP, x)  
  else  
     if (abs (x)  <= 0.47_DP) then  
        x2 = x**2  
        qe_erf=x *(p1 (1) + x2 * (p1 (2) + x2 * (p1 (3) + x2 * p1 (4) ) ) ) &
                / (q1 (1) + x2 * (q1 (2) + x2 * (q1 (3) + x2 * q1 (4) ) ) )
     else  
        qe_erf = 1.0_DP - qe_erfc (x)  
     endif
  endif
  !
  return  
end function qe_erf
!
!---------------------------------------------------------------------
function qe_erfc (x)  
  !---------------------------------------------------------------------
  !
  !     erfc(x) = 1-erf(x)  - See comments in erf
  !
  use kinds, only : DP
  implicit none  
  real(DP),intent(in) :: x
  real(DP)            :: qe_erfc
  real(DP) :: ax, x2, xm2, p2 (8), q2 (8), p3 (5), q3 (5), pim1
  real(DP), external :: qe_erf  
  data p2 / 3.004592610201616E2_DP,  4.519189537118719E2_DP, &
            3.393208167343437E2_DP,  1.529892850469404E2_DP, &
            4.316222722205674E1_DP,  7.211758250883094_DP,   &
            5.641955174789740E-1_DP,-1.368648573827167E-7_DP /
  data q2 / 3.004592609569833E2_DP,  7.909509253278980E2_DP, &
            9.313540948506096E2_DP,  6.389802644656312E2_DP, &
            2.775854447439876E2_DP,  7.700015293522947E1_DP, &
            1.278272731962942E1_DP,  1.000000000000000_DP /
  data p3 /-2.996107077035422E-3_DP,-4.947309106232507E-2_DP, &
           -2.269565935396869E-1_DP,-2.786613086096478E-1_DP, &
           -2.231924597341847E-2_DP /
  data q3 / 1.062092305284679E-2_DP, 1.913089261078298E-1_DP, &
            1.051675107067932_DP,    1.987332018171353_DP,    &
            1.000000000000000_DP /

  data pim1 / 0.56418958354775629_DP /  
  !        ( pim1= sqrt(1/pi) )
  ax = abs (x)  
  if (ax > 26.0_DP) then  
     !
     !  erfc(26.0)=10^(-296); erfc( 9.0)=10^(-37);
     !
     qe_erfc = 0.0_DP  
  elseif (ax > 4.0_DP) then  
     x2 = x**2  
     xm2 = (1.0_DP / ax) **2  
     qe_erfc = (1.0_DP / ax) * exp ( - x2) * (pim1 + xm2 * (p3 (1) &
          + xm2 * (p3 (2) + xm2 * (p3 (3) + xm2 * (p3 (4) + xm2 * p3 (5) &
          ) ) ) ) / (q3 (1) + xm2 * (q3 (2) + xm2 * (q3 (3) + xm2 * &
          (q3 (4) + xm2 * q3 (5) ) ) ) ) )
  elseif (ax > 0.47_DP) then  
     x2 = x**2  
     qe_erfc = exp ( - x2) * (p2 (1) + ax * (p2 (2) + ax * (p2 (3) &
          + ax * (p2 (4) + ax * (p2 (5) + ax * (p2 (6) + ax * (p2 (7) &
          + ax * p2 (8) ) ) ) ) ) ) ) / (q2 (1) + ax * (q2 (2) + ax * &
          (q2 (3) + ax * (q2 (4) + ax * (q2 (5) + ax * (q2 (6) + ax * &
          (q2 (7) + ax * q2 (8) ) ) ) ) ) ) )
  else  
     qe_erfc = 1.0_DP - qe_erf (ax)  
  endif
  !
  ! erf(-x)=-erf(x)  =>  erfc(-x) = 2-erfc(x)
  !
  if (x < 0.0_DP) qe_erfc = 2.0_DP - qe_erfc  
  !
  return  
end function qe_erfc
!
!---------------------------------------------------------------------
function gauss_freq (x)
  !---------------------------------------------------------------------
  !
  !     gauss_freq(x) = (1+erf(x/sqrt(2)))/2 = erfc(-x/sqrt(2))/2
  !             - See comments in erf
  !
  use kinds, only : DP
  implicit none
  real(DP),intent(in) :: x
  real(DP)            :: gauss_freq
  real(DP), parameter :: c =  0.7071067811865475_DP
  !        ( c= sqrt(1/2) )
  real(DP), external :: qe_erfc
  !
  gauss_freq = 0.5_DP * qe_erfc ( - x * c)
  !
  return
end function gauss_freq


!
! Copyright (C) 2001-2003 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
!-----------------------------------------------------------------------
subroutine cryst_to_cart (nvec, vec, trmat, iflag)
  !-----------------------------------------------------------------------
  !
  !     This routine transforms the atomic positions or the k-point
  !     components from crystallographic to cartesian coordinates 
  !     ( iflag=1 ) and viceversa ( iflag=-1 ).
  !     Output cartesian coordinates are stored in the input ('vec') array
  !
  !
  USE kinds, ONLY : DP
  implicit none
  !
  integer, intent(in) :: nvec, iflag
  ! nvec:  number of vectors (atomic positions or k-points)
  !        to be transformed from crystal to cartesian and vice versa
  ! iflag: gives the direction of the transformation
  real(DP), intent(in) :: trmat (3, 3)
  ! trmat: transformation matrix
  ! if iflag=1:
  !    trmat = at ,  basis of the real-space lattice,       for atoms   or
  !          = bg ,  basis of the reciprocal-space lattice, for k-points
  ! if iflag=-1: the opposite
  real(DP), intent(inout) :: vec (3, nvec)
  ! coordinates of the vector (atomic positions or k-points) to be
  ! transformed - overwritten on output
  !
  !    local variables
  !
  integer :: nv, kpol
  ! counter on vectors
  ! counter on polarizations
  real(DP) :: vau (3)
  ! workspace
  !
  !     Compute the cartesian coordinates of each vectors
  !     (atomic positions or k-points components)
  !
  do nv = 1, nvec
     if (iflag.eq.1) then
        do kpol = 1, 3
           vau (kpol) = trmat (kpol, 1) * vec (1, nv) + trmat (kpol, 2) &
                * vec (2, nv) + trmat (kpol, 3) * vec (3, nv)
        enddo
     else
        do kpol = 1, 3
           vau (kpol) = trmat (1, kpol) * vec (1, nv) + trmat (2, kpol) &
                * vec (2, nv) + trmat (3, kpol) * vec (3, nv)
        enddo
     endif
     do kpol = 1, 3
        vec (kpol, nv) = vau (kpol)
     enddo
  enddo
  !
  return
end subroutine cryst_to_cart

!
! Copyright (C) 2001-2003 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!--------------------------------------------------------------------
FUNCTION efermig (et, nbnd, nks, nelec, wk, Degauss, Ngauss, is, isk)
  !--------------------------------------------------------------------
  !
  !     Finds the Fermi energy - Gaussian Broadening
  !     (see Methfessel and Paxton, PRB 40, 3616 (1989 )
  !
  USE kinds,     ONLY : DP
  USE constants, ONLY : rytoev
  implicit none
  !  I/O variables
  integer, intent(in) :: nks, nbnd, Ngauss, is, isk(nks)
  real(DP), intent(in) :: wk (nks), et (nbnd, nks), Degauss, nelec
  real(DP) :: efermig
  !
  real(DP), parameter :: eps= 1.0d-10
  integer, parameter :: maxiter = 300
  ! internal variables
  real(DP) :: Ef, Eup, Elw, sumkup, sumklw, sumkmid
  real(DP), external::  sumkg
  integer :: i, kpoint
  !
  !      find bounds for the Fermi energy. Very safe choice!
  !
  Elw = et (1, 1)
  Eup = et (nbnd, 1)
  do kpoint = 2, nks
     Elw = min (Elw, et (1, kpoint) )
     Eup = max (Eup, et (nbnd, kpoint) )
  enddo
  Eup = Eup + 2 * Degauss
  Elw = Elw - 2 * Degauss
  !
  !      Bisection method
  !
  sumkup = sumkg (et, nbnd, nks, wk, Degauss, Ngauss, Eup, is, isk)
  sumklw = sumkg (et, nbnd, nks, wk, Degauss, Ngauss, Elw, is, isk)
  if ( (sumkup - nelec) < -eps .or. (sumklw - nelec) > eps )  &
       call errore ('efermig', 'internal error, cannot bracket Ef', 1)
  do i = 1, maxiter
     Ef = (Eup + Elw) / 2.d0
     sumkmid = sumkg (et, nbnd, nks, wk, Degauss, Ngauss, Ef, is, isk)
     if (abs (sumkmid-nelec) < eps) then
        efermig = Ef
        return
     elseif ( (sumkmid-nelec) < -eps) then
        Elw = Ef
     else
        Eup = Ef
     endif
  enddo
  if (is /= 0) WRITE(6, '(5x,"Spin Component #",i3)') is
  WRITE( 6, '(5x,"Warning: too many iterations in bisection"/ &
       &      5x,"Ef = ",f10.6," sumk = ",f10.6," electrons")' ) &
       Ef * rytoev, sumkmid
  !
  efermig = Ef
  return
end FUNCTION efermig

!
! Copyright (C) 2001-2003 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
!--------------------------------------------------------------------
FUNCTION efermit (et, nbnd, nks, nelec, nspin, ntetra, tetra, is, isk)
  !--------------------------------------------------------------------
  !
  !     Finds the Fermi energy - tetrahedron method
  !     (see P. E. Bloechl et al, PRB49, 16223 (1994))
  !
  USE kinds, ONLY: DP
  USE constants, ONLY: rytoev
  implicit none
  integer, intent(in)  :: nks, nbnd, nspin, ntetra, tetra (4, ntetra)
  ! nks   : the number of k points
  ! nbnd  : the number of bands
  ! nspin : the number of spin components
  ! ntetra: the number of tetrahedra
  ! tetra : the vertices of a tetrahedron
  real(DP), intent(in) :: et (nbnd, nks), nelec
  ! input: the eigenvalues
  ! input: the number of electrons
  real(DP):: efermit
  ! output: the fermi energy
  integer, intent(in) :: is, isk(nks)
  !
  !     two parameters
  !
  integer, parameter :: maxiter = 300
  ! the maximum number of iterations in bisection

  real(DP), parameter :: eps= 1.0d-10
  ! a small quantity
  !
  !     here the local variables
  !
  integer :: nlw, ik, iter
  ! the minimum energy band
  ! counter on k points
  ! counter on iterations

  real(DP) :: ef, elw, eup, sumkup, sumklw, sumkmid
  ! elw, eup: lower and upper bounds for fermi energy (ef)
  ! sumklw, sumkup: number of states for ef=elw, ef=eup resp.
  ! sumkmid:        number of states for ef=(elw+eup)/2
  real(DP), external :: sumkt

  real(DP) :: efbetter, better
  !
  !      find bounds for the Fermi energy.
  !
  nlw = max (1, nint (nelec / 2.0d0 - 5.0d0) )
  elw = et (nlw, 1)
  eup = et (nbnd, 1)
  do ik = 2, nks
     elw = min (elw, et (nlw, ik) )
     eup = max (eup, et (nbnd, ik) )
  enddo
  !
  !      Bisection method
  !
  sumkup = sumkt (et, nbnd, nks, nspin, ntetra, tetra, eup, is, isk)
  sumklw = sumkt (et, nbnd, nks, nspin, ntetra, tetra, elw, is, isk)
  better = 1.0d+10
  if ( (sumkup - nelec) < -eps .or. (sumklw - nelec) > eps)  then
     !
     ! this is a serious error and the code should stop here
     ! we don't stop because this may occasionally happen in nonscf
     ! calculations where it may be completely irrelevant
     !
     call infomsg ('efermit', 'internal error, cannot bracket Ef')
     efermit = better
     return
  end if
  do iter = 1, maxiter
     ef = (eup + elw) / 2.d0
     sumkmid = sumkt (et, nbnd, nks, nspin, ntetra, tetra, ef, is, isk)
     if (abs (sumkmid-nelec) < better) then
        better = abs (sumkmid-nelec)
        efbetter = ef
     endif
     ! converged
     if (abs (sumkmid-nelec) < eps) then
        goto 100
     elseif ( (sumkmid-nelec) < -eps) then
        elw = ef
     else
        eup = ef
     endif

  enddo
  !     unconverged exit:
  !     the best available ef is used . Needed in some difficult cases
  ef = efbetter
  sumkmid = sumkt (et, nbnd, nks, nspin, ntetra, tetra, ef, is, isk )

  if (is /= 0) WRITE(6, '(5x,"Spin Component #",i3)') is
  WRITE( 6, 9010) ef * rytoev, sumkmid
  !     converged exit:
100 continue
  !     Check if Fermi level is above any of the highest eigenvalues
  do ik = 1, nks
     if (is /= 0) then
        if (isk(ik) /= is ) cycle
     end if
     if (ef > et (nbnd, ik) + 1.d-4) &
          WRITE( 6, 9020) ef * rytoev, ik, et (nbnd, ik) * rytoev
  enddo

  efermit = ef
  return
9010 format (/5x,'Warning: too many iterations in bisection'/ &
       &          5x,'ef = ',f10.6,' sumk = ',f10.6,' electrons')

9020 format (/5x,'Warning: ef =',f10.6, &
       &     ' is above the highest band at k-point',i4,/5x,9x, &
       &     'e  = ',f10.6)
end FUNCTION efermit

