module tbg
  use kinds, only: DP
  use constants, only: ANGSTROM_AU
  use matrix_inversion, only: invmat

  implicit none

  public :: generate_tbg_unitcell
  public :: generate_ttg_unitcell

  private 
contains

  subroutine generate_tbg_unitcell (nlayer1, nlayer2, M, N, alat, &
                  d_layer, latt, na_uc_l, atoms_uc, costh, theta)

    integer, intent(in) :: &
      nlayer1, nlayer2, &
      M, N        ! commensurate supercell index

    real(dp), intent(in) :: &
      alat,    & ! MLG lattice constant in Bohr
      d_layer

    real(dp), intent(out) :: &
      latt(3,3), & ! CUC lattice vector
      costh, theta ! commensurate rotation angle
    integer, intent(out) :: &
      na_uc_l      ! # of atoms in CUC
    real(dp), allocatable, intent(out) :: &
      atoms_uc(:,:,:) ! atoms_uc(3,na_uc_l,nlayer) atom positions in Bohr

    !! Local variables
    integer :: &
      nlayer, & ! total number of layers = nlayer1+nlayer2
      ncell     ! number of primitive unit cells per layer in a CUC 

    integer, parameter :: &
      NA_PUC  = 2  

    real(dp) :: &
      latt_puc_orig(3,3) ! lattice vector of the unrotated primitive unit cell

    real(dp), allocatable :: &
      latt_puc(:,:,:), & ! latt_puc(3,3,nlayer) lattice vector of the primitive unit cell of each layer 
      rot(:,:,:),      & ! rot(3,3,nlayer) rotation matrix for each layer: R(th/2), R(-th/2)
      sites(:,:,:)       ! sites(2,2,nlayer) 

    integer, allocatable :: &
      cells(:,:,:) ! primitive lattice points of each layer inside the commensurate unit cell

    integer :: ia_offset, na_loc , ia, icell, ilayer, ia_uc, isite
    real(dp) :: invlatt(3,3), atf(2)
    real(dp) :: c ! vacuum (only used for exporting geometry)
    real(dp) :: z, z_frac

    nlayer = nlayer1+nlayer2
    ncell   = N*N+N*M+M*M
    na_uc_l = NA_PUC*ncell
    
    allocate(latt_puc(3,3,nlayer), rot(3,3,nlayer))
    allocate(atoms_uc(3,na_uc_l,nlayer))

    if (N.eq.1.and.M.eq.0 .or. M.eq.1.and.N.eq.0) then
      costh = 1.0d0
      theta = 0.d0
    else
      costh = (N*N+4.d0*N*M+M*M)/(2.d0*(N*N+N*M+M*M))
      theta = acos(costh)
    endif

    ! vacuum direction (usually not used) 
    c = 100.0
   
    latt_puc_orig(:,1) = alat*(/ dsqrt(3.d0)/2, -1.d0/2, 0.d0 /)
    latt_puc_orig(:,2) = alat*(/ dsqrt(3.d0)/2,  1.d0/2, 0.d0 /)
    latt_puc_orig(:,3) =    c*(/         0.d0,    0.d0, 1.d0 /)

    do ilayer = 1, nlayer1
      rot(1,:,ilayer) = (/  cos(theta/2),  sin(theta/2), 0.d0 /)
      rot(2,:,ilayer) = (/ -sin(theta/2),  cos(theta/2), 0.d0 /)
      rot(3,:,ilayer) = (/          0.d0,          0.d0, 1.d0 /)
    enddo

    do ilayer = 1, nlayer2
      rot(1,:,nlayer1+ilayer) = (/  cos(theta/2), -sin(theta/2), 0.d0 /)
      rot(2,:,nlayer1+ilayer) = (/  sin(theta/2),  cos(theta/2), 0.d0 /)
      rot(3,:,nlayer1+ilayer) = (/          0.d0,          0.d0, 1.d0 /)
    enddo

    do ilayer = 1, nlayer
      latt_puc(:,:,ilayer) = matmul(rot(:,:,ilayer), latt_puc_orig)
    enddo

    latt = 0.d0
    latt(1:2,1) =  N*latt_puc(1:2,1,1) +     M*latt_puc(1:2,2,1)
    latt(1:2,2) = -M*latt_puc(1:2,1,1) + (N+M)*latt_puc(1:2,2,1)
    latt(3,3)   =  c 

    allocate(cells(3,ncell,nlayer))
    cells = 0
    do ilayer = 1, nlayer1
      call find_lattice_points(N, M, ncell, cells(:,:,ilayer))
    enddo
    do ilayer = 1, nlayer2
      call find_lattice_points(M, N, ncell, cells(:,:,nlayer1+ilayer))
    enddo

    allocate(sites(2,2,nlayer))
    !! AB stacking
    do ilayer = 1, nlayer1
      if (mod(ilayer,2).eq.1) then
        sites(1:2,1, ilayer) = (/ 1.d0/3.d0, 1.d0/3.d0 /)
        sites(1:2,2, ilayer) = (/ 2.d0/3.d0, 2.d0/3.d0 /)
      else
        sites(1:2,1, ilayer) = (/ 2.d0/3.d0, 2.d0/3.d0 /)
        sites(1:2,2, ilayer) = (/ 3.d0/3.d0, 3.d0/3.d0 /)
      endif
    enddo

    do ilayer = 1, nlayer2
      if (mod(ilayer,2).eq.1) then
        sites(1:2,1, nlayer1+ilayer) = (/ 1.d0/3.d0, 1.d0/3.d0 /)
        sites(1:2,2, nlayer1+ilayer) = (/ 2.d0/3.d0, 2.d0/3.d0 /)
        ! sites(1:2,1, nlayer1+ilayer) = (/ 2.d0/3.d0, 2.d0/3.d0 /)
        ! sites(1:2,2, nlayer1+ilayer) = (/ 3.d0/3.d0, 3.d0/3.d0 /)
      else
        sites(1:2,1, nlayer1+ilayer) = (/ 2.d0/3.d0, 2.d0/3.d0 /)
        sites(1:2,2, nlayer1+ilayer) = (/ 3.d0/3.d0, 3.d0/3.d0 /)
        ! sites(1:2,1, nlayer1+ilayer) = (/ 1.d0/3.d0, 1.d0/3.d0 /)
        ! sites(1:2,2, nlayer1+ilayer) = (/ 2.d0/3.d0, 2.d0/3.d0 /)
      endif
    enddo

    call invmat(3, latt, invlatt)
    do ilayer = 1, nlayer
      z = (ilayer-1)*d_layer - d_layer / 2.d0 - (nlayer1 - 1) * d_layer

      ia_uc = 0
      do icell = 1, ncell 
        do isite = 1, 2
          ia_uc = ia_uc + 1

          atoms_uc(1:2, ia_uc, ilayer) = &
            matmul(latt_puc(1:2,1:2,ilayer), &
              cells(1:2,icell,ilayer) + sites(1:2,isite,ilayer))

          atoms_uc(3, ia_uc, ilayer) = z

          ! laterally shift to the unit cell
          atf = matmul(invlatt(1:2,1:2), atoms_uc(1:2,ia_uc,ilayer))
          atf(1:2) = atf(1:2) - floor(atf(1:2))

          atoms_uc(1:2, ia_uc, ilayer) = matmul(latt(1:2,1:2),atf(1:2))
        enddo
      enddo
    enddo

    z = sum(atoms_uc(3,:,:))/(nlayer*na_uc_l)
    atoms_uc(3,:,:) = atoms_uc(3,:,:) - z 
  end subroutine generate_tbg_unitcell

  subroutine generate_ttg_unitcell(M, N, alat, &
                  d_layer, latt, na_uc_l, atoms_uc, costh, theta, &
                  shift)

    integer, intent(in) :: &
      M, N       ! commensurate supercell index

    real(dp), intent(in) :: &
      alat,    & ! MLG lattice constant in Bohr
      d_layer, &
      shift(2)   ! shift between 1/3 layer in puc fractional coords.

    real(dp), intent(out) :: &
      latt(3,3), & ! CUC lattice vector
      costh, theta ! commensurate rotation angle
    integer, intent(out) :: &
      na_uc_l      ! # of atoms in CUC
    real(dp), allocatable, intent(out) :: &
      atoms_uc(:,:,:) ! atoms_uc(3,na_uc_l,nlayer) atom positions in Bohr

    !! Local variables
    integer :: &
      nlayer, & ! total number of layers = nlayer1+nlayer2
      ncell     ! number of primitive unit cells per layer in a CUC 

    integer, parameter :: &
      NA_PUC  = 2  

    real(dp) :: &
      latt_puc_orig(3,3) ! lattice vector of the unrotated primitive unit cell

    real(dp), allocatable :: &
      latt_puc(:,:,:), & ! latt_puc(3,3,nlayer) lattice vector of the primitive unit cell of each layer 
      rot(:,:,:),      & ! rot(3,3,nlayer) rotation matrix for each layer: R(th/2), R(-th/2)
      sites(:,:,:)       ! sites(2,2,nlayer) 

    integer, allocatable :: &
      cells(:,:,:) ! primitive lattice points of each layer inside the commensurate unit cell

    integer :: ia_offset, na_loc , ia, icell, ilayer, ia_uc, isite
    real(dp) :: invlatt(3,3), atf(2)
    real(dp) :: c ! vacuum (only used for exporting geometry)
    real(dp) :: z, z_frac

    nlayer  = 3
    ncell   = N*N+N*M+M*M
    na_uc_l = NA_PUC*ncell
    
    allocate(latt_puc(3,3,nlayer), rot(3,3,nlayer))
    allocate(atoms_uc(3,na_uc_l,nlayer))

    if (N.eq.1.and.M.eq.0 .or. M.eq.1.and.N.eq.0) then
      costh = 1.0d0
      theta = 0.d0
    else
      costh = (N*N+4.d0*N*M+M*M)/(2.d0*(N*N+N*M+M*M))
      theta = acos(costh)
    endif

    ! vacuum direction (usually not used) 
    c = 100.0
   
    latt_puc_orig(:,1) = alat*(/ dsqrt(3.d0)/2, -1.d0/2, 0.d0 /)
    latt_puc_orig(:,2) = alat*(/ dsqrt(3.d0)/2,  1.d0/2, 0.d0 /)
    latt_puc_orig(:,3) =    c*(/         0.d0,    0.d0, 1.d0 /)

    rot(1,:,1) = (/  cos(theta/2),  sin(theta/2), 0.d0 /)
    rot(2,:,1) = (/ -sin(theta/2),  cos(theta/2), 0.d0 /)
    rot(3,:,1) = (/          0.d0,          0.d0, 1.d0 /)

    rot(1,:,2) = (/  cos(theta/2), -sin(theta/2), 0.d0 /)
    rot(2,:,2) = (/  sin(theta/2),  cos(theta/2), 0.d0 /)
    rot(3,:,2) = (/          0.d0,          0.d0, 1.d0 /)

    rot(1,:,3) = (/  cos(theta/2),  sin(theta/2), 0.d0 /)
    rot(2,:,3) = (/ -sin(theta/2),  cos(theta/2), 0.d0 /)
    rot(3,:,3) = (/          0.d0,          0.d0, 1.d0 /)

    do ilayer = 1, nlayer
      latt_puc(:,:,ilayer) = matmul(rot(:,:,ilayer), latt_puc_orig)
    enddo

    latt = 0.d0
    latt(1:2,1) =  N*latt_puc(1:2,1,1) +     M*latt_puc(1:2,2,1)
    latt(1:2,2) = -M*latt_puc(1:2,1,1) + (N+M)*latt_puc(1:2,2,1)
    latt(3,3)   =  c 

    allocate(cells(3,ncell,nlayer))
    cells = 0
    call find_lattice_points(N, M, ncell, cells(:,:,1))
    call find_lattice_points(M, N, ncell, cells(:,:,2))
    call find_lattice_points(N, M, ncell, cells(:,:,3))

    allocate(sites(2,2,nlayer))
    sites(1:2,1,1) = (/ 1.d0/3.d0, 1.d0/3.d0 /)
    sites(1:2,2,1) = (/ 2.d0/3.d0, 2.d0/3.d0 /)
    sites(1:2,1,2) = (/ 1.d0/3.d0, 1.d0/3.d0 /)
    sites(1:2,2,2) = (/ 2.d0/3.d0, 2.d0/3.d0 /)

    sites(1:2,1,3) = (/ 1.d0/3.d0, 1.d0/3.d0 /) + shift
    sites(1:2,2,3) = (/ 2.d0/3.d0, 2.d0/3.d0 /) + shift

    call invmat(3, latt, invlatt)
    do ilayer = 1, nlayer
      z = (ilayer-1)*d_layer 

      ia_uc = 0
      do icell = 1, ncell 
        do isite = 1, 2
          ia_uc = ia_uc + 1

          atoms_uc(1:2, ia_uc, ilayer) = &
            matmul(latt_puc(1:2,1:2,ilayer), &
              cells(1:2,icell,ilayer) + sites(1:2,isite,ilayer))

          atoms_uc(3, ia_uc, ilayer) = z

          ! laterally shift to the unit cell
          atf = matmul(invlatt(1:2,1:2), atoms_uc(1:2,ia_uc,ilayer))
          atf(1:2) = atf(1:2) - floor(atf(1:2))

          atoms_uc(1:2, ia_uc, ilayer) = matmul(latt(1:2,1:2),atf(1:2))
        enddo
      enddo
    enddo

    z = sum(atoms_uc(3,:,:))/(nlayer*na_uc_l)
    atoms_uc(3,:,:) = atoms_uc(3,:,:) - z 

  end subroutine generate_ttg_unitcell

  subroutine find_lattice_points(N, M, ncell, lattice_points)

    integer, intent(in) :: N, M, ncell
    integer, intent(out) :: lattice_points(3,ncell)

    integer :: ip, i1, i2
    real(dp) :: fM, fN

    if (N.eq.0.and.M.eq.1 .or. M.eq.0.and.N.eq.1) then
      lattice_points(:, :) = 0
      return
    endif

    fM = dble(M)
    fN = dble(N)
    ip = 0
    do i1 = -M, N-1
      do i2 = 0, N+2*M-1
        if (                fM/fN*i1 .le. i2 .and. &
                      -(fN+fM)/fM*i1 .le. i2 .and. &
               fN+fM + fM/fN*(i1+fM) .gt. i2 .and. &
             fM - (fN+fM)/fM*(i1-fN) .gt. i2) then
          ip = ip + 1
          lattice_points(:, ip) = (/ i1, i2, 0 /)
        endif
      enddo
    enddo
  end subroutine find_lattice_points
end module tbg
