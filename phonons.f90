module phonons_m
  use mpi
  use mpi_pool 
  use kinds, only: dp
  use constants, only: TPI, eps32, ANGSTROM_AU, RYTOEV, eps16, eps12, eps6, eps8
  use input, only: params_t
  use lattice, only: struct_t
  use distrib_mat, only: distrib_mat_t, write_matrix_all, read_matrix
  use kgrid, only: kpoints_t
  use dynmat, only: fc_intra, NNB_INTRA, fc_kc, fc_kc_fd
  use ktetra, only: tetra_weights
  use dos, only: dos_tetra
  use global_m, only: verbosity, outdir, VERBOSITY_DEBUG
  use diag, only: diag_setup, diagonalize, diag_cleanup

  implicit none

  type, public :: phonons_t
    real(dp) :: rcut
    real(dp) :: mass
    integer :: nmode
    real(dp), allocatable :: wvq(:,:) 
    real(dp), allocatable :: wvq2(:,:)

    class(struct_t), pointer :: struct
    class(kpoints_t), pointer :: qpoints
    class(mpi_pool_t), pointer :: pool

    type(distrib_mat_t) :: Dmat

    !! Force constants
    integer :: nfc
    real(dp), allocatable:: fc(:)
    integer, allocatable :: fc_indx(:,:) ! fc_indx(3,nfc) i_loc, j_loc, isc

    integer :: nfc_diag
    real(dp), allocatable:: fc_diag(:) ! fc_diag(Dmat%Nl)
    integer, allocatable :: fc_diag_indx(:,:) ! fc_diag_indx(2,nfc) i_loc, j_loc

    logical :: has_fc

    contains

      procedure :: setup_fc
      procedure :: set_qpoints
      procedure :: solve_all
      procedure :: read_wvq
      procedure :: write_wvq
      procedure :: calc_dos
      ! procedure :: check_symmetries

      procedure, private :: collect_fc
      procedure, private :: collect_fc_diag
      procedure, private :: build_dynmat
      procedure, private :: check_eigvec_symmetry

  end type phonons_t

  interface phonons_t
    module procedure new_phonons_t
  end interface phonons_t

  public :: gamma_matrix

  private

contains

type(phonons_t) function new_phonons_t(params,struct,qpoints) result (this)
  class(params_t), intent(in) :: params
  class(struct_t), target, intent(in) :: struct
  class(kpoints_t), target, intent(in) :: qpoints

  this%has_fc = .false.
  this%mass = params%mass
  this%rcut = params%rcut_ph
  this%nmode = 3*struct%na_uc
  this%struct => struct
  this%qpoints => qpoints
  this%pool => qpoints%pool

  allocate(this%wvq(this%nmode,qpoints%nk))
  allocate(this%wvq2(this%nmode,qpoints%nk))

  call this%Dmat%setup_mat_2d(this%nmode, this%nmode, this%pool)

  if (ionode) then
    print *, 'nmode = ', this%nmode
    print *, 'rcut = ', this%rcut/ANGSTROM_AU
    print *, 'nq_irr = ', this%qpoints%nk 
  endif

  if (this%pool%rank.eq.0) then
    print *, "pool, Dmat%Mb, Dmat%Nb = ", this%pool%mypool, this%Dmat%Mb, this%Dmat%Nb 
  endif

  call mp_barrier ( comm_world )

end function new_phonons_t

subroutine set_qpoints(this, qpoints)
  class(phonons_t), intent(inout) :: this
  class(kpoints_t), target, intent(in) :: qpoints

  this%qpoints => qpoints
  if (allocated(this%wvq)) deallocate(this%wvq)
  allocate(this%wvq(this%nmode,qpoints%nk))

  if (allocated(this%wvq2)) deallocate(this%wvq2)
  allocate(this%wvq2(this%nmode,qpoints%nk))

end subroutine set_qpoints

subroutine setup_fc(this)
  class(phonons_t), intent(inout) :: this

  integer :: ia_uc_l, il, jl, inb, ja_sc_l, &
    ja_uc_l, ja_uc, ia_uc, i, j, i_loc, j_loc, ix, jx
  integer :: iprow, ipcol, nfc_tot
  real(dp) :: dvec(3), dist, arg, fc(3,3), val

  !! 1. Count the number of nonzero force constants
  call start_clock ('  ph-fc')

  call this%collect_fc(.true.)
  allocate(this%fc(this%nfc),this%fc_indx(3,this%nfc))
  !! 2. Collect force constants
  call this%collect_fc(.false.)

  call mpi_allreduce ( this%nfc, nfc_tot, 1, mpi_integer, mpi_sum, &
    comm_world, mpierr )

  if (ionode) then
    print *, 'nfc_tot = ', nfc_tot
  endif

  call this%collect_fc_diag()
  this%has_fc = .true.

  call stop_clock ('  ph-fc')

end subroutine setup_fc

subroutine collect_fc(this, count_only)
  class(phonons_t), intent(inout) :: this
  logical, intent(in) :: count_only

  integer :: ia_uc_l, il, jl, inb, ja_sc_l, &
    ja_uc_l, ja_uc, ia_uc, i, j, i_loc, j_loc, ix, jx, &
    ka_uc, ka_uc_l, kl, isym
  integer :: iprow, ipcol
  real(dp) :: dvec(3), dist, arg, fc(3,3), val, fc_rot(3,3), at_rot(3), fc2(3,3)
  integer :: ifc, nfc_intra, nfc_inter
  real(dp), allocatable :: dists(:)
  integer, allocatable :: ind(:)

  logical :: skip

  this%fc = 0._dp

  ifc = 0
  nfc_intra = 0
  nfc_inter = 0
  
  allocate(dists(this%struct%na_sc_l), ind(this%struct%na_sc_l))

  do i = 1, this%nmode
    call infog1l(i, this%Dmat%Mb, this%Dmat%nprow, this%Dmat%myprow, 0, i_loc, iprow)
    if (iprow/=this%Dmat%myprow) then
      cycle
    endif

    ! i = 3*(ia_uc-1)+ix
    ia_uc = int((i-1)/3)+1
    ix = mod(i-1,3)+1

    ia_uc_l = mod(ia_uc-1,this%struct%na_uc_l)+1
    il = int((ia_uc-1)/this%struct%na_uc_l)+1 

    do jl = 1, this%struct%nlayer
    ! do jl = il-1, il+1 ! vdw interaction only between adjacent layers
    !   if (jl<1 .or. jl>this%struct%nlayer) cycle

      if (il==jl) then

        ! Intralayer force constants
        do ja_sc_l = 1, this%struct%na_sc_l
          dvec = this%struct%atoms_sc(1:3,ja_sc_l,jl)-this%struct%atoms_uc(1:3,ia_uc_l,il)
          dists(ja_sc_l) = norm2(dvec)
        enddo

        ind(1) = 0
        call hpsort_eps ( this%struct%na_sc_l, dists, ind, eps16)

        do inb = 2, NNB_INTRA+1
          ja_sc_l = ind(inb) 
          ja_uc_l = this%struct%sc_uc_map(ja_sc_l,jl)
          ja_uc = (jl-1)*this%struct%na_uc_l+ja_uc_l

          do jx = 1, 3
            j = 3*(ja_uc-1)+jx
            call infog1l(j, this%Dmat%Nb, this%Dmat%npcol, this%Dmat%mypcol, 0, j_loc, ipcol)

            if (ipcol/=this%Dmat%mypcol) then
              cycle
            endif
            
            ! intralayer components should be evaluated at nonrelaxed positions
            dvec = this%struct%atoms_sc(1:3,ja_sc_l,jl)-this%struct%dtau(1:3,this%struct%sc_uc_map(ja_sc_l,jl),jl) &
              -(this%struct%atoms_uc(1:3,ia_uc_l,il)-this%struct%dtau(1:3,ia_uc_l,il))
            ! dvec = this%struct%atoms_sc(1:3,ja_sc_l,jl)-this%struct%atoms_uc(1:3,ia_uc_l,il)

            call fc_intra(inb-1, dvec, fc)

            val = fc(ix,jx)
            if (abs(val)>eps32) then
              ifc = ifc + 1
              if (.not.count_only) then
                this%fc(ifc) = val
                this%fc_indx(1,ifc) = i_loc
                this%fc_indx(2,ifc) = j_loc
                this%fc_indx(3,ifc) = this%struct%isc_map(ja_sc_l,jl)
              endif
            endif
          enddo ! jx

        enddo ! inb

      else ! il/=jl

        ! Interlayer force constants
        do ja_sc_l = 1, this%struct%na_sc_l
          ja_uc_l = this%struct%sc_uc_map(ja_sc_l,jl)
          ja_uc = (jl-1)*this%struct%na_uc_l+ja_uc_l

          skip = .true.
          do jx = 1, 3
            j = 3*(ja_uc-1)+jx
            call infog1l(j, this%Dmat%Nb, this%Dmat%npcol, this%Dmat%mypcol, 0, j_loc, ipcol)

            if (ipcol==this%pool%mypcol) then
              skip = .false.
            endif
          enddo ! jx

          if (skip) cycle

          dvec = this%struct%atoms_sc(1:3,ja_sc_l,jl)-this%struct%atoms_uc(1:3,ia_uc_l,il)
          dist = norm2(dvec)

          if (dist>this%rcut) cycle

          call fc_kc(dvec,fc)

          do jx = 1, 3 
            j = 3*(ja_uc-1)+jx
            call infog1l(j, this%Dmat%Nb, this%Dmat%npcol, this%Dmat%mypcol, 0, j_loc, ipcol)

            if (ipcol/=this%pool%mypcol) cycle

            if (abs(fc(ix,jx))>eps32) then
              ifc = ifc + 1
              if (.not.count_only) then
                this%fc(ifc) = fc(ix,jx)
                this%fc_indx(1,ifc) = i_loc
                this%fc_indx(2,ifc) = j_loc
                this%fc_indx(3,ifc) = this%struct%isc_map(ja_sc_l,jl)
              endif
            endif
          enddo ! jx

        enddo ! ja_sc_l
      endif
    enddo ! jl
      
  enddo ! i

  if (count_only) then
    this%nfc = ifc
  endif

  deallocate(dists, ind)

  call mp_barrier ( this%pool%comm )

end subroutine collect_fc

subroutine collect_fc_diag(this)
  class(phonons_t), intent(inout) :: this

  integer :: ia_uc_l, il, jl, inb, ja_sc_l, &
    ja_uc_l, ja_uc, ia_uc, i, j, i_loc, j_loc, ix, jx
  integer :: iprow, ipcol, isym
  real(dp) :: dvec(3), dist, arg, fc(3,3), val, fc_sum(3,3), &
    at_rot(3), fc_rot(3,3)
  integer :: ifc_diag

  logical :: skip

  real(dp), allocatable :: dists(:)
  integer, allocatable :: ind(:)

  allocate(dists(this%struct%na_sc_l), ind(this%struct%na_sc_l))
  allocate(this%fc_diag(3*this%Dmat%Nl))
  allocate(this%fc_diag_indx(2,3*this%Dmat%Nl))

  this%fc_diag = 0._dp

  ifc_diag = 0

  do il = 1, this%struct%nlayer
    do ia_uc_l = 1, this%struct%na_uc_l
      ia_uc = (il-1)*this%struct%na_uc_l+ia_uc_l
      skip = .true.
      do ix = 1, 3
        i = 3*(ia_uc-1)+ix
        call infog1l(i, this%Dmat%Mb, this%pool%nprow, this%pool%myprow, 0, i_loc, iprow)
        if (iprow==this%Dmat%myprow) then
          skip = .false.
        endif
      enddo

      if (skip) then
        cycle
      endif

      fc_sum = 0._dp

      do jl = 1, this%struct%nlayer
      ! do jl = il-1, il+1 ! vdw interaction only between adjacent layers
      !   if (jl<1 .or. jl>this%struct%nlayer) cycle

        if (il==jl) then
          ! intralayer components
          do ja_sc_l = 1, this%struct%na_sc_l
            dvec = this%struct%atoms_sc(1:3,ja_sc_l,jl)-this%struct%atoms_uc(1:3,ia_uc_l,il)
            dists(ja_sc_l) = norm2(dvec)
          enddo

          ind(1) = 0
          call hpsort_eps ( this%struct%na_sc_l, dists, ind, eps16)

          do inb = 2, NNB_INTRA+1 ! 3+6+3+6
            ja_sc_l = ind(inb)

            ja_uc_l = this%struct%sc_uc_map(ja_sc_l,jl)
            ja_uc = (jl-1)*this%struct%na_uc_l+ja_uc_l
            
            ! intralayer components should be evaluated at nonrelaxed positions
            dvec = this%struct%atoms_sc(1:3,ja_sc_l,jl)-this%struct%dtau(1:3,this%struct%sc_uc_map(ja_sc_l,jl),jl) &
              -(this%struct%atoms_uc(1:3,ia_uc_l,il)-this%struct%dtau(1:3,ia_uc_l,il))

            call fc_intra(inb-1, dvec, fc)

            fc_sum = fc_sum - fc
          enddo
        else
          ! interlayer components
          do ja_sc_l = 1, this%struct%na_sc_l

            ja_uc_l = this%struct%sc_uc_map(ja_sc_l,jl)
            ja_uc = (jl-1)*this%struct%na_uc_l+ja_uc_l

            dvec = this%struct%atoms_sc(1:3,ja_sc_l,jl)-this%struct%atoms_uc(1:3,ia_uc_l,il)
            dist = norm2(dvec)

            if (dist>this%rcut) then
              cycle
            endif

            call fc_kc(dvec, fc)
            fc_sum = fc_sum - fc
          enddo
        endif
      enddo

      do ix = 1, 3
        i = 3*(ia_uc-1)+ix
        call infog1l(i, this%Dmat%Mb, this%pool%nprow, this%pool%myprow, 0, i_loc, iprow)
        do jx = 1, 3
          j = 3*(ia_uc-1)+jx
          call infog1l(j, this%Dmat%Nb, this%pool%npcol, this%pool%mypcol, 0, j_loc, ipcol)

          if (iprow==this%pool%myprow .and. ipcol==this%pool%mypcol) then
            ifc_diag = ifc_diag + 1
            this%fc_diag(ifc_diag) = fc_sum(ix,jx)
            this%fc_diag_indx(1,ifc_diag) = i_loc
            this%fc_diag_indx(2,ifc_diag) = j_loc
          endif
        enddo
      enddo

    enddo
  enddo

  this%nfc_diag = ifc_diag

  deallocate(dists, ind)

end subroutine collect_fc_diag

subroutine read_wvq(this,data_prefix)
  class(phonons_t), intent(inout) :: this
  character(len=*), intent(in) :: data_prefix
  character(len=256) :: dummy
  integer :: n1, n2, ik

  if (ionode) then
    open(11,file=trim(outdir)//'/'//trim(data_prefix)//'_wvq.dat',form='unformatted')
    read(11) this%wvq
    read(11) this%wvq2
    close(11)
  endif

  call mp_bcast(this%wvq,0,comm_world)
  call mp_bcast(this%wvq2,0,comm_world)

end subroutine read_wvq

! solve phonon problem at all q-points
subroutine solve_all(this, solver, write_evq, data_prefix)
  class(phonons_t), intent(inout) :: this
  integer, intent(in) :: solver
  character(len=*), intent(in) :: data_prefix
  logical, intent(in) :: write_evq ! write eigenvectors to disk
  
  complex(dp), allocatable :: Z(:,:), D(:,:), work(:,:), work2(:,:), gam(:,:)
  real(dp), allocatable :: W(:)

  class(struct_t), pointer :: struct
  class(kpoints_t), pointer :: qpoints
  class(mpi_pool_t), pointer :: pool

  integer :: iq, info, imode, nmode
  character(len=256) :: fn

  integer :: i, j
  real(dp), external :: get_clock

  write(stdout, "(1x,a,F10.1,a)") "Phonons solve_all start @ ", get_clock('ep'), ' s' 

  struct => this%struct
  qpoints => this%qpoints
  pool => qpoints%pool

  nmode = this%Dmat%N

  call start_clock ( '  ph-setup' )
  allocate(D(this%Dmat%Ml,this%Dmat%Nl),Z(this%Dmat%Ml,this%Dmat%Nl),W(this%Dmat%N))
  call diag_setup (this%Dmat, this%Dmat%N, solver)
  call stop_clock ( '  ph-setup' )

  this%wvq = 0._dp
  call start_clock ( '  ph-solve_q' )
  do iq = qpoints%startk, qpoints%endk
    write(stdout, "(3x,a,i5,a,i5,a,F10.1,a)") "Solving qpt # ", iq, "/", qpoints%nk_pool, ' @ ', get_clock('ep'), ' s'

    call start_clock ( '    ph-buildD' )
    call this%build_dynmat (this%qpoints%xk(:,iq), D)
    call stop_clock ( '    ph-buildD' )

    call start_clock ( '    ph-diag' )
    call diagonalize(D, W, Z)
    call stop_clock ( '    ph-diag' )

    do imode = 1, this%nmode
      this%wvq2(imode,iq) = W(imode)

      if (W(imode)>0._dp) then
        this%wvq(imode,iq) = dsqrt(W(imode))
      else
        this%wvq(imode,iq) = -dsqrt(-W(imode))
      endif
    enddo

    if (write_evq) then
      call start_clock ( '    ph-iovec' )
      write(fn, '(a,i0.5,a)') trim(outdir)//'/'//trim(data_prefix)//'_evq.',iq,'.dat'
      call write_matrix_all (fn, this%Dmat, Z)
      call stop_clock ( '    ph-iovec' )
    endif
  enddo
  call stop_clock ( '  ph-solve_q' )

  if (ionode) then
    write(stdout, "(3x,a)") "Waiting for unfinished pools.."
  endif
  call mp_barrier ( comm_world )

  call mp_sum (this%wvq, pool%inter_comm)
  call mp_sum (this%wvq2, pool%inter_comm)

  write(stdout, "(1x,a,F10.1,a)") "Phonons solve_all end @ ", get_clock('ep'), ' s' 

  if (verbosity>=VERBOSITY_DEBUG.and.trim(data_prefix)=='grid') then
    call start_clock ( '  ph-check' )
    call this%check_eigvec_symmetry
    call stop_clock ( '  ph-check' )
  endif

  call start_clock ( '  ph-cleanup' )

  deallocate(D, W, Z)
  nullify(qpoints, struct, pool)

  call diag_cleanup

  call stop_clock ( '  ph-cleanup' )

end subroutine solve_all

subroutine write_wvq(this,data_prefix)
  class(phonons_t), intent(in) :: this
  character(len=*), intent(in) :: data_prefix

  integer :: iq, imode

  if (ionode) then
    open(11,file=trim(outdir)//'/'//trim(data_prefix)//'_wvq.txt',form='formatted')
    write(11,'(a,2I8)') '#', this%qpoints%nk, this%nmode
    do iq = 1, this%qpoints%nk
      do imode = 1, this%nmode
        write(11,'(F20.12)',advance='no') this%wvq(imode,iq) * RYTOEV
      enddo
      write(11,*)
    enddo
    close(11)

    open(11,file=trim(outdir)//'/'//trim(data_prefix)//'_wvq.dat',form='unformatted')
    write(11) this%wvq
    write(11) this%wvq2
    close(11)
  endif

  call mp_barrier(comm_world)
end subroutine write_wvq

subroutine build_dynmat (this, xq, D)
  class(phonons_t), intent(inout) :: this
  real(dp), intent(in) :: xq(3)
  complex(dp), intent(out) :: D(this%Dmat%Ml,this%Dmat%Nl)

  integer :: ia_uc_l, il, jl, inb, ja_sc_l, &
    ja_uc_l, ja_uc, ia_uc, i, j, i_loc, j_loc, ix, jx
  real(dp) :: arg, mat
  complex(dp) :: cfac

  integer :: iprow, ipcol, ifc, jsc

  D = 0._dp

  do ifc = 1, this%nfc
    mat = this%fc(ifc)
    i_loc = this%fc_indx(1,ifc)
    j_loc = this%fc_indx(2,ifc)
    jsc = this%fc_indx(3,ifc)

    arg = TPI*sum(xq*this%struct%R_isc(1:3,jsc))
    cfac = dcmplx(cos(arg),sin(arg))
    
    D(i_loc, j_loc) = D(i_loc, j_loc) + mat*cfac
  enddo

  ! ASR
  do ifc = 1, this%nfc_diag
    mat = this%fc_diag(ifc)
    i_loc = this%fc_diag_indx(1,ifc)
    j_loc = this%fc_diag_indx(2,ifc)

    D(i_loc, j_loc) = D(i_loc, j_loc) + mat
  enddo

  D = D / this%mass

  call pzgeadd('C', this%Dmat%M, this%Dmat%N, (0.5_dp, 0.0_dp), D, 1, 1, this%Dmat%desc,  (0.5_dp, 0.0_dp), D, 1, 1, this%Dmat%desc)

end subroutine build_dynmat

subroutine calc_dos(this,ne,emin,emax)
  class(phonons_t), intent(in) :: this
  integer, intent(in) :: ne
  real(dp), intent(in) :: emin, emax

  integer :: ie
  real(dp), allocatable :: dos(:), int_dos(:), egrid(:) 

  call start_clock('  ph-dos')

  allocate(dos(ne),int_dos(ne),egrid(ne))

  call dos_tetra(this%nmode,this%qpoints,ne,emin,emax,this%wvq,egrid,dos,int_dos)

  if (ionode) then
    print *, 'dos_ne = ', ne
    print *, 'dos_emin = ', emin*RYTOEV
    print *, 'dos_emax = ', emax*RYTOEV
    open(11,file=trim(outdir)//'/phdos.dat',form='formatted')

    do ie = 1, ne
      write(11,"(3F24.12)") RYTOEV*egrid(ie), dos(ie)/RYTOEV, int_dos(ie)
    enddo

    close(11)
  endif

  deallocate(dos,int_dos)

  call mp_barrier(comm_world)
  call stop_clock('  ph-dos')

end subroutine calc_dos

subroutine gamma_matrix (struct, Dmat, xq, isym, gam)
  class(struct_t), intent(in) :: struct
  class(distrib_mat_t), intent(in) :: Dmat
  real(dp), intent(in) :: xq(3)
  integer, intent(in) :: isym

  complex(dp), intent(out) :: gam(Dmat%Ml,Dmat%Nl)

  real(dp) :: q(3), dx(3), arg, scart(3,3)
  complex(dp) :: cfac
  integer, external :: indxl2g

  integer :: j_loc, j_g, i_g, i_loc, ia_uc, ia_uc_l, il, &
    iprow, ix, jx, ja_uc, ja_uc_l, jl, ism1, ipcol, irot 

  gam = (0._dp, 0._dp)

  if (isym < 0) then
    call errore('gamma_matrix','isym<0 at gamma_matrix',isym)
  else if (isym == 1 .or. isym == 0) then
    ! Identity matrix
    do i_loc = 1, Dmat%Ml
      i_g = indxl2g(i_loc, Dmat%Mb, Dmat%pool%myprow, 0, Dmat%pool%nprow)
      if (i_g<1.or.i_g>Dmat%M) cycle

      call infog1l(i_g, Dmat%Nb, Dmat%pool%npcol, Dmat%pool%mypcol, 0, j_loc, ipcol)
      if (ipcol==Dmat%pool%mypcol) then
        gam(i_loc, j_loc) = (1._dp, 0._dp)
      endif
    enddo
    return
  endif

  ! Note:
  ! isym = equiv_s(ik_star) such that
  ! k_star = S_inv(isym) * k_irr
  ! Therefore, we use irot = invs(isym)
  irot = struct%invs(isym)

  q = matmul(struct%relatt, xq)

  do j_loc = 1, Dmat%Nl
    j_g = indxl2g(j_loc, Dmat%Nb, Dmat%pool%mypcol, 0, Dmat%pool%npcol)
    if (j_g<1.or.j_g>Dmat%N) cycle

    jx = mod(j_g-1,3)+1
    ja_uc = int((j_g-1)/3)+1
    ja_uc_l = mod(ja_uc-1,struct%na_uc_l)+1
    jl = int((ja_uc-1)/struct%na_uc_l)+1 
    ia_uc = struct%sym_map(ja_uc_l,jl,irot)
    ia_uc_l = mod(ia_uc-1,struct%na_uc_l)+1
    il = int((ia_uc-1)/struct%na_uc_l)+1

    dx = matmul( struct%sr(:,:,struct%invs(irot)), struct%atoms_uc(:,ia_uc_l,il) ) - struct%atoms_uc(:,ja_uc_l,jl)
    arg = dot_product(q, dx)

    cfac = dcmplx(cos(arg), sin(arg))

    do ix = 1, 3
      i_g = 3*(ia_uc-1)+ix
      call infog1l(i_g, Dmat%Mb, Dmat%pool%nprow, Dmat%pool%myprow, 0, i_loc, iprow)
      if (iprow/=Dmat%pool%myprow) then
        cycle
      endif

      ! gam(i_loc, j_loc) =  cfac * struct%sr(ix,jx,struct%invs(irot))
      gam(i_loc, j_loc) =  cfac * struct%sr(ix,jx,irot)
    enddo
  enddo

end subroutine gamma_matrix

subroutine check_eigvec_symmetry(this)
  class(phonons_t), intent(inout) :: this

  complex(dp), allocatable :: &
    D_irr(:,:), &
    Z_irr(:,:), &
    D_star(:,:), &
    Z_rot(:,:), &
    work(:,:), &
    gam(:,:), &
    R(:,:)

  real(dp) :: residue, resmax, xq_irr(3), xq_rot(3), xq_star(3), freq
  complex(dp) :: overlap

  integer :: iq, info, ibnd, ibnd_g, ibnd_l, ia, ia_g, isym
  character(len=256) :: fn

  integer :: i, j, iq_star, irot, q, ia_uc, ia_uc_l, &
    il, jl, ja_uc, ja_uc_l, iq_irr, istar, ig, jg

  integer :: nq_irr, nq_tot, na_uc, nbnd, nmode

  integer, external :: indxl2g

  type(distrib_mat_t) :: Dmat

  real(dp), external :: get_clock

  na_uc = this%struct%na_uc
  nmode = 3*na_uc
  call Dmat%setup_mat_2d(nmode, nmode, this%pool)

  allocate(D_irr(Dmat%Ml,Dmat%Nl))
  allocate(Z_irr(Dmat%Ml,Dmat%Nl))
  allocate(D_star(Dmat%Ml,Dmat%Nl))
  allocate(gam(Dmat%Ml,Dmat%Nl))
  allocate(Z_rot(Dmat%Ml,Dmat%Nl))
  allocate(work(Dmat%Ml,Dmat%Nl))
  allocate(R(Dmat%Ml,Dmat%Nl))

  nq_irr = this%qpoints%nk
  nq_tot = this%qpoints%nkmax
  write(stdout, *)
  write(stdout, "(1x,a)") "Start checking symmetry of phonon eigenvectors"
  write(stdout, *)

  do iq_irr = this%qpoints%startk, this%qpoints%endk
    write(stdout, "(1x,a,i5,a,i5,a,F10.1,a)") "Checking symmetry iq_irr # ", iq_irr, '/', nq_irr, ' @ ', get_clock('ep'), ' s'
    
    write(fn, '(a,i0.5,a)') trim(outdir)//'/grid_evq.',iq_irr,'.dat'
    call read_matrix(fn, Dmat, Z_irr)

    xq_irr = this%qpoints%xk(1:3, iq_irr)

    do istar = 1, this%qpoints%n_stars(iq_irr)
      iq_star = this%qpoints%stars(istar, iq_irr)
      isym = this%qpoints%equiv_s(iq_star)

      xq_star = this%qpoints%xkg(1:3,iq_star)

      write(stdout, "(3x,a,i5,a,i5,a,i5,a,F10.1,a)") "istar, iq_star, isym # ", istar, ',', iq_star, ',', isym, ' @ ', get_clock('ep'), ' s'

      call this%build_dynmat (xq_star, D_star)

      ! rotate Z_irr
      call gamma_matrix (this%struct, Dmat, xq_irr, isym, gam)

      call PZGEMM ('N', 'N', nmode, nmode, nmode, &
        (1._dp, 0._dp), &
        gam, 1, 1, Dmat%desc, &
        Z_irr, 1, 1, Dmat%desc, &
        (0._dp, 0._dp), &
        Z_rot, 1, 1, Dmat%desc)

      ! calculate residue
      ! R = Z_rot^H * H_star * Z_rot
      call PZGEMM ('N', 'N', nmode, nmode, nmode, &
        (1._dp, 0._dp), &
        D_star, 1, 1, Dmat%desc, &
        Z_rot, 1, 1, Dmat%desc, &
        (0._dp, 0._dp), &
        work, 1, 1, Dmat%desc) ! Z_irr is here temporary array ( H_star * Z_rot )

      call PZGEMM ('C', 'N', nmode, nmode, nmode, &
        (1._dp, 0._dp), &
        Z_rot, 1, 1, Dmat%desc, &
        work, 1, 1, Dmat%desc, &
        (0._dp, 0._dp), &
        R, 1, 1, Dmat%desc)

      resmax = 0._dp

      do i = 1, Dmat%Ml
        do j = 1, Dmat%Nl
          ig = indxl2g(i, Dmat%Mb, this%pool%myprow, 0, this%pool%nprow)
          jg = indxl2g(j, Dmat%Nb, this%pool%mypcol, 0, this%pool%npcol)
          if (ig<1.or.ig>Dmat%M) cycle
          if (jg<1.or.jg>Dmat%N) cycle

          if (ig==jg) then
            residue = abs(this%wvq2(ig,iq_irr)-R(i,j))
          else
            residue = abs(R(i,j))
          endif

          if (residue > eps8) then
            print *, 'ig, jg, residue = ', ig, jg, residue
            print '(3F24.16)', this%wvq2(ig,iq_irr), DBLE(R(i,j)), DIMAG(R(i,j))
            call errore('check_eigvec_symmetry', 'symmetry check failed', 1)
          endif
          if (resmax < residue) then
            resmax = residue
          endif

        enddo
      enddo

      write(stdout, '(5x,a,E12.4)') 'max residue = ', resmax
    enddo
  enddo

  deallocate(D_irr)
  deallocate(Z_irr)

  deallocate(D_star)

  deallocate(gam)
  deallocate(Z_rot)
  deallocate(work)
  deallocate(R)

end subroutine check_eigvec_symmetry

end module phonons_m
