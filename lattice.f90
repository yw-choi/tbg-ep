module lattice

  use mpi
  use kinds, only: dp
  use constants, only: pi, angstrom_au, eps6, eps12, tpi, eps8
  use utils, only: distribute_indices
  use matrix_inversion, only: invmat
  use input, only: params_t

  implicit none

  type, public :: struct_t

    character(len=256) :: struct_fn
    integer :: nlayer1, nlayer2, nlayer, struct_type
    integer :: M, N
    real(dp) :: alat, d_layer, rcut
    real(dp) :: costh, theta, shift(2)

    real(dp) :: latt(3,3), relatt(3,3)
    integer :: na_uc, na_uc_l, na_sc, na_sc_l, nsc
    real(dp), allocatable :: &
      atoms_uc(:,:,:), &
      atoms_sc(:,:,:), &
      dtau(:,:,:)          ! displacement from non-relaxed positions

    integer, allocatable :: &
      sc_uc_map(:,:), &    ! sc_uc_map(na_sc_l,nlayer) sc atom index to uc index
      isc_map(:,:),   &    ! isc_map(na_sc_l,nlayer) supercell index
      R_isc(:,:)           ! R_isc(3,nsc) supercell lattice point (n1,n2)

    integer :: &
      max_neighbors

    integer, allocatable :: &
      num_neighbors(:,:,:), & ! num_neighbors(nlayer, na_uc_l, nlayer)
      neighbors(:,:,:,:)      ! inter_neighbors(max_neighbors,nlayer,  na_uc_l, nlayer)

    integer :: &
      symmetry,             & ! index for inverse s
      invs(48),             & ! index for inverse s
      s(3,3,48),            & ! symmetry matrices, in crystal axis
      nsym

    real(dp) :: &
      sr(3,3,48)

    logical :: time_reversal
    integer :: t_rev(48)

    integer, allocatable :: &
      sym_map(:,:,:)

    integer, allocatable :: &
      displs(:), recvcounts(:)

    integer :: ia_offset, na_loc

  contains

    procedure :: &
      from_tbg_unitcell, &
      from_file, &
      print_info, &
      to_file, &
      dealloc, &
      update_atoms_uc, &
      setup_neighbors, &
      set_rcut

  end type struct_t

  private

contains

  subroutine from_tbg_unitcell(this, params)
    use tbg, only: generate_tbg_unitcell, generate_ttg_unitcell

    class(struct_t), intent(inout) :: this
    class(params_t), intent(in) :: params

    call this%set_rcut (params)
    this%struct_fn = params%struct_fn

    this%nlayer1 = params%nlayer1
    this%nlayer2 = params%nlayer2
    this%M = params%M
    this%N = params%N
    this%alat = params%alat
    this%d_layer = params%d_layer
    this%struct_type = params%struct_type

    if (this%struct_type==2) then
      this%shift = 0.d0 ! TTG-AA
    else
      this%shift = params%shift
    end if

    if (this%struct_type==1) then
      if (params%use_symmetry) then
        if (this%nlayer1==1.and.this%nlayer2==1) then
          this%symmetry = 2
        else if (this%nlayer1==1.and.this%nlayer2==0) then
          this%symmetry = 2
        else if (this%nlayer1==0.and.this%nlayer2==1) then
          this%symmetry = 2
        else
          this%symmetry = 2
        endif
      else
        this%symmetry = 0
      endif

      call generate_tbg_unitcell(this%nlayer1, this%nlayer2, this%M, this%N, this%alat, &
        this%d_layer, this%latt, this%na_uc_l, this%atoms_uc, this%costh, this%theta)

      this%nlayer = this%nlayer1 + this%nlayer2
      this%na_uc = this%na_uc_l * this%nlayer

    else if (this%struct_type==2) then

      ! twisted trilayer sandwiched graphene, AA stacking (C3z)
      this%symmetry = 2 ! @TODO mirror symmetry should be considered.
      call generate_ttg_unitcell(this%M, this%N, this%alat, &
        this%d_layer, this%latt, this%na_uc_l, this%atoms_uc, this%costh, this%theta, &
        this%shift)

      this%nlayer = 3
      this%na_uc = this%na_uc_l * this%nlayer

    else
      call errore('lattice', 'struct_type invalid', 1)
    end if

    ! this%atoms_uc(3,:,1) = this%atoms_uc(3,:,1) - 10.d0
    ! this%atoms_uc(3,:,2) = this%atoms_uc(3,:,2) - 10.d0
    ! this%atoms_uc(3,:,3) = this%atoms_uc(3,:,3) + 10.d0
    ! this%atoms_uc(3,:,4) = this%atoms_uc(3,:,4) + 10.d0

    allocate(this%dtau(3, this%na_uc_l, this%nlayer))
    this%dtau = 0.d0

    call invmat (3, this%latt, this%relatt)
    this%relatt = TPI*transpose(this%relatt)

    call setup(this)

  end subroutine from_tbg_unitcell

  subroutine from_file(this, params)
    class(struct_t), intent(inout) :: this
    class(params_t), intent(in) :: params

    integer :: i, ia, il

    call this%set_rcut (params)
    this%struct_fn = params%struct_fn

    if (ionode) then

      open(11, file=trim(this%struct_fn), form="unformatted", status="old")

      read(11) this%alat, this%d_layer
      read(11) this%M, this%N
      read(11) this%nlayer1, this%nlayer2, this%nlayer, this%na_uc_l
      read(11) this%symmetry

      read(11) this%latt

      allocate(this%atoms_uc(3, this%na_uc_l, this%nlayer))
      allocate(this%dtau(3, this%na_uc_l, this%nlayer))

      read(11) this%atoms_uc
      read(11) this%dtau

      close(11)

    endif

    call mp_bcast(this%alat, 0, comm_world)
    call mp_bcast(this%d_layer, 0, comm_world)
    call mp_bcast(this%M, 0, comm_world)
    call mp_bcast(this%N, 0, comm_world)
    call mp_bcast(this%nlayer1, 0, comm_world)
    call mp_bcast(this%nlayer2, 0, comm_world)
    call mp_bcast(this%nlayer, 0, comm_world)
    call mp_bcast(this%na_uc_l, 0, comm_world)
    call mp_bcast(this%latt, 0, comm_world)
    call mp_bcast(this%symmetry, 0, comm_world)

    if (.not.ionode) then
      allocate(this%atoms_uc(3, this%na_uc_l, this%nlayer))
      allocate(this%dtau(3, this%na_uc_l, this%nlayer))
    endif

    call mp_bcast(this%atoms_uc, 0, comm_world)
    call mp_bcast(this%dtau, 0, comm_world)

    this%na_uc  = this%na_uc_l * this%nlayer
    call invmat (3, this%latt, this%relatt)
    this%relatt = TPI * transpose(this%relatt)

    if (this%N.eq.1.and.this%M.eq.0 .or. this%M.eq.1.and.this%N.eq.0) then
      this%costh = 1.0d0
      this%theta = 0.d0
    else
      this%costh = (this%N*this%N+4.d0*this%N*this%M+this%M*this%M)/(2.d0*(this%N*this%N+this%N*this%M+this%M*this%M))
      this%theta = acos(this%costh)
    endif

    call setup(this)

  end subroutine from_file

  subroutine set_rcut ( this, params )
    class(struct_t), intent(inout) :: this
    class(params_t), intent(in) :: params

    real(dp) :: rcut1, rcut2, rcut3, rcut4

    if (params%lrlx) then
      rcut1 = params%rcut_vdw
    else
      rcut1 = 0.d0
    endif

    if (params%ltb) then
      rcut2 = params%rcut_tb
    else
      rcut2 = 0.d0
    endif

    if (params%lph) then
      rcut3 = params%rcut_ph
    else
      rcut3 = 0.d0
    endif

    if (params%lepmat) then
      rcut4 = params%rcut_ep
    else
      rcut4 = 0.d0
    endif

    this%rcut = max(rcut1, rcut2, rcut3, rcut4)

  end subroutine set_rcut

  subroutine update_atoms_uc (this, atoms_uc_new, dtau)
    class(struct_t), intent(inout) :: this
    real(dp), intent(in) :: atoms_uc_new(3,this%na_uc_l,this%nlayer)
    real(dp), intent(in) :: dtau(3,this%na_uc_l,this%nlayer)

    integer :: il, ia_uc_l, ia_uc

    do il = 1, this%nlayer
      do ia_uc_l = 1, this%na_uc_l
        ia_uc = (il-1)*this%na_uc_l+ia_uc_l
        this%atoms_uc(:,ia_uc_l,il) = atoms_uc_new(:,ia_uc_l,il)
        this%dtau(:,ia_uc_l,il) = dtau(:,ia_uc_l,il)
      enddo
    enddo

    call setup(this)

  end subroutine update_atoms_uc

  subroutine to_file(this, fn)
    class(struct_t), intent(inout) :: this
    character(len=*), intent(in) :: fn ! file name

    integer :: i, ia, il
    logical :: exst
    real(dp) :: latt_T(3,3), invlatt(3,3)

    if (ionode) then
      call invmat(3, this%latt, invlatt)

      open(11, file=trim(fn), form="unformatted", status="replace")

      write(11) this%alat, this%d_layer
      write(11) this%M, this%N
      write(11) this%nlayer1, this%nlayer2, this%nlayer, this%na_uc_l
      write(11) this%symmetry

      write(11) this%latt
      write(11) this%atoms_uc
      write(11) this%dtau

      close(11)

      open(11, file=trim(fn)//'.vesta.in', &
        form="formatted", status="replace")

      do i = 1, 3
        write(11,'(a,3f20.12)') 'lattice_vector ', this%latt(1:3,i) / ANGSTROM_AU
      enddo

      do il = 1, this%nlayer
        do ia = 1, this%na_uc_l
          ! write(11,'(a,3f20.12,a)') 'atom_frac ', matmul(invlatt, atoms_uc(1:3, ia, il)), ' C'
          write(11,'(a,3f20.12,a)') 'atom ', this%atoms_uc(1:3, ia, il) / ANGSTROM_AU, ' C'
        enddo
      enddo

      close(11)
    endif

    call mp_barrier( comm_world )

  end subroutine to_file

  subroutine dealloc(this)
    class(struct_t), intent(inout) :: this

    deallocate(this%atoms_uc)
    deallocate(this%atoms_sc)
    deallocate(this%dtau)
    deallocate(this%sc_uc_map)
    deallocate(this%isc_map)
    deallocate(this%R_isc)
    deallocate(this%sym_map)
    deallocate(this%displs)
    deallocate(this%recvcounts)
  end subroutine dealloc

  subroutine print_info(this)
    class(struct_t), intent(in) :: this

    if (ionode) then
      print '(1x,a)', "TBG unit cell"
      print '(1x,a,i2,a,i2,a)', "(M,N) = (",this%M,",",this%N,")"
      print '(1x,a,f10.3,a)', "theta = ",this%theta*180.d0/PI," deg."
      print '(1x,a,i2,a,i2)', "nlayer1, nlayer2 = ",this%nlayer1,",",this%nlayer2
      print '(1x,a,f10.3,a)', "alat = ",this%alat / ANGSTROM_AU," Ang"
      print '(1x,a,f10.3,a)', "d_layer = ",this%d_layer / ANGSTROM_AU," Ang"
      print '(1x,a,i6,a,i6)', "na_uc, na_uc_l = ",this%na_uc,",",this%na_uc_l
      print *, "Lattice vectors (Ang)"
      print '(1x,a,2F16.8)', 'a1 = ', this%latt(1:2,1)/ANGSTROM_AU
      print '(1x,a,2F16.8)', 'a2 = ', this%latt(1:2,2)/ANGSTROM_AU
      print *, "Reciprocal Lattice vectors (Ang^-1)"
      print '(1x,a,2F16.8)', 'b1 = ', this%relatt(1:2,1)*ANGSTROM_AU
      print '(1x,a,2F16.8)', 'b2 = ', this%relatt(1:2,2)*ANGSTROM_AU
      print *, ''
    endif

    call mp_barrier( comm_world )

  end subroutine print_info

  ! Private subroutines
  subroutine setup(this)
    class(struct_t), intent(inout) :: this

    integer :: il, ia_uc_l

    write(stdout, '(1x,a)') 'Atomic Structure'

    write(stdout, '(1x,a)') 'Lattice vectors'
    write(stdout, '(3F24.12)') this%latt(1:3,1) / ANGSTROM_AU
    write(stdout, '(3F24.12)') this%latt(1:3,2) / ANGSTROM_AU
    write(stdout, '(3F24.12)') this%latt(1:3,3) / ANGSTROM_AU
    write(stdout, '(I)') this%nlayer, this%na_uc_l
    write(stdout, '(1x,a)') 'Atom positions (Ang)'
    do il = 1, this%nlayer
      do ia_uc_l = 1, this%na_uc_l
        write(stdout, '(2I7,3F24.12)') il, ia_uc_l, this%atoms_uc(1:3,ia_uc_l,il) / ANGSTROM_AU
      enddo
    enddo

    call mp_barrier ( comm_world )

    if (allocated(this%displs)) deallocate(this%displs)
    if (allocated(this%recvcounts)) deallocate(this%recvcounts)

    allocate(this%displs(0:nprocs-1),this%recvcounts(0:nprocs-1))

    call distribute_indices(this%na_uc_l, nprocs, this%displs, this%recvcounts)

    this%ia_offset = this%displs(rank)
    this%na_loc = this%recvcounts(rank)

    call setup_symmetries(this)

    call construct_aux_supercell (this)

  end subroutine setup

  subroutine construct_aux_supercell (this)
    class(struct_t), intent(inout) :: this

    integer :: lsc1, lsc2, nsc1, nsc2
    integer :: i, stat, ia, ia_sc, i1, isc1, i2, isc2, isc , il
    integer :: ia_sc_l

    lsc1 = 1 + int(this%rcut / norm2(this%latt(1:3,1)))
    lsc2 = lsc1
    nsc1 = 2*lsc1+1
    nsc2 = 2*lsc2+1
    this%nsc = nsc1*nsc2 ! total number of aux. supercells

    this%na_sc = this%na_uc * this%nsc
    this%na_sc_l = this%na_sc/this%nlayer

    if (.not.allocated(this%atoms_sc)) then
      allocate(this%atoms_sc(3, this%na_sc_l, this%nlayer))
      allocate(this%sc_uc_map(this%na_sc_l, this%nlayer), this%isc_map(this%na_sc_l, this%nlayer), this%R_isc(3,this%nsc))
    endif

    this%sc_uc_map = -1
    this%isc_map = -1

    ia_sc = 0
    isc = 0
    do i1 = 0, nsc1-1
      if (i1.gt.lsc1) then
        isc1 = i1 - nsc1
      else
        isc1 = i1
      endif

      do i2 = 0, nsc2-1
        if (i2.gt.lsc2) then
          isc2 = i2 - nsc2
        else
          isc2 = i2
        endif

        isc = isc + 1

        this%R_isc(1:3,isc) = (/ isc1, isc2, 0 /)

        do ia = 1, this%na_uc_l
          ia_sc = ia_sc + 1
          this%sc_uc_map(ia_sc,:) = ia
          this%isc_map(ia_sc,:) = isc
          do il = 1, this%nlayer
            this%atoms_sc(1:3, ia_sc, il) = matmul(this%latt(:,:), this%R_isc(1:3,isc)) &
                                   + this%atoms_uc(1:3, ia, il)
          enddo
        enddo
      enddo
    enddo

    do il = 1, this%nlayer
      do ia_sc_l = 1, this%na_sc_l
        if (this%sc_uc_map(ia_sc_l,il)<0) then
          print *, this%sc_uc_map(ia_sc_l,il)
          call errore('dbg', 'dbg', 1)
        endif
        ! print '(3I10)', il, ia_sc_l, this%sc_uc_map(ia_sc_l,il)
      enddo
    enddo

    if (ionode) then
      print '(1x,a,I3,a,I3)', "Aux. supercell = ", nsc1, ' x ', nsc2
      print *, 'Number of atoms in aux. supercell = ', this%na_sc
    endif

    call mp_barrier( comm_world )

  end subroutine construct_aux_supercell

  subroutine setup_symmetries(this)
    class(struct_t), intent(inout) :: this

    integer :: isym, il, ia, jl, ja

    real(dp) :: sa(3,3), x(3), xrot(3), invlatt(3,3), minerr, err, max_symm_err


    real(dp), allocatable :: atoms_frac(:,:,:)
    integer :: i, j

    if (this%symmetry.eq.0) then
      this%nsym = 1

      isym = 1
      this%s(1,1:3,isym) =  (/ 1, 0, 0 /)
      this%s(2,1:3,isym) =  (/ 0, 1, 0 /)
      this%s(3,1:3,isym) =  (/ 0, 0, 1 /)

      this%t_rev = 0
      this%time_reversal = .false.

    else if (this%symmetry.eq.1) then
      this%nsym = 12

      this%t_rev = 0
      this%time_reversal = .false.

      isym = 1
      this%s(1,:,isym) =  (/  1,   0,   0 /)
      this%s(2,:,isym) =  (/  0,   1,   0 /)
      this%s(3,:,isym) =  (/  0,   0,   1 /)

      ! isym = 2
      ! this%s(1,:,isym) =  (/ -1,   0,   0 /)
      ! this%s(2,:,isym) =  (/  0,  -1,   0 /)
      ! this%s(3,:,isym) =  (/  0,   0,   1 /)

      ! isym = 3
      ! this%s(1,:,isym) =  (/  0,   1,   0 /)
      ! this%s(2,:,isym) =  (/ -1,   1,   0 /)
      ! this%s(3,:,isym) =  (/  0,   0,   1 /)

      ! isym = 4
      ! this%s(1,:,isym) =  (/  1,  -1,   0 /)
      ! this%s(2,:,isym) =  (/  1,   0,   0 /)
      ! this%s(3,:,isym) =  (/  0,   0,   1 /)

      ! isym = 5
      ! this%s(1,:,isym) =  (/ -1,   1,   0 /)
      ! this%s(2,:,isym) =  (/ -1,   0,   0 /)
      ! this%s(3,:,isym) =  (/  0,   0,   1 /)

      ! isym = 6
      ! this%s(1,:,isym) =  (/  0,  -1,   0 /)
      ! this%s(2,:,isym) =  (/  1,  -1,   0 /)
      ! this%s(3,:,isym) =  (/  0,   0,   1 /)

      isym = 2
      this%s(1,:,isym) =  (/ -1,   0,   0 /)
      this%s(2,:,isym) =  (/  0,  -1,   0 /)
      this%s(3,:,isym) =  (/  0,   0,   1 /)

      isym = 3
      this%s(1,:,isym) =  (/ -1,   0,   0 /)
      this%s(2,:,isym) =  (/ -1,   1,   0 /)
      this%s(3,:,isym) =  (/  0,   0,  -1 /)

      isym = 4
      this%s(1,:,isym) =  (/  1,   0,   0 /)
      this%s(2,:,isym) =  (/  1,  -1,   0 /)
      this%s(3,:,isym) =  (/  0,   0,  -1 /)

      isym = 5
      this%s(1,:,isym) =  (/  0,   1,   0 /)
      this%s(2,:,isym) =  (/ -1,   1,   0 /)
      this%s(3,:,isym) =  (/  0,   0,   1 /)

      isym = 6
      this%s(1,:,isym) =  (/  1,  -1,   0 /)
      this%s(2,:,isym) =  (/  1,   0,   0 /)
      this%s(3,:,isym) =  (/  0,   0,   1 /)

      isym = 7
      this%s(1,:,isym) =  (/ -1,   1,   0 /)
      this%s(2,:,isym) =  (/ -1,   0,   0 /)
      this%s(3,:,isym) =  (/  0,   0,   1 /)

      isym = 8
      this%s(1,:,isym) =  (/  0,  -1,   0 /)
      this%s(2,:,isym) =  (/  1,  -1,   0 /)
      this%s(3,:,isym) =  (/  0,   0,   1 /)

      isym = 9
      this%s(1,:,isym) =  (/  1,  -1,   0 /)
      this%s(2,:,isym) =  (/  0,  -1,   0 /)
      this%s(3,:,isym) =  (/  0,   0,  -1 /)

      isym = 10
      this%s(1,:,isym) =  (/  0,   1,   0 /)
      this%s(2,:,isym) =  (/  1,   0,   0 /)
      this%s(3,:,isym) =  (/  0,   0,  -1 /)

      isym = 11
      this%s(1,:,isym) =  (/  0,  -1,   0 /)
      this%s(2,:,isym) =  (/ -1,   0,   0 /)
      this%s(3,:,isym) =  (/  0,   0,  -1 /)

      isym = 12
      this%s(1,:,isym) =  (/ -1,   1,   0 /)
      this%s(2,:,isym) =  (/  0,   1,   0 /)
      this%s(3,:,isym) =  (/  0,   0,  -1 /)

    else if (this%symmetry.eq.2) then
      ! C3

      this%nsym = 3

      this%t_rev = 0
      this%time_reversal = .false.

      isym =  1   ! identity
      this%s(1,:,isym) = (/  1,  0,  0 /)
      this%s(2,:,isym) = (/  0,  1,  0 /)
      this%s(3,:,isym) = (/  0,  0,  1 /)

      isym =  2   ! 120 deg rotation - cryst. axis [0,0,1]
      this%s(1,:,isym) = (/ -1,  1,  0 /)
      this%s(2,:,isym) = (/ -1,  0,  0 /)
      this%s(3,:,isym) = (/  0,  0,  1 /)

      isym =  3   ! 120 deg rotation - cryst. axis [0,0,-1]
      this%s(1,:,isym) = (/  0, -1,  0 /)
      this%s(2,:,isym) = (/  1, -1,  0 /)
      this%s(3,:,isym) = (/  0,  0,  1 /)

    else
      call errore('setup_symmetry', 'wrong symmetry', this%symmetry)
    end if

    this%sr = 0.d0
    do isym = 1, this%nsym
      sa = dble(transpose(this%s(:,:,isym)))
      this%sr(:,:,isym) = matmul(sa, transpose(this%relatt/TPI))
      this%sr(:,:,isym) = matmul(this%latt, this%sr(:,:,isym))
    enddo

    call inverse_s(this)

    allocate(atoms_frac(3,this%na_uc_l,this%nlayer))

    call invmat(3, this%latt, invlatt)

    do il = 1, this%nlayer
      do ia = 1, this%na_uc_l
        x = matmul(invlatt, this%atoms_uc(:,ia,il))
        x = x - nint(x+eps6)
        ! x(1:2) = x(1:2) - floor(x(1:2)+eps6)
        atoms_frac(:,ia,il) = x
      enddo
    enddo

    if (.not.allocated(this%sym_map)) allocate(this%sym_map(this%na_uc_l,this%nlayer,this%nsym))

    this%sym_map = -1

    max_symm_err = 0.d0
    do isym = 1, this%nsym
      do il = 1, this%nlayer
        do ia = 1, this%na_uc_l
          x = matmul(this%sr(:,:,isym), this%atoms_uc(:,ia,il))
          x = matmul(invlatt, x)
          x = x - nint(x+eps6)
          ! x(1:2) = x(1:2) - floor(x(1:2)+eps6)

          minerr = 1d10
          jloop: do jl = 1, this%nlayer
            do ja = 1, this%na_uc_l
              err = norm2( matmul(this%latt, x - atoms_frac(:,ja,jl)) )
              if (minerr>err) minerr = err
              if (err < eps6) then
                if (max_symm_err<err) max_symm_err = err
                this%sym_map(ia,il,isym) = (jl-1)*this%na_uc_l+ja
                exit jloop
              endif
            enddo
          enddo jloop

          if (this%sym_map(ia,il,isym)==-1) then
            if (ionode) then
              print *, 'ia,il,isym = ',ia,il,isym
              print *, minerr
              print *, this%sr(1,:,isym)
              print *, this%sr(2,:,isym)
              print *, this%sr(3,:,isym)
            endif
            call errore('setup_symmetry', 'equiv. atom not found', isym)
          endif
        enddo
      enddo
    enddo

    if (ionode) then
      print *, 'Symmetry Info'
      print *, 'symmerty = ', this%symmetry
      print *, 'nsym = ', this%nsym
      print '(1x,a,E20.6)', 'max. symm. error = ', max_symm_err
      do isym = 1, this%nsym
        write(6,"(1x,a,I,a,I)") 'isym = ',isym, ', invs(isym) = ', this%invs(isym)
        do i=1,3
          write(6,"(1x,3I,10x,3F8.4)",advance="no") this%s(i,:,isym), this%sr(i,:,isym)
          write(6,*)
        enddo
        write(6,*)
      enddo
    endif

    call mp_barrier( comm_world )

    deallocate(atoms_frac)

  end subroutine setup_symmetries

  subroutine inverse_s(struct)
    class(struct_t), intent(inout) :: struct

    integer :: isym, jsym, ss(3,3)
    logical :: found
    do isym = 1, struct%nsym
      found = .false.
      do jsym = 1, struct%nsym
        ss = matmul(struct%s(:,:,jsym),struct%s(:,:,isym))
        if (all(struct%s(:,:,1)==ss(:,:))) then
          struct%invs(isym)=jsym
          found=.true.
        endif
      enddo
      if (.not.found) call errore('inverse_s', 'inverse s not found', isym)
    enddo

  end subroutine inverse_s

  subroutine setup_neighbors (this, rcut_neighbor)
    class(struct_t), intent(inout) :: this
    real(dp), intent(in) :: rcut_neighbor

    integer :: ia_uc, il, ja_sc, ja_uc, jl
    real(dp) :: dist
    real(dp), allocatable :: dists(:)
    integer, allocatable :: ind(:), unsorted(:)
    integer :: nnb, inb, ierr

    if (.not.allocated(this%num_neighbors)) then
      allocate(this%num_neighbors(this%nlayer,this%na_uc_l,this%nlayer))
    endif

    ! count the number of neighbors within rcut
    ! in each layer
    this%num_neighbors = 0
    do il = 1, this%nlayer
      do ia_uc = this%ia_offset+1, this%ia_offset+this%na_loc
        do jl = 1, this%nlayer
          do ja_sc = 1, this%na_sc_l
            dist = norm2(this%atoms_sc(:,ja_sc,jl)-this%atoms_uc(:,ia_uc,il))
            if (eps6.lt.dist.and.dist.lt.rcut_neighbor) then
              this%num_neighbors(jl, ia_uc, il) = this%num_neighbors(jl, ia_uc, il) + 1
            endif
          enddo
        enddo
      enddo

      call mpi_allgatherv(mpi_in_place, 0, mpi_datatype_null,&
                    this%num_neighbors(:,:,il), &
                    this%nlayer*this%recvcounts, &
                    this%nlayer*this%displs, &
                    mpi_integer, comm_world, ierr)
    enddo

    this%max_neighbors = maxval(this%num_neighbors)
    allocate(dists(this%max_neighbors),ind(this%max_neighbors),unsorted(this%max_neighbors))
    if (.not.allocated(this%neighbors)) then
      allocate(this%neighbors(this%max_neighbors,this%nlayer,this%na_uc_l,this%nlayer))
    endif
    ! collect the indices of the neighbors in each layer
    this%neighbors = 0
    do il = 1, this%nlayer
      do ia_uc = this%ia_offset+1, this%ia_offset+this%na_loc
        do jl = 1, this%nlayer
          nnb = 0
          do ja_sc = 1, this%na_sc_l
            dist = norm2(this%atoms_sc(:,ja_sc,jl)-this%atoms_uc(:,ia_uc,il))

            if (eps6.lt.dist.and.dist.lt.rcut_neighbor) then
              nnb = nnb + 1
              dists(nnb) = dist
              ! this%neighbors(nnb,jl,ia_uc,il) = ja_sc
              unsorted(nnb) = ja_sc
            endif

          enddo
          ind(1) = 0
          call hpsort_eps(nnb,dists,ind,0.001d0)
          do inb = 1, nnb
              this%neighbors(inb,jl,ia_uc,il) = unsorted(ind(inb))
          enddo
        enddo
      enddo
      call mpi_allgatherv(mpi_in_place, 0, mpi_datatype_null,&
                    this%neighbors(:,:,:,il), &
                    this%max_neighbors*this%nlayer*this%recvcounts, &
                    this%max_neighbors*this%nlayer*this%displs, &
                    mpi_integer, comm_world, ierr)
    enddo

    deallocate(dists,ind,unsorted)

  end subroutine setup_neighbors
end module lattice
