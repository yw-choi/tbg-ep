module dynmat
  use mpi
  use matrix_inversion, only: invmat
  use constants, only: eps16, eps6, &
    AMU_RY, RYTOEV, PI, ANGSTROM_AU, RYDBERG_SI, BOHR_RADIUS_SI
  use kinds, only: DP

  implicit none
  ! K. Zhang and E. B. Tadmor, Journal of the Mechanics and Physics of Solids 112, 225 (2018).
  !! LJ potential parameters
  real(dp), parameter :: LJ_eps = 2.39d-3 / RYTOEV, &
                         LJ_sig = 3.41 * ANGSTROM_AU
  
  !! KC potential parameters from Phys. Rev. B 71, 235415
  real(dp), parameter :: &
    C0 = 15.71d-3 / RYTOEV, &
    C2 = 12.29d-3 / RYTOEV, &
    C4 = 4.933d-3 / RYTOEV, &
    C  = 3.030d-3 / RYTOEV, &
    delta = 0.578d0 * ANGSTROM_AU, &
    lambda = 3.629d0 / ANGSTROM_AU, &
    A_KC = 10.238d-3 / RYTOEV, &
    z0 = 3.34d0 * ANGSTROM_AU

  public

  integer, parameter :: NNB_INTRA = 18

contains 

  subroutine fc_intra(inb, dvec, fc)
    integer, intent(in) :: inb
    real(dp), intent(in) :: dvec(3)
    real(dp), intent(out) :: fc(3,3)

    integer :: ishell
    real(dp) :: fc_shell(3,3), costh, sinth, rot(3,3)

    !! intralayer components
    !! parameter setup
    ! Ref. L. Wirtz and A. Rubio, Solid State Communications 131, 141 (2004).
    
    fc_shell = 0.d0
    if (inb<=3) then
      fc_shell(1,1) = -398.7
      fc_shell(2,2) = -172.8
      fc_shell(3,3) =  -98.9

    else if (inb<=9) then
      fc_shell(1,1) = -72.9
      fc_shell(2,2) =  46.1
      fc_shell(3,3) =   8.2

    else if (inb<=12) then
      fc_shell(1,1) =  26.4
      fc_shell(2,2) = -33.1
      fc_shell(3,3) =  -5.8 

    else if (inb<=18) then
      fc_shell(1,1) = -1.0
      fc_shell(2,2) = -7.9
      fc_shell(3,3) =  5.2
    else
      call errore('fc_intra', 'inb>NNB_INTRA', inb)
    endif

    fc_shell = fc_shell / RYDBERG_SI * BOHR_RADIUS_SI * BOHR_RADIUS_SI

    costh = dvec(1) / norm2(dvec)
    sinth = dvec(2) / norm2(dvec)

    call rotation_matrix ( rot, costh, sinth )

    fc = matmul(rot, matmul(fc_shell, transpose(rot)))

  end subroutine fc_intra

  subroutine v_kc ( rvec, rcut, res )
    real(dp), intent(in) :: rvec(3), rcut
    real(dp), intent(out) :: res

    real(dp) :: r, rhod2

    r = norm2(rvec)
    rhod2 = (rvec(1)*rvec(1)+rvec(2)*rvec(2))/(delta*delta)

    res = exp(-lambda*(r-z0))*(C+2.0d0*f_kc(rhod2)) &
           - A_KC * (z0/r)**6 + A_KC * (z0/rcut)**6

  end subroutine v_kc

  subroutine dv_kc ( rvec, res )
    real(dp), intent(in) :: rvec(3)
    real(dp), intent(out) :: res(3)

    real(dp) :: r, rhod2, xy(3)

    xy = (/ rvec(1), rvec(2), 0.d0 /)

    r = norm2(rvec)
    rhod2 = (rvec(1)*rvec(1)+rvec(2)*rvec(2))/(delta*delta)

    res = exp(-lambda*(r-z0))*(-lambda*rvec/r*(C+2.0d0*f_kc(rhod2)) &
                   + 4.d0/(delta*delta)*df_kc(rhod2)*xy) &
                   + 6*A_KC*(z0/r)**6 * rvec / (r*r)
  end subroutine dv_kc

  subroutine fc_KC ( rvec, fc )
    real(dp), intent(in) :: rvec(3) 
    real(dp), intent(out) :: fc(3,3)

    real(dp) :: r, r_xy(3), dlt(3,3), dlt_xy(3,3), x, expfac, f, df, d2f

    integer :: a, b

    fc = 0.d0
    r = norm2(rvec)
    r_xy = [ rvec(1), rvec(2), 0._dp ]
    dlt = 0.d0
    dlt(1,1) = 1.d0
    dlt(2,2) = 1.d0
    dlt(3,3) = 1.d0
    dlt_xy = 0.d0
    dlt_xy(1,1) = 1.d0
    dlt_xy(2,2) = 1.d0
    x = (rvec(1)*rvec(1)+rvec(2)*rvec(2))/(delta*delta)
    expfac = exp(-lambda*(r-z0))

    f = f_kc(x)
    df = df_kc(x)
    d2f = d2f_kc(x)

    do a = 1, 3
      do b = 1, 3
        fc(a,b) = expfac*( lambda**2*rvec(a)*rvec(b)/r**2*(c+2*f) &
                           - 8*lambda*r_xy(a)*r_xy(b)/r/delta**2*df_kc(x) &
                           - lambda*(dlt(a,b)/r-rvec(a)*rvec(b)/r**3)*(c+2*f) &
                           + 4/delta**2*dlt_xy(a,b)*df &
                           + 8/delta**4*r_xy(a)*r_xy(b)*d2f) &
          +6*A_KC*(z0/r)**6 * (dlt(a,b)/r**2-8*rvec(a)*rvec(b)/r**4)
      enddo
    enddo

    fc = -fc 

  end subroutine fc_KC

  subroutine fc_kc_fd ( rvec, fc )
    real(dp), intent(in) :: rvec(3) 
    real(dp), intent(out) :: fc(3,3)

    real(dp), parameter :: h = 1d-4
    real(dp) :: r1p(3), r1m(3), r2p(3), r2m(3), &
      v1p, v1m, v2p, v2m, v0
    integer :: a, b

    do a = 1, 3
      do b = 1, 3
        r1p = rvec
        r1p(a) = r1p(a) + h
        r1m = rvec
        r1m(a) = r1m(a) - h

        r2p = rvec
        r2p(b) = r2p(b) + h
        r2m = rvec
        r2m(b) = r2m(b) - h

        call v_kc ( rvec, 100.d0, v0 )
        call v_kc ( r1p, 100.d0, v1p )
        call v_kc ( r1m, 100.d0, v1m )
        call v_kc ( r2p, 100.d0, v2p )
        call v_kc ( r2m, 100.d0, v2m )

        fc(a,b) = -(v1p+v1m+v2p+v2m-4*v0)/(h*h)
      enddo
    enddo

  end subroutine fc_kc_fd

  ! subroutine fc_KC ( rvec, res )
  !   real(dp), intent(in) :: rvec(3) 
  !   real(dp), intent(out) :: res(3,3)

  !   real(dp) :: rho, r, f, x, x2, fprho, fp, fpp, expfac, arg, &
  !               g, d2g_ab, dg_a, dg_b, dr_a, dr_b, d2r_ab, &
  !               drho_a, drho_b, d2rho_ab
  !   integer :: da3, db3, dab, a, b

  !   res = 0.d0
  !   r = norm2(rvec)

  !   ! do a = 1, 3
  !   !   do b = 1, 3
  !   !     res(a,b) =  -4.0d0*LJ_eps*(156.d0*(LJ_sig/r)**12/r**4-42.d0*(LJ_sig/r)**6/r**4)*rvec(a)*rvec(b)
  !   !   enddo
  !   ! enddo
  !   ! return

  !   rho = norm2(rvec(1:2))

  !   x = rho/delta
  !   x2 = x*x

  !   arg = x2
  !   expfac = exp(-arg)
  !   f = expfac*(C0+C2*x**2+C4*x**4)
  !   fp = 2.d0/delta*expfac*x*( (C2-C0) + (2*C4-C2)*x**2 - C4*x**4  )
  !   fpp = 2.d0/(delta*delta)*expfac*( C2-C0 + &
  !                                     (6*C4-5*C2+2*C0)*x**2 + &
  !                                     (2*C2-9*C4)*x**4 + &
  !                                     2*C4*x**6)

  !   arg = lambda*(r-z0) 
  !   expfac = exp(-arg)
  !   res = 0.d0
  !   do a = 1, 3
  !     do b = 1, 3
  !       if (a.eq.b) then
  !         dab = 1
  !       else
  !         dab = 0
  !       endif
  !       if (a.eq.3) then
  !         da3 = 1
  !       else
  !         da3 = 0
  !       endif
  !       if (b.eq.3) then
  !         db3 = 1
  !       else
  !         db3 = 0
  !       endif
        
  !       if (rho.gt.eps6) then
  !         drho_a = (1-da3)*rvec(a)/rho
  !         drho_b = (1-db3)*rvec(b)/rho
  !         d2rho_ab = (1-da3)*(1-db3)*(rho*rho*dab - rvec(a)*rvec(b))/rho**3 

  !         dr_a = rvec(a)/r
  !         dr_b = rvec(b)/r
  !         d2r_ab = (r*r*dab-rvec(a)*rvec(b))/r**3

  !         g = C+2*f
  !         dg_a = 2*fp*drho_a
  !         dg_b = 2*fp*drho_b

  !         d2g_ab = 2*fpp*drho_a*drho_b + 2*fp*d2rho_ab

  !         res(a,b) = -expfac*(d2g_ab - lambda*(dr_a*dg_b+dr_b*dg_a+d2r_ab*g) + lambda**2*dr_a*dr_b*g) &
  !           + 42.d0 * A_KC/z0**2 * (z0/r)**8 * dr_a * dr_b - 6.d0 * A_KC/z0 * (z0/r)**7 * d2r_ab 
  !       else
  !         ! res(a,b) =  -4.0d0*LJ_eps*(156.d0*(LJ_sig/r)**12/r**4-42.d0*(LJ_sig/r)**6/r**4)*rvec(a)*rvec(b)
  !       endif
        
  !     enddo
  !   enddo
  ! end subroutine fc_KC

  real(dp) function f_kc(x)
    real(dp), intent(in) :: x
    f_kc = exp(-x)*(c0+c2*x+c4*x*x)
  end function f_kc

  real(dp) function df_kc(x)
    real(dp), intent(in) :: x
    df_kc = exp(-x)*(c2-c0 + (2*c4-c2)*x - c4*x*x)
  end function df_kc

  real(dp) function d2f_kc(x)
    real(dp), intent(in) :: x
    d2f_kc = exp(-x)*(2*c4-2*c2+c0+(c2-4*c4)*x+c4*x*x)
  end function d2f_kc

  subroutine rotation_matrix(rot, costh, sinth)
    real(dp), intent(in) :: costh, sinth
    real(dp), intent(out) :: rot(3,3)
    
    rot(1,:) = (/  costh, -sinth, 0.d0 /)
    rot(2,:) = (/  sinth,  costh, 0.d0 /)
    rot(3,:) = (/   0.d0,   0.d0, 1.d0 /)
  end subroutine rotation_matrix
end module dynmat
