module dos
  use mpi
  use constants, only: eps16
  use kinds, only: dp
  use kgrid, only: kpoints_t
  use ktetra, only: tetra_weights
  use utils, only: distribute_indices

  implicit none

contains

  subroutine dos_tetra(nbnd,kpt,ne,emin,emax,eig,egrid,dos,int_dos)
    class(kpoints_t), intent(in) :: kpt
    integer, intent(in) :: ne, nbnd
    real(dp), intent(in) :: emin, emax, eig(nbnd,kpt%nk)
    real(dp), intent(out) :: dos(ne), int_dos(ne), egrid(ne) 

    integer :: ie_g, ie_l, ne_l, nb
    real(dp), allocatable :: wnk(:,:), dwnk(:,:)

    integer, allocatable :: displs(:), recvcounts(:)

    allocate(wnk(nbnd,kpt%nk))
    allocate(dwnk(nbnd,kpt%nk))

    allocate(displs(0:nprocs-1),recvcounts(0:nprocs-1))

    call distribute_indices(ne,nprocs,displs,recvcounts)

    ne_l = recvcounts(rank)

    do ie_l = 1, ne_l
      ie_g = displs(rank)+ie_l

      egrid(ie_g) = emin + (emax-emin)*(ie_g-1)/dble(ne-1)
      call tetra_weights (kpt%nk, nbnd, kpt%ntetra, kpt%tetra, eig, egrid(ie_g), wnk, dwnk)

      dos(ie_g) = sum(dwnk)
      int_dos(ie_g) = sum(wnk)
    enddo

    call mpi_allgatherv(mpi_in_place,0,mpi_datatype_null,&
                  egrid, recvcounts, displs, &
                  mpi_double_precision,comm_world,mpierr)
    call mpi_allgatherv(mpi_in_place,0,mpi_datatype_null,&
                  int_dos, recvcounts, displs, &
                  mpi_double_precision,comm_world,mpierr)
    call mpi_allgatherv(mpi_in_place,0,mpi_datatype_null,&
                  dos, recvcounts, displs, &
                  mpi_double_precision,comm_world,mpierr)

    deallocate(wnk,dwnk,displs,recvcounts)

  end subroutine dos_tetra

  subroutine compute_dos_weights (nbnd, kpt, ef, eig, dwnk, N_F)
    integer, intent(in) :: nbnd
    class(kpoints_t), intent(in) :: kpt
    real(dp), intent(in) :: ef, eig(nbnd,kpt%nk)
    real(dp), intent(out) :: dwnk(nbnd,kpt%nkmax), N_F

    integer :: i, ibnd, ik, ik_irr 
    character(len=100) :: fn
    real(dp), allocatable :: wnk_irr(:,:), dwnk_irr(:,:)
    real(dp) :: ef1

    allocate(wnk_irr(nbnd,kpt%nk))
    allocate(dwnk_irr(nbnd,kpt%nk))

    call tetra_weights (kpt%nk, nbnd, &
      kpt%ntetra, kpt%tetra, &
      eig, ef, wnk_irr, dwnk_irr)

    ! DOS per spin at ef
    N_F = sum(dwnk_irr) 

    dwnk_irr = dwnk_irr / N_F

    if (N_F.lt.eps16) then
      dwnk = 0.d0
    else
      do ik = 1, kpt%nkmax
        ik_irr = kpt%equiv(ik)
        dwnk(:,ik) = dwnk_irr(:,ik_irr)/kpt%n_stars(ik_irr)
      enddo
    endif

    deallocate(wnk_irr,dwnk_irr)

  end subroutine compute_dos_weights
end module dos
